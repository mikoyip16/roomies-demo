import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
    appId: 'live.roomies.app',
    appName: 'roomie_app',
    webDir: 'build',
    bundledWebRuntime: false
};

export default config;
