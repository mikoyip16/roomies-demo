self.addEventListener('install', function () {
    self.skipWaiting();
});

self.addEventListener('activate', function (event) {
    event.waitUntil(clients.claim());
});

self.addEventListener('notificationclick', function (event) { //彈完個notification出黎click可以做乜
    console.log(event)
    let notification = event.notification;
    let action = event.action;
    let url = notification.data.url; //禁完去邊條link


    if (action === 'close') {
        notification.close();
    } else {
        clients.openWindow(url);
        notification.close();
    }
});


self.addEventListener('push', event => { //彈notification
    const data = event.data.json()
    console.log('New notification', data)
    self.registration.showNotification(data.title, data)
    // event.waitUntil(
    //     self.registration.showNotification(data.title, options)
    // );
})