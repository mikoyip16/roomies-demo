
export async function callAPI<Input, Output>(
    method: "GET" | "POST" | "PUT" | "DELETE",
    url: string,
    input?: Input
): Promise<Output> {
    const { REACT_APP_API_SERVER } = process.env;
    if (!REACT_APP_API_SERVER) {
        throw new Error("Missing REACT_APP_API_URL in env");
    }
    let Authorization = "";
    if (!url.startsWith("http")) {
        url = REACT_APP_API_SERVER + url;
        console.log(REACT_APP_API_SERVER)
        console.log(url)
        Authorization = "Bearer " + localStorage.getItem("roomie_token");
    }

    let res = await (function () {
        if (method === "GET" && input) {
            let params = new URLSearchParams();
            Object.entries(input).forEach(([key, value]) => {
                params.set(key, value);
            });
            url = url + "?" + params.toString();
            return fetch(url, {
                headers: {
                    Authorization,
                },
            });
        }

        return fetch(url, {
            method,
            headers: {
                Authorization,
                "Content-Type": "application/json",
            },
            body: JSON.stringify(input),
        });
    })();

    const json = await res.json();
    if (json.error) {
        throw new Error(json.error);
    }
    return json;
}

export function toUrl(url: string) {
    const { REACT_APP_IMAGE_SERVER } = process.env;
    if (!REACT_APP_IMAGE_SERVER) {
        throw new Error("Missing REACT_APP_IMAGE_SERVER in env");
    }
    if (!url.startsWith("/")) {
        url = "/" + url
    }
    return REACT_APP_IMAGE_SERVER + url
}