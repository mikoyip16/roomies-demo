import React, { useEffect, useState } from 'react';
import flower from './images/03flower.png'
import flipflop from './images/04flipflop.png'
import UserReturnBar from '../layout/UserReturnBar';
import { History } from 'history'
import './scss/User.scss'
import { IonContent, IonHeader, IonLoading, IonPage, IonSlide, IonSlides, useIonAlert } from '@ionic/react';
import { useLocation } from 'react-router';
import { useDispatch, useSelector } from 'react-redux';
import { IRootState } from '../../redux/store';
import { getUserbyIDThunk } from '../../redux/roommateInfo/thunk';
import { toUrl } from '../../helpers/api';
import GoogleMapReact from 'google-map-react';
import { Link } from 'react-router-dom'
import moment from 'moment';
import { coordinateList } from '../../redux/types'
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from 'react-responsive-carousel';






const SuggestedUser = (props: {
    lat: number,
    lng: number,
    text: string,
    id: number,
    icon: string,
    userName: string,
}) => {
    const { lat, lng, text, id, icon, userName } = props
    return (
        <div>
            <Link to={`/user/search?userID=${id}`}>
                <div style={{ width: '3rem', height: '2.5rem', borderRadius: '0.5rem', overflow: 'hidden' }}>
                    <div
                        style={{
                            overflow: "hidden",
                            backgroundImage: `url(${toUrl(`${icon}`)})`,
                            backgroundSize: 'cover',
                            backgroundPosition: 'center',
                            width: '3rem',
                            height: '1.5rem',
                        }}>
                    </div>
                    <div style={{
                        width: '3rem',
                        height: '1rem',
                        fontSize: '0.8rem',
                        textAlign: 'center',
                        color: 'black',
                        backgroundColor: 'rgba(244,224,214,1)'
                    }}>{userName}</div>
                </div>
            </Link>
            <div style={{ fontSize: '1.5rem', color: '#e96443' }}>
                <i className="fas fa-map-marker-alt"></i>
            </div>

        </div>

    )

}

const User = (props: { history: History }) => {
    const location = useLocation()
    const params = new URLSearchParams(location.search)
    const userID = params.get('userID') 

 
    const dispatch = useDispatch();
    const userResult = useSelector((state: IRootState) => state.roommateInfo.payload)! as any
    const [present] = useIonAlert();
    const [present2] = useIonAlert();
    const allRoommates = useSelector((state: IRootState) => (state.roommatePage.payload))!
    const filteredRoommates = useSelector((state: IRootState) => (state.roommateFilter.payload))!
    const [like, setLike] = useState(false)
    const [dislike, setDislike] = useState(false)
    const likeAction = () => {
        setLike(!like)
    }
    const disLikeAction = () => {
        setDislike(!dislike)
    }

    useEffect(() => {
        if(!userID){
            return
        }
        dispatch(getUserbyIDThunk(userID))
    }, [dispatch, userID])

    const createNotifcation = async (message: string) => {
        const body = {
            'message': message,
        };
        const res = await fetch(process.env.REACT_APP_API_SERVER + "/user/notification/" + userID, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem('roomie_token')!
            },
            body: JSON.stringify(body)
        });
        const resJson = await res.json();
        console.log(resJson.payload);
    }
    const slideOpts = {
        initialSlide: 2,
        speed: 400
    };


    if (!userResult) {
        return (
            <IonPage>
                <IonHeader>

                    <UserReturnBar history={props.history} />
                </IonHeader>
                <IonContent>
                    <IonLoading isOpen></IonLoading>
                </IonContent>
            </IonPage>)
    }
    const userInfo = userResult.userData
    const meetingData = userResult.meetingData[0]
    const siteData = userResult.siteData[0]
    const siteDataArr = userResult.siteData
    console.log(siteDataArr)
    const roommateIconUrl = toUrl(userInfo.image || 'userIcon.png')
    const rentDate = moment(userInfo.rent_date).format('DD/MM/YYYY')

    const ranNumber = Math.ceil(Math.random() * 90)
    const suggestedRoommateNumber = ranNumber + 5
    console.log(allRoommates.slice(ranNumber, suggestedRoommateNumber))

    return (
        <IonPage>
            <IonHeader>

                <UserReturnBar history={props.history} />
            </IonHeader>
            <IonContent>

                <div className='User'>

                    <div className='user-container'>

                        <div className='profile'>
                            <div className='user-name'>{userInfo.nickname}{siteData && (<span className='purple-text'>業主</span>)}</div>
                            <div className='userIcon' style={{

                                overflow: "hidden",
                                backgroundImage: `url(${roommateIconUrl})`,
                                backgroundSize: 'cover',
                                backgroundPosition: 'center',
                                width: '19vw',
                                height: '19vw',
                                borderRadius: '9.5VW',
                                margin: '1rem'
                            }}></div>
                            <div className='requirement'>
                                <div className='purple-text'>租住月數： {userInfo.rent_duration}</div>
                                <div className='purple-text'>心儀地區： {userInfo.preferred_location}</div>
                                <div className='purple-text'>起租日期：{rentDate}</div>
                                <div className='purple-text'>預計租金: ${userInfo.budget_min}~${userInfo.budget_max}</div>



                            </div>
                            <div className='detail-description'>
                                <div className='black-text'>來自{userInfo.ethnicity} ｜ {userInfo.age}</div>
                                <div className='black-text'>從事{userInfo.occupation}行業的{userInfo.gender}生</div>
                                <div className='tag'>#{userInfo.not_smoking_text}</div>
                                <div className='tag'>#{userInfo.have_no_kids_text}</div>
                                <div className='tag'>#{userInfo.have_no_pet_text}</div>
                                <div className='tag'>#{userInfo.language}</div>
                                <div className='tag'> #{userInfo.religion}</div>

                            </div>
                            <div className='like-dislike'>
                                <div className='like-container' onClick={likeAction}>
                                    <img src={flower}
                                        alt=''
                                        style={{
                                            width: 'auto',
                                            height: '33px',
                                            overflow: 'hidden',
                                        }} />
                                    <div className='number' >{userInfo.flower + like}</div>
                                </div>
                                <div className='dislike-container' onClick={disLikeAction}>
                                    <img src={flipflop}
                                        alt=''
                                        style={{
                                            width: 'auto',
                                            height: '33px',
                                            overflow: 'hidden',
                                        }} />
                                    <div className='number'>{userInfo.egg + dislike}</div>
                                </div>
                            </div>
                        </div>
                        <div className='message-container'>
                            <div className={`message-btn ${userInfo.telegram_username ? '' : 'ion-hide'}`}>
                                <a href={`https://t.me/${userInfo.telegram_username}`} target="_blank">
                                    <span>
                                        <i className="fab fa-telegram-plane"></i>
                                        {'  '}
                                        Telegram
                                    </span>
                                </a>
                            </div>
                            <div className='message-btn'>
                                <span onClick={() => present({
                                    header: '講比佢知，有人對佢有興趣！',
                                    message: '你仲想同佢講d咩？',
                                    inputs: [
                                        {
                                            type: 'text',
                                            name: 'message',
                                            placeholder: 'Please Input here!',
                                           
                                        }
                                    ],
                                    buttons: [
                                        'Cancel',
                                        { text: 'Ok', handler: (d) => { createNotifcation(d.message); present2('我地幫你講左佢知啦～', [{ text: 'Ok' }]) } },
                                    ],
                                })
                                }>
                                    <i className="far fa-envelope"></i>
                                    {'  '}
                                    發送興趣
                                </span>
                            </div>
                        </div>
                        <div className='about'>
                            
                            <div className='title'>
                                關於
                            </div>
                            <div className='about-detail'>
                                {userInfo.introduction}
                            </div>
                        </div>
                        <div className='activity'>
                            <div className='title'>
                                活動
                            </div>
                            {
                                !meetingData ? (<div className='activity-detail'><div className='heading input-box'>
                                    暫時未有參與活動
                                </div></div>) : (<div className='activity-detail'>
                                    <div className='heading input-box'>
                                        {meetingData.address}見面會
                                    </div>
                                    <div className='input-box'>
                                        日期: {moment(meetingData.date).format('DD/MM/YYYY')}
                                    </div>
                                    <div className='input-box'>
                                        時間： 晚上7點
                                    </div>
                                </div>)
                            }

                        </div>
                        <div className='site'>
                            <div className='title'>
                                他/她的樓盤
                            </div>




                            {!siteData ? (<div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', marginTop: '20%', fontWeight: 'bold', fontSize: '14px' }}>暫時未有樓盤</div>)
                                : (<Carousel>
                                    {siteDataArr.slice(0, 3).map((site: any, i: number) => {
                                       

                                        return <SiteDetailInfo
                                            i={i}
                                            title={site.title}
                                            years={site.years}
                                            usable_area={site.usable_area}
                                            floor={site.floor}
                                            rooms={site.rooms}
                                            district={site.district}
                                            address={site.address}
                                            rent={site.rent}
                                            image={site.image}
                                        />
                                    })

                                    }
                                </Carousel>)}

                        </div>
                        <div className='suggested-roommate'>
                            <div className='title'>
                                建議室友
                            </div>
                            <div className='map' style={{ height: '30vh', width: '100%', overflow: 'hidden' }}>
                                {/* Google Map To Be Edited */}
                                <GoogleMapReact
                                    bootstrapURLKeys={{ key: `${process.env.REACT_APP_GOOGLE_MAP_API_KEY}` }}
                                    defaultCenter={{
                                        lat: coordinateList[allRoommates[ranNumber].preferred_location].lat,
                                        lng: coordinateList[allRoommates[ranNumber].preferred_location].lng
                                    }}
                                    defaultZoom={12}>
                                   
                                    {allRoommates.slice(ranNumber, suggestedRoommateNumber).map((roommate, i) => {
                                       
                                        return (

                                            <SuggestedUser
                                                key={i}
                                                lat={coordinateList[roommate.preferred_location].lat}
                                                lng={coordinateList[roommate.preferred_location].lng}
                                                text="My Marker"
                                                id={roommate.id}
                                                icon={roommate.image}
                                                userName={roommate.username} />
                                        )
                                    })}



                                </GoogleMapReact>
                            </div>
                        </div>
                    </div>
                </div>
            </IonContent>
        </IonPage>
    )
}

export default User


const SiteDetailInfo = (props: {
    i: number,
    title: string,
    years: number,
    usable_area: number,
    floor: number,
    rooms: number,
    district: string,
    address: string,
    rent: number
    image: string
}) => {
    const { i, title, years, usable_area, floor, rooms, district, address, rent, image } = props
    return (
        <div key={`slide${i}`} style={{ padding: 20, height: 170 }}>
            <div className='site-detail'>
                <img className='site-pic' src={toUrl(image)} alt="" style={{ objectFit: 'cover' }} />
                <div className='site-description'>
                    <div className='heading-black'>{title}</div>
                    <div className='content-grey'>
                        樓齡{years}年
                        <span className='separator'>{' '}｜{' '}</span>
                        {usable_area}呎
                        <span className='separator'>{' '}｜{' '}</span>
                        {floor}/F
                        <span className='separator'>{' '}｜{' '}</span>
                        {rooms}房1廳
                    </div>
                    <div className='content-grey'>
                        {district}
                        <span className='separator'>{' '}｜{' '}</span>
                        {address}
                    </div>
                    <div className='rent'>
                        ${rent}/月
                    </div>
                </div>
            </div>
        </div>)
}

