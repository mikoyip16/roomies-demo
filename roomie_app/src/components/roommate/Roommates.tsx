import React from 'react';
import RoommateItem from './RoommateItem';
import { useSelector } from "react-redux";
import { IRootState } from '../../redux/store';
import './scss/Roommates.scss'

const Roommates = () => {


    const userID = useSelector((state: IRootState) => state.auth.payload?.userID)
    const allRoommates = useSelector((state: IRootState) => (state.roommatePage.payload))!
    const filteredRoommates = useSelector((state: IRootState) => (state.roommateFilter.payload))!
    const hasQuery = useSelector((state: IRootState) => (state.roommateFilter.hasQuery))
    const searchText = useSelector((state: IRootState) => (state.roommateFilter.searchText?.toLocaleLowerCase() || ''))


    let payload = (filteredRoommates || allRoommates)
        ?.filter(roommate =>
            roommate.username.toLocaleLowerCase().includes(searchText) && 
            roommate.id !== userID
        )


    return (
        <div>
            {payload&&payload.length !== 0 ? (
                payload.map((roommate, i) => (
                    <RoommateItem
                        key={i}
                        id={roommate.id}
                        username={roommate.username}
                        image={roommate.image}
                        flower={roommate.flower}
                        egg={roommate.egg}
                        gender={roommate.gender}
                        nickname={roommate.nickname}
                        age={roommate.age}
                        introduction={roommate.introduction}
                        educationLevel={roommate.education_level}
                        ethnicity={roommate.ethnicity}
                        occupation={roommate.occupation}
                        language={roommate.language}

                    />))
            ) : (<div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', height: '200px' }}>
                <div style={{ color: '#c4c4c4' }}> 搜尋結果： 沒有符合條件的室友， 請輸入其它條件</div>

            </div>)}
        </div>
    )
}

export default Roommates