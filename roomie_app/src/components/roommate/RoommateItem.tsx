import React from 'react'
import { Link } from 'react-router-dom'
import { toUrl } from '../../helpers/api'
import './scss/RoomateItem.scss'

const RoommateItem = (props:{
    id?: number
    username?: string
    image?: string
    flower?: number
    egg?: number
    gender?: string
    nickname?: string
    age?: string
    introduction?: string
    educationLevel?: string
    ethnicity: string
    occupation?: string
    language?: string
}) => {
    const {id, image, flower, egg, gender, nickname, age, introduction, educationLevel, ethnicity, occupation} = props
    const roommateIconUrl = toUrl(image || 'userIcon.png')
    return (
        <Link to={ `/user/search?userID=${id}` }>
            <div className='user-card-container'>
                <div className='userIcon' style={{
                   
                    overflow: "hidden",
                    backgroundImage: `url(${roommateIconUrl})`,
                    backgroundSize: 'cover',
                    backgroundPosition: 'center'
                }}></div>
                <div className='profile-detail'>
                    <div className='userName'>{nickname}</div>
                    <div className='black-des'>來自{ethnicity}{' '}<span className='vertical-ora'>｜</span>{' '}{age}</div>
                    <div className='black-des'>從事{occupation}行業的{gender}生</div>
                    <div className='grey-des'>
                        {introduction}
                    </div>
                </div>


            </div>
        </Link>
    )
}

export default RoommateItem