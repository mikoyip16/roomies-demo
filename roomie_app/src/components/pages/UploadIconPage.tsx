import React from 'react';
import { Link, useHistory } from 'react-router-dom'
import {
    IonPage,
    IonContent,
    IonLabel,
    IonHeader,
    IonToolbar,
    IonTitle,
    IonFooter,
    IonButton,
    IonIcon,
    IonAvatar,
    IonImg
} from '@ionic/react';
import { imageOutline } from 'ionicons/icons'
import { selectImage, fileToBase64String } from '@beenotung/tslib/file'
import { useStateProxy } from 'use-state-proxy'
import './scss/SignUp.scss'
import { updateIconAction } from '../../redux/auth/action';
import { useDispatch, useSelector } from 'react-redux';
import { toUrl } from '../../helpers/api';
import { IRootState } from '../../redux/store';

function Label({ text }: { text: string }) {
    return (
        <IonLabel
            color="roomie"
            position="floating"
            style={{ textTransform: 'capitalize' }}
        >
            {text}
        </IonLabel>
    )
}


const UploadIconPage = () => {
    const image = useSelector((state: IRootState) => state.auth.payload?.image)
    const roommateIconUrl = toUrl(image || 'userIcon.png')
    type State = {
        file?: File
        preview?: string
        uplodeFile: boolean
    }
    const state = useStateProxy<State>({
        uplodeFile: false
    })
    const history = useHistory()
    async function pickFileaynsc() {
        console.log('select img')
        const [file] = await selectImage({ multiple: false })

        console.log('make preview')
        state.file = file
        state.preview = await fileToBase64String(file)
        console.log('made preview')
        state.uplodeFile = true
    }
    function pickFile() {
        console.log('select img')

        let input = document.createElement('input')
        input.type='file'
        input.accept='image/*'
        input.onchange=()=>{
            let file = input.files![0]
            console.log('make preview')
            state.file = file
            fileToBase64String(file).then(preview=>{
                state.preview = preview

            console.log('made preview')
            state.uplodeFile = true
            })
        }
        document.body.appendChild(input)
        input.click()
    }
    const dispatch = useDispatch()
    async function submit() {
        const token = localStorage.getItem('roomie_token')!
        if (!state.file) {
            console.log("backHome")
            history.push('/')
            return
        }
        let formData = new FormData()
        formData.set('token', token)
        formData.set('image', state.file)

        let res = await fetch(process.env.REACT_APP_API_SERVER + '/user/upload/userIcon', {
            method: 'PUT',
            headers:{
                "Authorization": "Bearer " + localStorage.getItem('roomie_token'),
               
            },
            body: formData
        })
        if (res.status == 200) {
            const result = await res.json()
            localStorage.setItem('roomie_token', result.jwt_token)
            dispatch(updateIconAction(result.jwt_token))
            history.push('/')
        }
    }

   

    return (
        <IonPage className='SignUpPageOne'>
            <IonHeader>
                <IonToolbar className='rainbow-bg'>
                    <IonTitle >更改頭像</IonTitle>

                </IonToolbar>

            </IonHeader>
            <IonContent>

                <IonAvatar style={{ marginTop: '8rem', marginLeft: 'calc(50% - 5rem)', width: '10rem', height: '10rem' }}>
                    <IonImg src={state.preview || roommateIconUrl}></IonImg>
                </IonAvatar>




                <IonButton color="roomie" style={{ position: 'absolute', left: 'calc(50% + 0rem)', top: '15.5rem' }} size="large" fill='clear' onClick={pickFile}>
                    <IonIcon icon={imageOutline}></IonIcon>
                </IonButton>


            </IonContent>
            <IonFooter style={{ top: '-3rem', left: 'calc(50% - 3rem)' }}>
                    <div style={{
                        display: 'flex',
                        alignItems: 'center',
                        justifyContent: 'center',
                        left: '143px',
                        width: '6rem',
                        height: '45px',
                        backgroundColor: '#E96443',
                        borderRadius: '45px',
                        color: 'white',
                    }}
                        onClick={submit}
                    >
                        上傳頭像
                    </div>

            </IonFooter>
        </IonPage>
    )
}


export default UploadIconPage