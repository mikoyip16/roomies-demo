import React, { MouseEvent, useEffect, useState } from 'react'
import { Link, useHistory, useLocation } from 'react-router-dom'
import SiteHeader from '../layout/SiteHeader'
import { IonPage, IonContent, IonFooter, IonItem, IonAvatar, IonImg, IonInput, IonButtons, IonButton, IonIcon } from '@ionic/react';
import './scss/SiteMeetingPageDetail.scss'
import GatheringInfo from '../gatherings/GatheringInfo';
import peter from "../../images/peter.jpg"
import GatheringAnttendee from '../gatherings/GatheringAnttendee';
import GatheringMessage from '../gatherings/GatheringMessage';
import { useSelector } from 'react-redux';
import { IRootState } from '../../redux/store';
import { paperPlane } from 'ionicons/icons';
interface IMessage {
    "usersid": number,
    "id": number,
    "content": string,
    "nickname": string,
    "image": string,
    "msg_img": string,
    "edit": boolean,
    "del": boolean,
}

const GatheringPageDetail = () => {
    const history = useHistory()
    const location = useLocation()
    const [eventInfo, setEventInfo] = useState<any>({});
    const [eventImage, setEventImage] = useState<any>({});
    const [imageIndex, setImageIndex] = useState(0);
    const [content, setContent] = useState<string>("");
    const [messages, setMessages] = useState<IMessage[]>([]);

    const params = new URLSearchParams(location.search)
    const tab = params.get('tab')
    const eventID = params.get('eventID')
  
    function formatDate(date: any) {
        try {
            date = new Date(date)
        } catch (error) {
            date = new Date()
        }
        if (!date.getTime()) {
            date = new Date()
        }
        return new Intl.DateTimeFormat('cn-HK', {
            dateStyle: 'medium',
        }).format(date)
    }

    const createMessages = async (meetingID: string, content: string) => {
        const formData = new FormData();
        formData.set('eventID', meetingID);
        formData.set('content', content);

        const res = await fetch(process.env.REACT_APP_API_SERVER + "/event/message", {
            method: "POST",
            headers: {
                'Authorization': 'Bearer ' + localStorage.getItem('roomie_token')
            },
            body: formData
        });
        if (res.status !== 200) {
            alert("Create Message Fail!")
        } else {
            alert("Create Message Success")
        }
    }
    const getMessages = async (eventID: string) => {
        const res = await fetch(process.env.REACT_APP_API_SERVER + `/event/messages/${eventID}`) //!!!!!!!!!!!TODO:先hardcode user id =2, 之後應該用jwt set
        const resJson = await res.json();
        console.log(resJson.payload);
        setMessages(resJson.payload.messages);
    }

    const onSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
        
        e.preventDefault();
        if (!content) {
            return;
        }
        await createMessages(eventID!, content);
        await getMessages(eventID!);
        setContent('')

    };

    function formatTime(date: any) {
        try {
            date = new Date(date)
        } catch (error) {
            date = new Date()
        }
        if (!date.getTime()) {
            date = new Date()
        }
        return new Intl.DateTimeFormat('cn-HK', {
            hour: 'numeric',
            minute: 'numeric',
            // dateStyle: 'medium',
            timeZone: 'GMT'
        }).format(date)
    }
    const googleMapURL = `http://www.google.com/maps/place/${eventInfo.map_x},${eventInfo.map_y}`

    const changeTab = (name: string) => {
        let url = location.pathname + '?eventID=' + eventID + '&tab=' + name
        console.log(url)
        history.push(url)
    }
    const getEventInfo = async (eventID: string) => {
        const res = await fetch(`${process.env.REACT_APP_API_SERVER}/event/${eventID}`) //而家backend同fontend分開左，就要由http開始寫
        console.log("res")
        console.log(res);
        if (res.status !== 200) {
            console.log("error")
        } else {
            const resJson = await res.json();
            console.log(resJson)
            if (!resJson.payload.eventInfo[0]) {
                // return
            }
            setEventInfo(resJson.payload.eventInfo[0]);
            setEventImage(resJson.payload.eventImage);
        }
    }
    const { userID, username, nickname, image } = useSelector((state: IRootState) => state.auth.payload!)

    useEffect(() => {
        if (eventID) {
            getEventInfo(eventID);
            getMessages(eventID!);
        }
    }, [eventID])

    return (
        <IonPage>
            <IonContent>
                <SiteHeader title={eventInfo.title} previousPage='/' />
                {/* style={{ width: '100vw', position: 'fixed', top: '40px', zIndex: '-2' }} */}
                <div className="flatPic" style={{ zIndex: 99999 }}>
                    {/* ???????????siteInfo.siteImage[0]!!!!!!!!!!!!!!!!!!!有四張圖 */}
                    {/* process.env.REACT_APP_IMAGE_SERVER as String + siteInfo.siteImage[0] */}
                    <img style={{ height: '240px', width: '100vw', objectFit: 'cover' }} src={process.env.REACT_APP_IMAGE_SERVER + "/" + eventImage[imageIndex]?.image}></img>
                    <div className="siteMeetingDiv">
                        <div>活動</div>
                        <div>{formatTime(eventInfo.date)}</div>
                        <div>{formatDate(eventInfo.date)}</div>

                    </div>
                    <div className="siteImageMapDiv" >
                        <a href={googleMapURL} target="_blank">
                            <i className="fas fa-map-marker-alt"></i>
                        </a>
                    </div>
                    <img className='siteOwnerDiv' src={process.env.REACT_APP_IMAGE_SERVER + "/" + eventInfo.image}></img>
                    <div className="siteImageNavDiv">
                        <div onClick={() => { setImageIndex(0) }} className={`siteImageNav ${imageIndex == 0 ? 'active' : ''}`}></div>
                        {/* <div onClick={() => { setImageIndex(1) }} className={`siteImageNav ${imageIndex == 1 ? 'active' : ''}`} ></div>
                        <div onClick={() => { setImageIndex(2) }} className={`siteImageNav ${imageIndex == 2 ? 'active' : ''}`}></div>
                        <div onClick={() => { setImageIndex(3) }} className={`siteImageNav ${imageIndex == 3 ? 'active' : ''}`}></div> */}
                    </div>
                </div>
                <div className="siteNavBar" style={{ zIndex: 99999 }}>
                    <div className={tab == 'gatheringInfo' ? 'active nav' : 'nav'} onClick={() => changeTab('gatheringInfo')}>樓盤資訊</div>
                    <div className={tab == 'eventAnttendee' ? 'active nav' : 'nav'} onClick={() => changeTab('eventAnttendee')}>出席者</div>
                    <div className={tab == 'eventMessage' ? 'active nav' : 'nav'} onClick={() => changeTab('eventMessage')}>留言區</div>
                </div>
                <div className="PlaceHolderForNavBarAndFlatPic" style={{ height: '345px', width: '100%', background: 'white', zIndex: 99998 }}></div>
                {(function () {
                    if (!eventID) {
                        return
                    }
                    switch (tab) {
                        case 'gatheringInfo':
                            return <GatheringInfo description={eventInfo.description} ppl={eventInfo.ppl} address={eventInfo.meeting_address} />
                        case 'eventAnttendee':
                            return <GatheringAnttendee eventID={eventID} />
                        case 'eventMessage':
                            return <GatheringMessage messages={messages} />
                        case '':
                            return <GatheringInfo description={eventInfo.description} ppl={eventInfo.ppl} address={eventInfo.meeting_address} />

                    }
                })()}
            </IonContent>
            {tab == 'eventMessage' &&
                <IonFooter>
                    <IonItem>
                        <IonAvatar style={{ width: '40px', height: '40px' }} slot='start'>
                            <IonImg src={process.env.REACT_APP_IMAGE_SERVER + "/" + image}></IonImg>
                        </IonAvatar>
                        <IonInput onIonChange={(e: Event) => {
                            setContent((e.target as HTMLInputElement).value)
                        }} value={content} placeholder='Please input here' style={{ borderBottom: '1px solid' }}></IonInput>
                        <IonButtons slot='end'>
                            <IonButton onClick={(e: any) => onSubmit(e)} color="dark">
                                <IonIcon icon={paperPlane}></IonIcon>
                            </IonButton>
                        </IonButtons>
                    </IonItem>
                </IonFooter>
            }
        </IonPage>)
}

export default GatheringPageDetail
