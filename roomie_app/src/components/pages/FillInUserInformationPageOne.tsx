import React, { useEffect } from 'react';
import { Link } from 'react-router-dom'
import {
    IonPage,
    IonContent,
    IonLabel,
    IonSelect,
    IonSelectOption,
    IonHeader,
    IonToolbar,
    IonTitle,
    IonFooter,
    IonInput,
    IonLoading
} from '@ionic/react';
import { useIonFormState } from 'react-use-ionic-form'
import './scss/SignUp.scss'
import { useDispatch, useSelector } from 'react-redux';
import { IRootState } from '../../redux/store';
import { getUserbyIDThunk } from '../../redux/roommateInfo/thunk';

function Label({ text }: { text: string }) {
    return (
        <IonLabel
            color="roomie"
            position="floating"
            style={{ textTransform: 'capitalize' }}
        >
            {text}
        </IonLabel>
    )
}
async function updateUserInformation(info: {
    nickname?: string;
    ethnicity?: string;
    religion?: string;
    language?: string;
    occupation?: string;
    gender?: string;
    age?: string;
    not_smoking_text?: string;
    have_no_pet_text?: string;
    have_no_kids_text?: string;
    rent_duration?: string;
    rent_date?: string;
    introduction?: string;
    budget_min?: number;
    budget_max?: number;
    preferred_location?: string


}) {
    try {
        const host = process.env.REACT_APP_API_SERVER
        const res = await fetch(`${host}/user/update`, {
            method: 'PUT',
            headers: {
                "Authorization": "Bearer " + localStorage.getItem('roomie_token'),
                "Content-Type": "application/json",
            },
            body: JSON.stringify({

                info
            })
        })
        if (res.status == 200) {
            console.log('success')
        }
    } catch (err) {
        console.error(err.message)
    }

}


const FillInUserInformationPageOne = () => {

    const userInfoObj = JSON.parse(localStorage.getItem('userInfo')!)
    const userInfo = userInfoObj.userData

  

    const onClick = () => {

        const info = state

        updateUserInformation(info)

    }
    const { state, item } = useIonFormState({
        nickname: userInfo.nickname || '',
        ethnicity: userInfo.ethnicity || '',
        religion: userInfo.religion || '',
        language: userInfo.language || '',
        occupation: userInfo.occupation || '',
        gender: userInfo.gender || '',
        age: userInfo.age || '',
        not_smoking_text: userInfo.not_smoking_text || '',
        have_no_pet_text: userInfo.have_no_pet_text || '',
        have_no_kids_text: userInfo.have_no_kids_text || '',

    })


    return (
        <IonPage className='SignUpPageTwo'>
            <IonHeader>
                <IonToolbar className='rainbow-bg'>
                    <IonTitle >填寫/更改用戶資料</IonTitle>

                </IonToolbar>
                <div className='stepper'>
                    <div className='circle circle-active'></div>
                    <div className='circle'></div>
                </div>
            </IonHeader>
            <IonContent>
                <div style={{ marginTop: '1.5rem', width: "100vw", height: '60vh', overflow: 'auto' }}>
                    {item({
                        name: 'nickname',
                        renderLabel: () => <Label text="暱稱" />,
                        renderContent: props => <IonInput type="text" {...props} />,
                    })}
                    {item({
                        name: 'ethnicity',
                        renderLabel: () => <Label text="來自" />,
                        renderContent: props => (
                            <IonSelect {...props}>
                                <IonSelectOption>香港</IonSelectOption>
                                <IonSelectOption>台灣</IonSelectOption>
                                <IonSelectOption>中國內地</IonSelectOption>
                            </IonSelect>
                        ),
                    })}
                    {item({
                        name: 'religion',
                        renderLabel: () => <Label text="宗教" />,
                        renderContent: props => (
                            <IonSelect {...props}>
                                <IonSelectOption>佛教</IonSelectOption>
                                <IonSelectOption>天主教</IonSelectOption>
                                <IonSelectOption>基督教</IonSelectOption>
                                <IonSelectOption>伊斯蘭教</IonSelectOption>
                                <IonSelectOption>其它</IonSelectOption>
                            </IonSelect>
                        ),
                    })}
                    {item({
                        name: 'language',
                        renderLabel: () => <Label text="語言" />,
                        renderContent: props => (
                            <IonSelect multiple={false} {...props}>
                                <IonSelectOption>廣東話</IonSelectOption>
                                <IonSelectOption>普通話</IonSelectOption>
                                <IonSelectOption>英文</IonSelectOption>
                                <IonSelectOption>法文</IonSelectOption>
                                <IonSelectOption>德文</IonSelectOption>
                                <IonSelectOption>日文</IonSelectOption>
                                <IonSelectOption>西班牙文</IonSelectOption>
                                <IonSelectOption>韓文</IonSelectOption>
                                <IonSelectOption>其它</IonSelectOption>
                            </IonSelect>
                        ),
                    })}
                    {item({
                        name: 'occupation',
                        renderLabel: () => <Label text="職業" />,
                        renderContent: props => (
                            <IonSelect  {...props}>
                                <IonSelectOption></IonSelectOption>
                                <IonSelectOption>學生</IonSelectOption>
                                <IonSelectOption>航空</IonSelectOption>
                                <IonSelectOption>法律</IonSelectOption>
                                <IonSelectOption>醫療</IonSelectOption>
                                <IonSelectOption>建築</IonSelectOption>
                                <IonSelectOption>餐飲</IonSelectOption>
                                <IonSelectOption>零售</IonSelectOption>
                                <IonSelectOption>金融</IonSelectOption>
                                <IonSelectOption>教育</IonSelectOption>
                                <IonSelectOption>體育</IonSelectOption>
                                <IonSelectOption>娛樂</IonSelectOption>
                                <IonSelectOption>製造</IonSelectOption>
                                <IonSelectOption>貿易</IonSelectOption>
                                <IonSelectOption>行政</IonSelectOption>
                                <IonSelectOption>房地產</IonSelectOption>
                                <IonSelectOption>物流運輸</IonSelectOption>
                                <IonSelectOption>資訊科技</IonSelectOption>
                                <IonSelectOption>農林牧漁</IonSelectOption>
                                <IonSelectOption>已退休</IonSelectOption>
                                <IonSelectOption>自僱</IonSelectOption>
                                <IonSelectOption>其他</IonSelectOption>
                            </IonSelect>
                        ),
                    })}
                    {item({
                        name: 'gender',
                        renderLabel: () => <Label text="性別" />,
                        renderContent: props => (
                            <IonSelect {...props}>
                                <IonSelectOption>男</IonSelectOption>
                                <IonSelectOption>女</IonSelectOption>
                                <IonSelectOption>其他</IonSelectOption>
                            </IonSelect>
                        ),
                    })}
                    {item({
                        name: 'age',
                        renderLabel: () => <Label text="年齡" />,
                        renderContent: props => (
                            <IonSelect {...props}>
                                <IonSelectOption>18-25歲</IonSelectOption>
                                <IonSelectOption>26-30歲</IonSelectOption>
                                <IonSelectOption>30-40歲</IonSelectOption>
                                <IonSelectOption>40歲或以上</IonSelectOption>
                            </IonSelect>
                        ),
                    })}
                    {item({
                        name: 'not_smoking_text',
                        renderLabel: () => <Label text="吸煙情況" />,
                        renderContent: props => (
                            <IonSelect {...props}>
                                <IonSelectOption>吸煙</IonSelectOption>
                                <IonSelectOption>不吸煙</IonSelectOption>
                            </IonSelect>
                        ),
                    })}
                    {item({
                        name: 'have_no_pet_text',
                        renderLabel: () => <Label text="是否養寵物" />,
                        renderContent: props => (
                            <IonSelect {...props}>
                                <IonSelectOption>無寵物</IonSelectOption>
                                <IonSelectOption>有寵物</IonSelectOption>
                            </IonSelect>
                        ),
                    })}
                    {item({
                        name: 'have_no_kids_text',
                        renderLabel: () => <Label text="是否有孩子" />,
                        renderContent: props => (
                            <IonSelect {...props}>
                                <IonSelectOption>無孩子</IonSelectOption>
                                <IonSelectOption>有孩子</IonSelectOption>
                            </IonSelect>
                        ),
                    })}
                </div>


            </IonContent>
            <IonFooter style={{ top: '-3rem', left: 'calc(50% - 3rem)' }}>
                <Link to='/fillIn/two' style={{ textDecoration: 'none' }}>
                    <div style={{
                        display: 'flex',
                        alignItems: 'center',
                        justifyContent: 'center',
                        left: '143px',
                        width: '6rem',
                        height: '45px',
                        backgroundColor: '#E96443',
                        borderRadius: '45px',
                        color: 'white',
                    }}
                        onClick={onClick}
                    >
                        下一頁
                    </div>
                </Link>
            </IonFooter>
        </IonPage>
    )
}


export default FillInUserInformationPageOne