import React, { MouseEvent, useEffect, useState } from 'react'
import { Link, useHistory, useLocation } from 'react-router-dom'
import SiteHeader from '../layout/SiteHeader'
import siteImg from "../../images/site.png"
import { IonPage, IonContent, IonFooter, IonItem, IonAvatar, IonImg, IonInput, IonButtons, IonButton, IonIcon } from '@ionic/react';
import './scss/SiteMeetingPageDetail.scss'
import SiteInfo from '../sites/SiteInfo';
import SiteAnttendee from '../sites/SiteAnttendee';
import SiteMessage from '../sites/SiteMessage';
import peter from "../../images/peter.jpg"
import { useDispatch, useSelector } from 'react-redux';
import { setSiteInfosThunk } from '../../redux/siteInfo/thunk';
import { IRootState } from '../../redux/store';
import { paperPlane } from 'ionicons/icons';

interface IMessage {
    "usersid": number,
    "id": number,
    "content": string,
    "nickname": string,
    "image": string,
    "msg_img": string,
    "edit": boolean,
    "del": boolean,
}

const SiteMeetingPageDetail = () => {
    const history = useHistory()
    const location = useLocation()
    const params = new URLSearchParams(location.search)
    const tab = params.get('tab')
    const siteID = params.get('siteID') || '36'
    const changeTab = (name: string) => {
        let url = location.pathname + '?siteID=' + siteID + '&tab=' + name
        console.log(url)
        history.push(url)
    }
    const dispatch = useDispatch();
    const siteInfo = useSelector((state: IRootState) => state.siteInfo)
    console.log(siteInfo);
    const googleMapURL = `http://www.google.com/maps/place/${siteInfo.map_x},${siteInfo.map_y}`
    const [content, setContent] = useState<string>("");
    const [messageDetails, setMesssageDetails] = useState<IMessage[]>([]);

    const getMessages = async (meetingID: string) => {
        const res = await fetch(process.env.REACT_APP_API_SERVER + `/messages/${meetingID}`)
        const resJson = await res.json();
        console.log(resJson.payload);
        setMesssageDetails(resJson.payload.messages);
    }
    const onSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
        
        e.preventDefault();
        await createMessages(siteInfo.meeting_id, content);
        getMessages(siteInfo.meeting_id);
        setContent('') 
    };

    useEffect(() => {
        dispatch(setSiteInfosThunk(siteID))
        getMessages(siteInfo.meeting_id);
    }, [dispatch, siteID])

    useEffect(() => {
        getMessages(siteInfo.meeting_id);
    }, [siteInfo])
    const [imageIndex, setImageIndex] = useState(0);
    const { userID, username, nickname, image } = useSelector((state: IRootState) => state.auth.payload!)


    function formatDate(date: any) { 
        try {
            date = new Date(date)
        } catch (error) {
            date = new Date()
        }
        if (!date.getTime()) {
            date = new Date()
        }
        return new Intl.DateTimeFormat('cn-HK', {
            dateStyle: 'medium',
        }).format(date)
    }

    function formatTime(date: any) {
        try {
            date = new Date(date)
        } catch (error) {
            date = new Date()
        }
        if (!date.getTime()) {
            date = new Date()
        }
        return new Intl.DateTimeFormat('cn-HK', {
            hour: 'numeric',
            minute: 'numeric',
            timeZone: 'GMT'
        }).format(date)
    }
    console.log(siteInfo.meeting_date);
    if (!siteInfo.meeting_id) {
        return <IonPage>
            <IonContent>
                Loading...
            </IonContent>
        </IonPage>
    }
    const createMessages = async (meetingID: string, content: string) => {
        const formData = new FormData();
        formData.set('siteMeetingId', meetingID);
        formData.set('content', content);

        const res = await fetch(process.env.REACT_APP_API_SERVER + "/messages", {
            method: "POST",
            headers: {
                'Authorization': 'Bearer ' + localStorage.getItem('roomie_token')
            },
            body: formData
        });
        if (res.status !== 200) {
            alert("Create Message Fail!")
        } else {
            alert("Create Message Success")
        }
    }


    return (
        <IonPage>
            <IonContent>
                <SiteHeader title={siteInfo.title} previousPage='/' />

                <div className="flatPic" style={{ zIndex: 99999 }}>

                    <img style={{ height: '240px', width: '100vw', objectFit: 'cover' }} src={process.env.REACT_APP_IMAGE_SERVER + "/" + siteInfo.siteImage[imageIndex]?.image}></img>
                    <div className="siteMeetingDiv">
                        <div>睇樓團</div>
                        <div>{formatTime(siteInfo.meeting_date)}</div>
                        <div>{formatDate(siteInfo.meeting_date)}</div>

                    </div>
                    <div className="siteImageMapDiv" >
                        <a href={googleMapURL} target="_blank">
                            <i className="fas fa-map-marker-alt"></i>
                        </a>
                    </div>
                    <img className='siteOwnerDiv' src={process.env.REACT_APP_IMAGE_SERVER + "/" + siteInfo.image}></img>
                    <div className="siteImageNavDiv">
                        <div onClick={() => { setImageIndex(0) }} className={`siteImageNav ${imageIndex == 0 ? 'active' : ''}`}></div>
                        <div onClick={() => { setImageIndex(1) }} className={`siteImageNav ${imageIndex == 1 ? 'active' : ''}`} ></div>
                        <div onClick={() => { setImageIndex(2) }} className={`siteImageNav ${imageIndex == 2 ? 'active' : ''}`}></div>
                        <div onClick={() => { setImageIndex(3) }} className={`siteImageNav ${imageIndex == 3 ? 'active' : ''}`}></div>
                    </div>
                </div>
                <div className="siteNavBar" style={{ zIndex: 99999 }}>
                    <div className={tab == 'siteInfo' ? 'active nav' : 'nav'} onClick={() => changeTab('siteInfo')}>樓盤資訊</div>
                    <div className={tab == 'siteAnttendee' ? 'active nav' : 'nav'} onClick={() => changeTab('siteAnttendee')}>出席者</div>
                    <div className={tab == 'siteMessage' ? 'active nav' : 'nav'} onClick={() => changeTab('siteMessage')}>留言區</div>
                </div>
                <div className="PlaceHolderForNavBarAndFlatPic" style={{ height: '345px', width: '100%', background: 'white', zIndex: 99998 }}></div>
                {(function () {
                    switch (tab) {
                        case 'siteInfo':
                            return <SiteInfo siteInfo={siteInfo} />
                        case 'siteAnttendee':
                            return <SiteAnttendee siteID={siteID} meetingID={siteInfo.meeting_id} meetingDate={siteInfo.meeting_date} />
                        case 'siteMessage':
                            return <SiteMessage messages={messageDetails} />
                        case '':
                            return <SiteInfo siteInfo={siteInfo} />

                    }
                })()}
            </IonContent>
            {tab == 'siteMessage' &&
                <IonFooter>
                    <IonItem>
                        <IonAvatar style={{ width: '40px', height: '40px' }} slot='start'>
                            <IonImg src={process.env.REACT_APP_IMAGE_SERVER + "/" + image}></IonImg>
                        </IonAvatar>
                        <IonInput onIonChange={(e: Event) => {
                            setContent((e.target as HTMLInputElement).value)
                        }} value={content} placeholder='Please input here' style={{ borderBottom: '1px solid' }}></IonInput>
                        <IonButtons slot='end'>
                            <IonButton onClick={(e: any) => onSubmit(e)} color="dark">
                                <IonIcon icon={paperPlane}></IonIcon>
                            </IonButton>
                        </IonButtons>
                    </IonItem>
                </IonFooter>
            }
        </IonPage>)
}

export default SiteMeetingPageDetail
