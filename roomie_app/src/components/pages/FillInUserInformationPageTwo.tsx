import React from 'react';
import { Link, useHistory } from 'react-router-dom'
import {
    IonPage,
    IonContent,
    IonLabel,
    IonInput,
    IonHeader,
    IonToolbar,
    IonTitle,
    IonFooter,
    IonSelectOption,
    IonSelect
} from '@ionic/react';
import { useIonFormState } from 'react-use-ionic-form'
import './scss/SignUp.scss'
import { JWTPayload } from '../../redux/types';

function Label({ text }: { text: string }) {
    return (
        <IonLabel
            color="primary"
            position="floating"
            style={{ textTransform: 'capitalize' }}
        >
            {text}
        </IonLabel>
    )
}

async function updateUserInformation(info: {
    nickname?: string;
    ethnicity?: string;
    religion?: string;
    language?: string;
    occupation?: string;
    gender?: string;
    age?: string;
    not_smoking_text?: string;
    have_no_pet_text?: string;
    have_no_kids_text?: string;
    rent_duration?: string;
    rent_date?: string;
    introduction?: string;
    budget_min?: number;
    budget_max?: number;
    preferred_location?: string


}) {
    try {
        const host = process.env.REACT_APP_API_SERVER
        const res = await fetch(`${host}/user/update`, {
            method: 'PUT',
            headers: {
                "Authorization": "Bearer " + localStorage.getItem('roomie_token'),
                "Content-Type": "application/json",
            },
            body: JSON.stringify({
                info
            })
        })
        if (res.status == 200) {
            console.log('success')
        }
    } catch (err) {
        console.error(err.message)
    }

}

const FillInUserInformationPageTwo = () => {
    const history = useHistory()
    const userInfoObj = JSON.parse(localStorage.getItem('userInfo')!)
    const userInfo = userInfoObj.userData
    const image = userInfo.image
    const { state, item } = useIonFormState({
        rent_duration: userInfo.rent_duration || '',
        rent_date: userInfo.rent_date || '',
        budget_min: userInfo.budget_min || '',
        budget_max: userInfo.budget_max || '',
        introduction: userInfo.introduction || '',
        preferred_location: userInfo.preferred_location || ''
    })
    const onClick = () => {
        const info = state
        updateUserInformation(info)
        console.log(image)
        if(image === 'testingBackground.jpg'){
            history.push('/uploadIcon')
        }else{
            history.push('/')
        }
        
    }
    return (
        <IonPage className='SignUpPageThree'>
            <IonHeader>
                <IonToolbar className='rainbow-bg'>
                    <IonTitle >填寫/更改用戶資料</IonTitle>

                </IonToolbar>
                <div className='stepper'>
                    <div className='circle'></div>
                    <div className='circle circle-active'></div>
                </div>
            </IonHeader>
            <IonContent>
                <div id='rentDuration' style={{ marginTop: '5rem', width: "100vw", }}>
                    {item({
                        name: 'rent_duration',
                        renderLabel: () => <Label text="租住月數" />,

                        renderContent: props => <IonInput type="number" {...props} />,
                    })}
                    {item({
                        name: 'rent_date',
                        renderLabel: () => <Label text="起租日期" />,
                        renderContent: props => <IonInput type="date" {...props} />,
                    })}
                     {item({
                        name: 'budget_min',
                        renderLabel: () => <Label text="最低租金預算" />,
                        renderContent: props => <IonInput style={{ height: '500px' }} type="number" {...props} />,
                    })}
                     {item({
                        name: 'budget_max',
                        renderLabel: () => <Label text="最高租金預算" />,
                        renderContent: props => <IonInput style={{ height: '500px' }} type="number" {...props} />,
                    })}　　　　
                    {item({
                        name: 'preferred_location',
                        renderLabel: () => <Label text="希望租住地區" />,
                        renderContent: props => (
                            <IonSelect  {...props}>
                                <IonSelectOption></IonSelectOption>
                                <IonSelectOption>中西區</IonSelectOption>
                                <IonSelectOption>東區</IonSelectOption>
                                <IonSelectOption>南區</IonSelectOption>
                                <IonSelectOption>灣仔區</IonSelectOption>
                                <IonSelectOption>九龍城區</IonSelectOption>
                                <IonSelectOption>觀塘區</IonSelectOption>
                                <IonSelectOption>深水埗區</IonSelectOption>
                                <IonSelectOption>黃大仙區</IonSelectOption>
                                <IonSelectOption>油尖旺區</IonSelectOption>
                                <IonSelectOption>葵青區</IonSelectOption>
                                <IonSelectOption>北區</IonSelectOption>
                                <IonSelectOption>西貢區</IonSelectOption>
                                <IonSelectOption>大埔區</IonSelectOption>
                                <IonSelectOption>沙田區</IonSelectOption>
                                <IonSelectOption>荃灣區</IonSelectOption>
                                <IonSelectOption>屯門區</IonSelectOption>
                                <IonSelectOption>元朗區</IonSelectOption>
                                <IonSelectOption>離島區</IonSelectOption>
                                
                            </IonSelect>
                        ),
                    })}
                    {item({
                        name: 'introduction',
                        renderLabel: () => <Label text="關於" />,
                        renderContent: props => <IonInput style={{ height: '500px' }} type="text" {...props} />,
                    })}

                </div>


            </IonContent>
            <IonFooter style={{ top: '-3rem', left: 'calc(50% - 3rem)' }}>
                    <div style={{
                        display: 'flex',
                        alignItems: 'center',
                        justifyContent: 'center',
                        left: '143px',
                        width: '6rem',
                        height: '45px',
                        backgroundColor: '#E96443',
                        borderRadius: '45px',
                        color: 'white',
                    }}
                    onClick ={onClick}
                    >
                        保存
                    </div>
            </IonFooter>
        </IonPage>
    )
}


export default FillInUserInformationPageTwo