import React from 'react'
import { Link } from 'react-router-dom'

const AttendeeOnList = (props: { img: any; name: any; isAttended: any; pastEvent: any; }) => {
    const { img, name, isAttended, pastEvent } = props;
    return (<>
        <div style={{ width: '100vw', display: 'flex', justifyContent: 'center' }}>
            <div className='attendeeList' style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center', width: '330px', paddingTop: '15px' }}>
                <img style={{ height: '45px', width: '45px', objectFit: 'cover', borderRadius: '50px' }} src={process.env.REACT_APP_IMAGE_SERVER + "/" + img}></img>
                <div id='attendeeName'>{name}</div>
                {pastEvent ? '' : <div id='isAttended' style={{ backgroundColor: 'purple', height: '25px', width: '70px', borderRadius: '40px', padding: '3px', color: 'white', textAlign: 'center' }}>
                    未出席
                </div>}

                {pastEvent && (isAttended ? <div id='isAttended' style={{ backgroundColor: 'green', height: '25px', width: '70px', borderRadius: '40px', padding: '3px', color: 'white', textAlign: 'center' }}>
                    出席
                </div>
                    : <div id='isAttended' style={{ backgroundColor: 'red', height: '25px', width: '70px', borderRadius: '40px', padding: '3px', color: 'white', textAlign: 'center' }}>
                        缺席
                    </div>)}

            </div>
        </div>

    </>

    )
}

export default AttendeeOnList
