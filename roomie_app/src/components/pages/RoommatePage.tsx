import React, { useEffect, useState } from 'react';
import Navbar from '../layout/NavBar';
import SearchBar from '../layout/SearchBar';
import RoommateFilter from '../layout/RoommateFilter';
import Roommates from '../roommate/Roommates'
import { IonPage, IonContent, IonHeader, IonFooter } from '@ionic/react';
import { people } from 'ionicons/icons';
import { useDispatch, useSelector } from 'react-redux';
import './scss/RoommatePage.scss'
import { searchRoommateAction } from '../../redux/roommateFilter/action';
import { queryRoommateThunk } from '../../redux/roommateFilter/thunk';
import { IRootState } from '../../redux/store';


const RoommatePage = () => {
    const dispatch = useDispatch()
    const queryState = useSelector((state: IRootState) => state.roommateFilter.query)
    const ethnicity = useSelector((state: IRootState) => state.roommateFilter.query?.ethnicity)
    const religion = useSelector((state: IRootState) => state.roommateFilter.query?.religion)
    const language = useSelector((state: IRootState) => state.roommateFilter.query?.language)
    const occupation = useSelector((state: IRootState) => state.roommateFilter.query?.occupation)
    const rentRange = useSelector((state: IRootState) => state.roommateFilter.query?.rentRange)
    const preferredLocation = useSelector((state: IRootState) => state.roommateFilter.query?.preferredLocation)
    const gender = useSelector((state: IRootState) => state.roommateFilter.query?.gender)
    const rentDate = useSelector((state: IRootState) => state.roommateFilter.query?.rentDate)
    const rentDuration = useSelector((state: IRootState) => state.roommateFilter.query?.rentDuration)
    const specialList = useSelector((state: IRootState) => state.roommateFilter.query?.specialRequirementList)
    const ageList = useSelector((state: IRootState) => state.roommateFilter.query?.ageList)
    useEffect(() => {
        dispatch(queryRoommateThunk({
            ...queryState, ethnicity: ethnicity, religion: religion, language: language, occupation: occupation,
            preferredLocation: preferredLocation, gender: gender

        }))
    }, [queryState])
    const [roommateSearch, setRoommateSearch] = useState('')

    const placeHolderText: string = '尋找室友..'
    const icon = people
    const searchChange = (text: string) => {
        dispatch(searchRoommateAction(text))

    }
    return (
        <IonPage>

            <IonHeader>
                <SearchBar placeHolderText={placeHolderText} icon={icon} searchChange={searchChange} />

            </IonHeader>
            <IonContent className='ion-padding'>


                <RoommateFilter />
                <div className='roomate-card'>
                    <Roommates />
                </div>

            </IonContent>
            <IonFooter>
                <Navbar />
            </IonFooter>
        </IonPage>
    )
}

export default RoommatePage