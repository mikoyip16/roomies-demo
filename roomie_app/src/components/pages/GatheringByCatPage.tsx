import React from 'react';
import { Link, useHistory, useLocation } from 'react-router-dom';
import Navbar from '../layout/NavBar';
import UserIcon from '../layout/UserIcon';
import Notification from '../layout/Notification';
import Gatherings from '../gatherings/Gatherings'
import { IonContent, IonHeader, IonFooter, IonPage, IonTitle, IonToolbar, IonFab, IonFabButton, IonIcon, IonFabList, IonRippleEffect } from '@ionic/react';
import { add, settings, share, person, arrowForwardCircle, arrowBackCircle, arrowUpCircle, logoVimeo, logoFacebook, logoInstagram, logoTwitter } from 'ionicons/icons';
import './scss/Gathering.scss'
import headerIconRoomies from '../../images/headerIconRoomies.svg'
import HeaderBar from '../layout/HeaderBar';


const GatheringByCatPage = () => {
    const history = useHistory()
    const location = useLocation()
    return (
        <IonPage>
            <IonHeader>
             <HeaderBar />
             </IonHeader>
            <IonContent>
               
                <div>
                    <Gatherings />
                </div>
            </IonContent>
            <IonFooter>
                <Navbar />
            </IonFooter>
            <Link to='/create-event'>
                <IonFab className='ion-margin-bottom' vertical="bottom" horizontal="end" slot="fixed">
                    <IonFabButton style={{ fontWeight: 'bold', fontSize: '2em' }} color="secondary">
                        <IonIcon icon={add} />
                    </IonFabButton>
                </IonFab>
            </Link>
        </IonPage>
    )
}

export default GatheringByCatPage