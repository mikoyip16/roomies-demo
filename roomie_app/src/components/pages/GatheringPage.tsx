import React from 'react';
import { Link } from 'react-router-dom';
import Navbar from '../layout/NavBar';
import UserIcon from '../layout/UserIcon';
import Notification from '../layout/Notification';
import Gatherings from '../gatherings/Gatherings'
import { IonContent, IonHeader, IonFooter, IonPage, IonTitle, IonToolbar, IonFab, IonFabButton, IonIcon, IonFabList, IonRippleEffect } from '@ionic/react';
import { add, settings, share, person, arrowForwardCircle, arrowBackCircle, arrowUpCircle, logoVimeo, logoFacebook, logoInstagram, logoTwitter } from 'ionicons/icons';
import './scss/Gathering.scss'
import headerIconRoomies from '../../images/headerIconRoomies.svg'
import HeaderBar from '../layout/HeaderBar';


const GatheringPage = () => {
    return (
        <div>
            <Gatherings />
        </div>
    )
}

export default GatheringPage