import React, { useEffect } from 'react';
import { Link, useHistory, useLocation } from 'react-router-dom'
import {
    IonPage,
    IonContent,
    IonHeader,
    IonFooter
} from '@ionic/react';
import './scss/SignUp.scss'
import { useDispatch, useSelector } from 'react-redux';
import { loginSuccessAction } from '../../redux/auth/action';
import { getLoginUserInfoThunk } from '../../redux/roommateInfo/thunk';
import { IRootState } from '../../redux/store';




const SignUpPageSuccess = () => {
    const history = useHistory()
    const location = useLocation()
    const params = new URLSearchParams(location.search)
    const token = params.get('jwt_token')
    const localSignUptoken = localStorage.getItem('roomie_token');
    const userID = JSON.stringify(useSelector((state: IRootState) => state.auth.payload?.userID))
    const dispatch = useDispatch()
    
    useEffect(() => {
        if(localSignUptoken){
            dispatch(getLoginUserInfoThunk(userID))
        }
        else if (token) {
            localStorage.setItem('roomie_token', token);
            dispatch(loginSuccessAction(token));
            dispatch(getLoginUserInfoThunk(userID))
            history.push("/signUp/success")
        }
        
    }, [])

    const userInfo = JSON.stringify(useSelector((state: IRootState) => state.roommateInfo.loginUserInfo)!)
    if (userInfo) {
        localStorage.setItem('userInfo', userInfo)
    }
    return (
        <IonPage className='SignUpPageOne'>
            <IonHeader>
            </IonHeader>
            <IonContent>
                <div style={{ fontSize: '36px', width: '50vw', height: '275px', marginTop: '13rem', marginLeft: '25vw', textAlign: 'center' }}>
                    <div style={{ marginTop: '1rem' }}>
                        恭喜,
                    </div>

                    <div style={{ marginTop: '1rem' }}>
                        成功註冊!
                    </div>

                </div>


            </IonContent>
            <IonFooter style={{ top: '-3rem', left: 'calc(50% - 4.5rem)' }}>
                <Link to='/fillIn/one' style={{ textDecoration: 'none', }}>
                    <div style={{
                        display: 'flex',
                        alignItems: 'center',
                        justifyContent: 'center',

                        width: '9rem',
                        height: '45px',
                        backgroundColor: '#E96443',
                        borderRadius: '45px',
                        color: 'white',
                    }}
                    >
                        填寫用戶資料
                    </div>
                </Link>
            </IonFooter>
        </IonPage>
    )
}


export default SignUpPageSuccess