import React, { FormEvent, useState } from 'react';
import { facebookLoginThunk, nativeLoginThunk } from '../../redux/auth/thunk'
import { IRootState } from '../../redux/store';
import { useDispatch, useSelector } from 'react-redux';
import { Redirect, useHistory } from 'react-router';
import { Link } from 'react-router-dom';
import roomieLogo from '../../roomie-icon-login.png'
import { IonPage, IonContent, IonHeader, IonIcon, IonNote } from '@ionic/react';
import { chevronBackOutline, logoFacebook } from 'ionicons/icons';
import { History } from 'history';
import './scss/LoginPage.scss';
import { Alert } from 'reactstrap';



const LoginPage = () => {

    const history = useHistory()
    const isLoading = useSelector((state: IRootState) => state.auth.isLoading)
    const isAuthenticated = useSelector(
        (state: IRootState) => state.auth.isAuthenticated,
    )
    const errorMessage = useSelector(
        (state: IRootState) => state.auth.errorMessage
    )
    const [username, setUsername] = useState('')
    const [password, setPassword] = useState('')
    const dispatch = useDispatch()

    if (isAuthenticated) {
        return <Redirect to='/' />
    }

    function submit(event: FormEvent) {
        event.preventDefault()
        console.log(submit)
        dispatch(nativeLoginThunk(username, password, history))
    }



    return (
        <IonPage>
            <IonHeader>
                <div style={{
                    display: 'flex',
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    margin: '1rem',
                    alignItems: 'center'
                }}>

                    <Link to='/' style={{
                        color: 'black', display: 'flex',
                        flexDirection: 'row',
                        alignItems: 'center'
                    }}>

                        <IonIcon size='large' icon={chevronBackOutline}></IonIcon>

                        首頁
                    </Link>

                </div>

            </IonHeader>
            <IonContent>
                <div style={{ marginLeft: '2rem' }}>
                    <div style={{ marginBottom: '1rem', fontSize: '20px' }}>
                        Welcom to Roomies,
                    </div>
                    <div style={{ marginBottom: '1rem', fontSize: '16px', }}>
                        <Link to='/signUp' style={{ textUnderlinePosition: 'under', fontWeight: 'bold' }}>Sign Up</Link> Now!
                    </div>
                </div>
                <div style={{ display: 'flex', justifyContent: 'center', marginTop: '3rem' }}>
                    <img src={roomieLogo} alt='' style={{ width: 'auto', height: '10rem', left: '50%' }}></img>
                </div>


                <form className='login-form' onSubmit={submit}>
                    <div className='form-group'>


                        <input type='text' name='name' required value={username} placeholder='用戶名' onChange={(e) => { setUsername(e.target.value) }} />
                    </div>
                    <div className='form-group'>


                        <input type='password' name='password' required value={password} placeholder='密碼' onChange={(e) => { setPassword(e.target.value) }} />

                    </div>
                    {errorMessage ?
                        <Alert color="red">
                            {errorMessage}
                        </Alert> : ""
                    }

                    <button type='submit' >
                        登入
                    </button>
                </form>

                <div className='login-icon'>
                    <a href="https://t.me/Roomiesss_bot?start=roomiesLogin" target="_blank"><div style={{ display: 'flex', width: '32px', height: '32px', backgroundColor: 'lightblue', borderRadius: '16px', justifyContent: 'center', alignItems: 'center' }}>
                        <i className="fab fa-telegram-plane" style={{ color: 'white', fontSize: '22px' }}></i>
                    </div></a>
                </div>

            </IonContent>
        </IonPage>
    )
}


export default LoginPage