import React, { useState } from 'react'
import { Link, useHistory } from 'react-router-dom'
import gamesSVG from "../../images/gatheringCat/games.svg"
import hobbiesSVG from "../../images/gatheringCat/hobbies.svg"
import musicSVG from "../../images/gatheringCat/music.svg"
import petsSVG from "../../images/gatheringCat/pets.svg"
import spiritualSVG from "../../images/gatheringCat/spiritual.svg"
import sportsSVG from "../../images/gatheringCat/sports.svg"
import othersSVG from "../../images/gatheringCat/others.svg"
import { IonContent, IonHeader, IonFooter, IonPage, IonTitle, IonToolbar, IonFab, IonFabButton, IonIcon, IonFabList, IonRippleEffect, useIonAlert } from '@ionic/react';
import { add, settings, share, person, arrowForwardCircle, arrowBackCircle, arrowUpCircle, logoVimeo, logoFacebook, logoInstagram, logoTwitter } from 'ionicons/icons';
import './scss/Gathering.scss'
import HeaderBar from '../layout/HeaderBar'
import Navbar from '../layout/NavBar'


const GatheringCatPage = () => {
    const history = useHistory();
    const [present] = useIonAlert();

    const checkHaveToken = () => {
        if (localStorage.getItem('roomie_token')) history.push('/create-event');
        else present("請先登入", [{ text: 'Ok' }])
    }

    const [type, setType] = useState("")
    return (
        <IonPage>
            <IonContent>
                <IonHeader>
                    <HeaderBar />
                </IonHeader>
                {/* {type ==} */}
                <div className="eventContainer">
                    <Link to='/gatheringList?type=運動與健身' >
                        <img style={{ 'marginBottom': '10px' }} src={sportsSVG}></img>
                    </Link>
                    <Link to='/gatheringList?type=遊戲' style={{ background: 'white' }} >
                        <img style={{ 'marginBottom': '10px' }} src={gamesSVG}></img>
                    </Link>
                    <Link to='/gatheringList?type=愛好' >
                        <img style={{ 'marginBottom': '10px' }} src={hobbiesSVG}></img>
                    </Link>
                    <Link to='/gatheringList?type=音樂' >
                        <img style={{ 'marginBottom': '10px' }} src={musicSVG}></img>
                    </Link>
                    <Link to='/gatheringList?type=音樂' >
                        <img style={{ 'marginBottom': '10px' }} src={petsSVG}></img>
                    </Link>
                    <Link to='/gatheringList?type=宗教與靈修' >
                        <img style={{ 'marginBottom': '10px' }} src={spiritualSVG}></img>
                    </Link>
                    <Link to='/gatheringList?type=其他' >
                        <img style={{ 'marginBottom': '10px' }} src={othersSVG}></img>
                    </Link>
                </div>
            </IonContent>
            <IonFooter>
                <Navbar />
            </IonFooter>
            <div onClick={() => {
                checkHaveToken();
            }}>
                <IonFab className='ion-margin-bottom' vertical="bottom" horizontal="end" slot="fixed">
                    <IonFabButton style={{ fontWeight: 'bold', fontSize: '2em' }} color="secondary">
                        <IonIcon icon={add} />
                    </IonFabButton>
                </IonFab>
            </div>
        </IonPage>
    )
}

export default GatheringCatPage;
