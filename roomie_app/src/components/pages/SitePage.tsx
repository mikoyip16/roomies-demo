import React, { useEffect, useState } from 'react';
import Navbar from '../layout/NavBar';
import SearchBar from '../layout/SearchBar';
import SiteFilter from '../layout/SiteFilter';
import Sites from '../sites/Sites'
import { compassSharp } from 'ionicons/icons';
import { IonPage, IonContent, IonFooter, IonHeader, IonToolbar } from '@ionic/react';
import './scss/SitePage.scss'
import { useDispatch, useSelector } from 'react-redux';
import { searchSiteAction } from '../../redux/siteFilter/action';
import { IRootState } from '../../redux/store';
import { querySiteThunk } from '../../redux/siteFilter/thunk';


const SitePage = () => {
    const dispatch = useDispatch()
    const queryState = useSelector((state: IRootState) => state.siteFilter.query)
    const rangeList = useSelector((state: IRootState) => state.siteFilter.query?.rangeList)
    const rentRange = useSelector((state: IRootState) => state.siteFilter.query?.rentRange)
    const area = useSelector((state: IRootState) => state.siteFilter.query?.area)
    const compartment = useSelector((state: IRootState) => state.siteFilter.query?.compartment)
    const requirementList = useSelector((state: IRootState) => state.siteFilter.query?.requirementList)
    const specialListList = useSelector((state: IRootState) => state.siteFilter.query?.specialListList)
    useEffect(() => {
        dispatch(querySiteThunk({
            ...queryState, rangeList: rangeList, rentRange: rentRange, area: area,
            compartment: compartment, requirementList: requirementList, specialListList: specialListList
        }))
    })


    const placeHolderText: string = '尋找心水樓盤..'
    const icon = compassSharp
    const searchChange = (text: string) => {
        dispatch(searchSiteAction(text))
    }
    


    return (
        <IonPage>
            <IonHeader className='ion-padding-bottom'>

                    <SearchBar placeHolderText={placeHolderText} icon={icon} searchChange={searchChange} />
                
            </IonHeader>
            <IonContent className='ion-padding'>



                
                <SiteFilter />
                <div className='site-card'>
                    <Sites />
                </div>
            </IonContent>
            <IonFooter>
                <Navbar />
            </IonFooter>
        </IonPage>
    )
}

export default SitePage