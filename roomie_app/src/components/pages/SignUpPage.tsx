import React from 'react';
import { Link, useHistory } from 'react-router-dom'
import {
    IonPage,
    IonContent,
    IonLabel,
    IonInput,
    IonHeader,
    IonToolbar,
    IonTitle,
    IonFooter
} from '@ionic/react';
import { useIonFormState } from 'react-use-ionic-form'
import './scss/SignUp.scss'
import { useDispatch } from 'react-redux';
import { registerThunk } from '../../redux/register/thunk';

function Label({ text }: { text: string }) {
    return (
        <IonLabel
        color="roomie"
            position="floating"
            style={{ textTransform: 'capitalize' }}
        >
            {text}
        </IonLabel>
    )
}


const SignUpPage = () => {
    const { state, item } = useIonFormState({
        email: '',
        username: '',
        nickname: '',
        password: '',
        
    })
    const dispatch = useDispatch()
    const history = useHistory()
    return (
        <IonPage className='SignUpPageOne'>
            <IonHeader>
                <IonToolbar className='rainbow-bg'>
                    <IonTitle >註冊新用戶</IonTitle>
                    
                </IonToolbar>
                
            </IonHeader>
            <IonContent>
                <div style={{ width: "98vw", marginTop: '5rem' }}>

                   
                    {item({
                        name: 'username',
                        renderLabel: () => <Label text="用戶名" />,
                        renderContent: props => <IonInput type="text" {...props} />,
                    })}
                      {item({
                        name: 'nickname',
                        renderLabel: () => <Label text="暱稱" />,
                        renderContent: props => <IonInput type="text" {...props} />,
                    })}
                    {item({
                        name: 'email',
                        renderLabel: () => <Label text="電郵" />,
                        renderContent: props => <IonInput type="email" {...props} />,
                    })}
                    {item({
                        name: 'password',
                        renderLabel: () => <Label text="密碼" />,
                        renderContent: props => <IonInput type="password" {...props} />,
                    })}

                </div>
                

            </IonContent>
            <IonFooter style={ {top: '-3rem', left: 'calc(50% - 3rem)'}}>
            <Link to='/signUp/success' style={{ textDecoration: 'none' }}>
            <div style={{
                    display: 'flex',
                    alignItems: 'center',
                    // position: 'absolute',
                    justifyContent: 'center',
                    // top: '600px',
                    left: '143px',
                    width: '6rem',
                    height: '45px',
                    backgroundColor: '#E96443',
                    borderRadius: '45px',
                    color: 'white',
                }}
                onClick={() => dispatch(registerThunk(state.username, state.password, state.email, state.nickname, history))}
                >
                    註冊
                </div>
                </Link>
                </IonFooter>
        </IonPage>
    )
}


export default SignUpPage