import React, { MouseEvent, useEffect, useState } from 'react'
import { Link, useHistory, useLocation } from 'react-router-dom'
import SiteHeader from '../layout/SiteHeader'
import { IonPage, IonContent, IonFooter } from '@ionic/react';
import './scss/SiteMeetingPageDetail.scss'
import GatheringInfo from '../gatherings/GatheringInfo';
import peter from "../../images/peter.jpg"
import GatheringAnttendee from '../gatherings/GatheringAnttendee';
import GatheringMessage from '../gatherings/GatheringMessage';
import { useSelector } from 'react-redux';
import { IRootState } from '../../redux/store';
import Navbar from '../layout/NavBar';
import NotificationBox from '../NotificationBox';


const NotificationPage = () => {
    const history = useHistory()
    const location = useLocation()
    const [notifications, setNotifications] = useState<Array<any>>([]);


    function formatDate(date: any) { 
        try {
            date = new Date(date)
        } catch (error) {
            date = new Date()
        }
        if (!date.getTime()) {
            date = new Date()
        }
        return new Intl.DateTimeFormat('cn-HK', {
           
            dateStyle: 'medium',
        }).format(date)
    }

    function formatTime(date: any) { 
        try {
            date = new Date(date)
        } catch (error) {
            date = new Date()
        }
        if (!date.getTime()) {
            date = new Date()
        }
        return new Intl.DateTimeFormat('cn-HK', {
            hour: 'numeric',
            minute: 'numeric',
            // dateStyle: 'medium',
            timeZone: 'GMT'
        }).format(date)
    }

    const getNotifications = async () => {

        const res = await fetch(process.env.REACT_APP_API_SERVER + "/user/notifications", {
            method: "GET",
            headers: {
                'Authorization': 'Bearer ' + localStorage.getItem('roomie_token')
            },
        });
        const resJson = await res.json();
        console.log(resJson.payload.notifications)
        setNotifications(resJson.payload.notifications);
    }

    const { userID, username, nickname, image } = useSelector((state: IRootState) => state.auth.payload!)
    useEffect(() => {
        getNotifications();
    }, [])

    return (
        <IonPage>
            <IonContent>
                <SiteHeader title={'消息通知'} previousPage='/' />
                <div style={{ height: '35px' }}></div>
                {notifications.map((notification: any) => {
                    return <NotificationBox senderImage={notification.image} senderNickname={notification.nickname} message={notification.message} targetURL={notification.target_url} date={formatDate(notification.created_at)} />
                })}
               
            </IonContent>
        </IonPage>)
}

export default NotificationPage