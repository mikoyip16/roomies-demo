import React, { useEffect, useState } from 'react';
import Sites from '../sites/Sites';
import SitesHome from '../sites/SitesHome';
import Navbar from '../../components/layout/NavBar';
import HeaderBar from '../../components/layout/HeaderBar';
import "./scss/HomePage.scss";
import { IonPage, IonContent, IonHeader, IonFooter, IonToolbar } from '@ionic/react';
import GatheringPage from './GatheringPage';
import { useHistory, useLocation } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { loginSuccessAction } from '../../redux/auth/action';
import { getLoginUserInfoThunk, getUserbyIDThunk } from '../../redux/roommateInfo/thunk';
import { IRootState } from '../../redux/store';
import { displayAllRoommateThunk } from '../../redux/roommatePage/thunk';
import { env } from 'process';




const HomePage = () => {
    const history = useHistory()
    const location = useLocation()
    const params = new URLSearchParams(location.search)
    const token = params.get('jwt_token')
    const dispatch = useDispatch()
    const [showFuture, setShowFuture] = useState("showSite")
    const [siteBtn, setSiteBtn] = useState("active")
    const [gatherBtn, setGatherBtn] = useState("non-active")

    const userID = JSON.stringify(useSelector((state: IRootState) => state.auth.payload?.userID))

    useEffect(() => {
        dispatch(getLoginUserInfoThunk(userID))

    }, [])

    const isAuthenticated = useSelector((state: IRootState) => state.auth.isAuthenticated);
    const userInfo = JSON.stringify(useSelector((state: IRootState) => state.roommateInfo.loginUserInfo)!)
    if (userInfo) {
        localStorage.setItem('userInfo', userInfo)
    }

    if (!isAuthenticated) {
        localStorage.removeItem('userInfo')
    }

    const siteVisit = () => {
        setShowFuture("showSite")
        setSiteBtn("active")
        setGatherBtn("non-active")
    }

    const gathering = () => {
        setShowFuture("showGathering")
        setGatherBtn("active")
        setSiteBtn("non-active")
    }
    useEffect(() => {
        if (token) {
            // alert("u logged in by tg")
            localStorage.setItem('roomie_token', token);
            dispatch(loginSuccessAction(token));
            history.push("/")
        }
    }, [token])
    return (
        <IonPage>

            <IonHeader >

                <HeaderBar />


            </IonHeader>
            <IonContent>
                <div className='suggestion'>
                    <div className={"future-site-visit " + siteBtn} onClick={siteVisit}>
                        未來睇樓團
                    </div>
                    <div className={"future-gathering " + gatherBtn} onClick={gathering}>
                        未來聚會
                    </div>
                </div>



                {showFuture === "showSite" && (<div className='future-detail ion-padding'><SitesHome /></div>)}
                {showFuture === "showGathering" && (<div className='future-activities ion-padding'><GatheringPage /></div>)}

            </IonContent>
            <IonFooter>
                <Navbar />
            </IonFooter>
        </IonPage>
    )
}

export default HomePage


