import { IonButton } from '@ionic/react';
import React from 'react'
import { Link } from 'react-router-dom'
import peter from '../../images/peter.jpg'

const NotificationBox = (props: { senderImage: any, senderNickname: any; message: any; targetURL: any; date: any }) => {
    const { senderImage, senderNickname, message, targetURL, date } = props;
    return (<>

        <div className='attendeeList' style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center', width: '100%', paddingTop: '15px' }}>
            <img style={{ marginLeft: '10px', height: '45px', width: '45px', objectFit: 'cover', borderRadius: '50px' }} src={process.env.REACT_APP_IMAGE_SERVER + "/" + senderImage}></img>
            <div id='attendeeName' style={{ flex: '3', paddingLeft: '10px', paddingRight: '10px', height: '100%', overflow: 'auto' }}><span style={{ fontWeight: 'bold' }}>{senderNickname} </span>{message} <span style={{ fontSize: '13px', color: 'gray' }}>{date}</span></div>
            <Link to={targetURL}><IonButton style={{ width: '45px', height: '25px', borderRadius: '0', color: 'white', marginRight: '10px' }}>睇睇</IonButton></Link>
        </div>


    </>

    )
}

export default NotificationBox