import React from 'react'
import peter from "../images/peter.jpg"

const CommentBox = (props: { nickname: any, img: any; content: any, ref_?: any }) => {
    const { nickname, img, content, ref_ } = props
    return (
        <div ref={ref_} className="commentArea" style={{ paddingLeft: '20px', display: 'flex', marginTop: '10px' }}>
            <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                <img id='commentAreaIcon' style={{ height: '50px', width: '50px', objectFit: 'cover', borderRadius: '50px' }} src={process.env.REACT_APP_IMAGE_SERVER + "/" + img}></img>
                <span style={{ textAlign: 'center', textOverflow: 'ellipsis', whiteSpace: 'nowrap', overflow: 'hidden', width: '60px', fontSize: '9px', color: 'gray' }}>{nickname}</span>
            </div>
            <div id='commentBoxContent' style={{ filter: 'drop-shadow(0px 4px 4px rgba(0, 0, 0, 0.25))', backgroundColor: '#EAECED', marginLeft: '20px', padding: '15px', borderRadius: '7px' }}>{content}</div>
        </div>
    )
}

export default CommentBox