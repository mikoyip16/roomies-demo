import React from 'react'
import { Link } from 'react-router-dom'

import { SitePageData } from '../../redux/types'
import url from '../../uploads/1-1.jpg'
import { toUrl } from '../../helpers/api'
import './scss/SiteItem.scss'

const SiteItem = (props: {
    id?: number
    image?: string
    title?: string
    rooms?: number
    usable_area?: number
    floor?: number
    years?: number
    district?: string
    address?: string
    rent?: number
    owner_id?: number
    updated_at?: Date
    date?: Date
   }
) => {
    const { id, image, title, rooms, usable_area, floor, years, district, address, rent, owner_id, updated_at, date} = props
    const siteImgUrl =  toUrl(`/${image}`)
    
    return (
        <Link to={ `/site/detail?siteID=${id}&tab=siteInfo` } className='card-container'>
            <div className='site-card-container'>
                <div style={{
                    width: "32vw",
                    height: "32vw",
                    overflow: "hidden",
                    backgroundImage: `url(${siteImgUrl})`,
                    backgroundSize: 'cover',
                    backgroundPosition: 'center'
                }} className='site-img' ></div>
                <div className='description' style={{
                    
                    height: '32vw',
                    width: '56vw'
                    }}>
                        <div className='bold-tit padding'>{title}</div>
                        <div className='rent-ora padding'>${rent}/月</div>
                        <div className='grey-des '>樓齡{years}年{' '}<span className='vertical-ora'>|</span>{' '}{usable_area}呎{' '}<span className='vertical-ora'>|</span>{' '}32/F{' '}<span className='vertical-ora'>|</span>{' '}{rooms}房</div>
                        <div className='grey-des '>{district}{' '}<span className='vertical-ora'>|</span>{' '}{address}</div>
                        <div className='rent-ora padding'><button className='join-btn'>參加睇樓團</button></div>
                </div>


            </div>
        </Link>
    )
}

export default SiteItem