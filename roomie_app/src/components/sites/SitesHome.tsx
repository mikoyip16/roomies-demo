import React from 'react';
import SiteItem from './SiteItem';
import './scss/Sites.scss'
import { useDispatch, useSelector } from 'react-redux';
import { displayAllThunk } from '../../redux/sitePage/thunk';
import { IRootState } from '../../redux/store';

const SitesHome = () => {
    
    const payload = useSelector((state: IRootState) => (state.sitePage.payload))!
   
    
    return (
        <div >
            {payload &&
                (
                    payload.map((site, i) => (
                        <SiteItem
                            key={i}
                            id={site.id}
                            image={site.image}                            
                            title={site.title}
                            rooms={site.rooms}
                            usable_area={site.usable_area}
                            floor={site.floor}
                            years={site.years}
                            district={site.district}
                            address={site.address}
                            rent={site.rent}
                            owner_id={site.owner_id}
                            updated_at={site.updated_at}
                            date={site.date} />
                    ))
                )
            }

        </div>
    )
}
export default SitesHome

