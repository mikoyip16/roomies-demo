
import './scss/SiteInfo.scss'


const SiteInfo = (props: { siteInfo: { address: any, title: any; rent: any; gross_area: any; usable_area: any; rooms: any; years: any; view: any; television: any; air_con: any; washing_machine: any; water_heater: any; microwave: any; fridge: any; bed: any; }; }) => {
    const {
        address,
        title,
        rent,
        gross_area,
        usable_area,
        rooms,
        years,
        view,
        television,
        air_con,
        washing_machine,
        water_heater,
        microwave,
        fridge,
        bed, } = props.siteInfo

    return (
        <div >
            <div className="flat_detail">
                <div style={{ textAlign: 'center', color: 'dark gray' }}>地址:{address}</div>
                <div className="flat_main_info">
                    <div className="price">
                        <div id="租">租 $ {rent}</div>

                    </div>
                    <div className="gross_area">
                        <span id="建築">建築</span>
                        {gross_area} 呎
                    </div>
                    <div className="usable_area">
                        <span id="實用">實用</span>
                        {usable_area} 呎
                    </div>
                </div>

                <div className="sc-4q1ceo-0 sc-4q1ceo-2 sc-4q1ceo-3 eusbUG">
                    <div className="sc-4q1ceo-1 sc-4q1ceo-4 hXnFSI">
                        <div className="sc-4q1ceo-5 fVreVr">間隔</div><br></br>
                        <div className="sc-4q1ceo-6 gvxbVn" id="rooms">{rooms}房</div>
                    </div>
                    <div className="sc-4q1ceo-1 sc-4q1ceo-4 hXnFSI">
                        <div className="sc-4q1ceo-5 fVreVr">座向</div><br></br>
                        <div className="sc-4q1ceo-6 gvxbVn" id="view">{view}</div>
                    </div>
                    <div className="sc-4q1ceo-1 sc-4q1ceo-4 hXnFSI">
                        <div className="sc-4q1ceo-5 fVreVr">樓齡</div><br></br>
                        <div className="sc-4q1ceo-6 gvxbVn" id="years">{years}年
                        </div>
                    </div>
                </div>
                <div className="furniture">
                    <div className={`furniture_icon ${television ? 'active' : ''}`}>
                        <i className="fas fa-tv"></i>電視
                    </div>
                    <div className={`furniture_icon ${air_con ? 'active' : ''}`}>
                        <i className="fas fa-hdd" id="air_con"></i>冷氣
                    </div>
                    <div className={`furniture_icon ${washing_machine ? 'active' : ''}`}>
                        <i className="fas fa-water" id="washing-machine"></i>洗衣機
                    </div>
                </div>
                <div className="furniture">
                    <div className={`furniture_icon ${water_heater ? 'active' : ''}`}>
                        <i className="fab fa-hotjar" id="water-heater"></i>熱水爐
                    </div>
                    <div className={`furniture_icon ${microwave ? 'active' : ''}`}>
                        <i className="fas fa-laptop" id="microwave"></i>微波爐
                    </div>
                    <div className={`furniture_icon ${fridge ? 'active' : ''}`}>
                        <i className="fas fa-temperature-low" id="fridge"></i>雪櫃
                    </div>
                    <div className={`furniture_icon ${bed ? 'active' : ''}`}>
                        <i className="fas fa-bed"></i>床
                    </div>
                </div>
            </div>
        </div>)
}
export default SiteInfo