import React from 'react'
import { Link } from 'react-router-dom'
import './scss/SiteItem.scss'

const SiteItem = () => {
    return (
        <Link to='/site/detail?tab=siteInfo' className='card-container'>

            <div className='site-card-container'>
                <div style={{
                    backgroundImage: `url('/Users/lixingye/Desktop/c15-frd-project-03-tw/roomie_app/src/roomie-icon.png')`,
                    backgroundRepeat: 'no-repeat',
                    width: "120px",
                    height: "120px"
                }} className='site-img'></div>
                <svg className='description'
                    xmlns="http://www.w3.org/2000/svg"
                    width="190"
                    height="120"
                    fill="none"
                    viewBox="0 0 190 120"
                >
                    <text
                        fill="#232323"
                        style={{ whiteSpace: "pre" }}
                        fontFamily="Roboto"
                        fontSize="14"
                        letterSpacing="0em"
                    >
                        <tspan x="0" y="22.285">
                            近地鐵站 室內 開放式間隔
                        </tspan>
                    </text>
                    <text
                        fill="#C4C4C4"
                        style={{ whiteSpace: "pre" }}
                        fontFamily="Roboto"
                        fontSize="10"
                        letterSpacing="0em"
                    >
                        <tspan x="62.151" y="72.418"></tspan>
                        <tspan x="67.073" y="72.418"></tspan>
                        <tspan x="88.345" y="72.418"></tspan>
                        <tspan x="95.747" y="72.418"></tspan>
                        <tspan x="135.335" y="72.418"></tspan>
                    </text>
                    <text
                        fill="#C4C4C4"
                        style={{ whiteSpace: "pre" }}
                        fontFamily="Roboto"
                        fontSize="9"
                        letterSpacing="0em"
                    >
                        <tspan x="0" y="72.418">
                            1房1廳
                        </tspan>
                        <tspan x="37.99" y="72.418">
                            400呎
                        </tspan>
                        <tspan x="69.554" y="72.418">
                            32/F
                        </tspan>
                        <tspan x="98.228" y="72.418">
                            樓齡37年
                        </tspan>
                    </text>
                    <text
                        fill="#E96443"
                        style={{ whiteSpace: "pre" }}
                        fontFamily="Roboto"
                        fontSize="10"
                        letterSpacing="0em"
                    >
                        <tspan x="28.107" y="72.418">
                            {" "}
                            |{" "}
                        </tspan>
                        <tspan x="64.632" y="72.418">
                            |
                        </tspan>
                        <tspan x="93.306" y="72.418">
                            |
                        </tspan>
                    </text>
                    <text
                        fill="#E96443"
                        style={{ whiteSpace: "pre" }}
                        fontFamily="Roboto"
                        fontSize="12"
                        fontWeight="bold"
                        letterSpacing="0em"
                    >
                        <tspan x="130.059" y="51.602">
                            $7000/月
                        </tspan>
                    </text>
                    <text
                        fill="#E96443"
                        style={{ whiteSpace: "pre" }}
                        fontFamily="Roboto"
                        fontSize="10"
                        letterSpacing="0em"
                    >
                        <tspan x="27" y="90.418">
                            {" "}
                            |{" "}
                        </tspan>
                    </text>
                    <text
                        fill="#C4C4C4"
                        style={{ whiteSpace: "pre" }}
                        fontFamily="Roboto"
                        fontSize="9"
                        letterSpacing="0em"
                    >
                        <tspan x="36.883" y="90.418">
                            德輔道西97號
                        </tspan>
                    </text>
                    <text
                        fill="#C4C4C4"
                        style={{ whiteSpace: "pre" }}
                        fontFamily="Roboto"
                        fontSize="9"
                        letterSpacing="0em"
                    >
                        <tspan x="0" y="90.418">
                            中西區
                        </tspan>
                    </text>
                    <g filter="url(#filter0_dd)">
                        <rect
                            width="59"
                            height="14"
                            x="121.5"
                            y="105.5"
                            fill="#fff"
                            stroke="#E96443"
                            rx="5.5"
                        ></rect>
                        <text
                            fill="#E96443"
                            style={{ whiteSpace: "pre" }}
                            fontFamily="Roboto"
                            fontSize="9"
                            letterSpacing="0em"
                        >
                            <tspan x="128.5" y="115.576">
                                參加睇樓團
                            </tspan>
                        </text>
                    </g>
                    <defs>
                        <filter
                            id="filter0_dd"
                            width="212"
                            height="167"
                            x="45"
                            y="67"
                            colorInterpolationFilters="sRGB"
                            filterUnits="userSpaceOnUse"
                        >
                            <feFlood floodOpacity="0" result="BackgroundImageFix"></feFlood>
                            <feColorMatrix
                                in="SourceAlpha"
                                result="hardAlpha"
                                values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
                            ></feColorMatrix>
                            <feOffset dy="6"></feOffset>
                            <feGaussianBlur stdDeviation="12"></feGaussianBlur>
                            <feColorMatrix values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.08 0"></feColorMatrix>
                            <feBlend
                                in2="BackgroundImageFix"
                                result="effect1_dropShadow"
                            ></feBlend>
                            <feColorMatrix
                                in="SourceAlpha"
                                result="hardAlpha"
                                values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
                            ></feColorMatrix>
                            <feOffset dy="38"></feOffset>
                            <feGaussianBlur stdDeviation="38"></feGaussianBlur>
                            <feColorMatrix values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.08 0"></feColorMatrix>
                            <feBlend
                                in2="effect1_dropShadow"
                                result="effect2_dropShadow"
                            ></feBlend>
                            <feBlend
                                in="SourceGraphic"
                                in2="effect2_dropShadow"
                                result="shape"
                            ></feBlend>
                        </filter>
                    </defs>
                </svg>
            </div>
        </Link>
    )
}

export default SiteItem