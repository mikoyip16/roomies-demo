import React from 'react';
import SiteItem from './SiteItem';
import './scss/SiteInfo.scss'

const SiteInfo = () => {
    return (
        <div >
            <div className="flat_detail">
                <div className="flat_main_info">
                    <div className="price">
                        <div id="租">租 $ 1,400</div>

                    </div>
                    <div className="gross_area">
                        <span id="建築">建築</span>
                        550 呎
                    </div>
                    <div className="usable_area">
                        <span id="實用">實用</span>
                        411 呎
                    </div>
                </div>

                <div className="sc-4q1ceo-0 sc-4q1ceo-2 sc-4q1ceo-3 eusbUG">
                    <div className="sc-4q1ceo-1 sc-4q1ceo-4 hXnFSI">
                        <div className="sc-4q1ceo-5 fVreVr">間隔</div><br></br>
                        <div className="sc-4q1ceo-6 gvxbVn" id="rooms">4房1廳2廁</div>
                    </div>
                    <div className="sc-4q1ceo-1 sc-4q1ceo-4 hXnFSI">
                        <div className="sc-4q1ceo-5 fVreVr">座向</div><br></br>
                        <div className="sc-4q1ceo-6 gvxbVn" id="view">東北</div>
                    </div>
                    <div className="sc-4q1ceo-1 sc-4q1ceo-4 hXnFSI">
                        <div className="sc-4q1ceo-5 fVreVr">樓齡</div><br></br>
                        <div className="sc-4q1ceo-6 gvxbVn" id="years">6年
                        </div>
                    </div>
                </div>
                <div className="furniture">
                    <div className="furniture_icon active">
                        <i className="fas fa-tv"></i>電視
                    </div>
                    <div className="furniture_icon">
                        <i className="fas fa-hdd" id="air_con"></i>冷氣
                    </div>
                    <div className="furniture_icon">
                        <i className="fas fa-water" id="washing-machine"></i>洗衣機
                    </div>
                </div>
                <div className="furniture">
                    <div className="furniture_icon">
                        <i className="fab fa-hotjar" id="water-heater"></i>熱水爐
                    </div>
                    <div className="furniture_icon">
                        <i className="fas fa-laptop" id="microwave"></i>微波爐
                    </div>
                    <div className="furniture_icon">
                        <i className="fas fa-temperature-low" id="fridge"></i>雪櫃
                    </div>
                    <div className="furniture_icon">
                        <i className="fas fa-bed"></i>床
                    </div>
                </div>
            </div>
        </div>)
}
export default SiteInfo