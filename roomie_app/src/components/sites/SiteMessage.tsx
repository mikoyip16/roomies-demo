import React, { useEffect, useState } from 'react';
import './scss/SiteMessage.scss'
import CommentBox from '../CommentBox';
import { Form, Input } from 'reactstrap';
import { useForm } from 'react-hook-form';
import peter from "../../images/peter.jpg"
import { useSelector } from 'react-redux';
import { IonInput, IonPage } from '@ionic/react';
import { IRootState } from '../../redux/store';

interface IMessage {
    "usersid": number,
    "id": number,
    "content": string,
    "nickname": string,
    "image": string,
    "msg_img": string,
    "edit": boolean,
    "del": boolean,
}

const SiteMessage = (props: { messages: IMessage[]; }) => {
    const { messages } = props;
    return (
        <div className="underSegmentBtn" id="chatContainer" style={{ display: 'flex', flexDirection: 'column' }}>
            {messages.map((message, idx) => {
                return <CommentBox ref_={(e: HTMLDivElement) => { 
                    if (e && idx === messages.length - 1) {
                        e.scrollIntoView({ behavior: 'smooth' })
                    }
                }} key={idx} img={message.image} content={message.content} nickname={message.nickname} />
            })}
            <div style={{ height: '80px' }}></div>
        </div>
    )
}
export default SiteMessage