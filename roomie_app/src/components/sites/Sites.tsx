import React from 'react';
import SiteItem from './SiteItem';
import './scss/Sites.scss'
import { useDispatch, useSelector } from 'react-redux';
import { displayAllThunk } from '../../redux/sitePage/thunk';
import { IRootState } from '../../redux/store';

const Sites = () => {
    

    const allSites = useSelector((state: IRootState) => (state.sitePage.payload))
    const filteredSite = useSelector((state: IRootState) => (state.siteFilter.payload))
    const hasQuery = useSelector((state: IRootState) => (state.siteFilter.hasQuery))
    const searchText = useSelector((state: IRootState) => (state.siteFilter.searchText?.toLocaleLowerCase() || ''))

   

    let payload = (filteredSite || allSites)
      ?.filter(site => 
        site.title.toLocaleLowerCase().includes(searchText) || 
        site.address.toLocaleLowerCase().includes(searchText))

    return (
        <div >
            {payload?.length !== 0 && payload? 
                (
                    payload.map((site, i) => (
                        <SiteItem
                            key={i}
                            id={site.id}
                            image={site.image}
                            title={site.title}
                            rooms={site.rooms}
                            usable_area={site.usable_area}
                            floor={site.floor}
                            years={site.years}
                            district={site.district}
                            address={site.address}
                            rent={site.rent}
                            owner_id={site.owner_id}
                            updated_at={site.updated_at}
                            date={site.date} />
                    ))
                ) : (<div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', height: '200px' }}>
                <div style={{ color: '#c4c4c4' }}> 搜尋結果： 沒有符合條件的樓盤， 請輸入其它條件</div>

            </div>)
            }

        </div>
    )
}
export default Sites

