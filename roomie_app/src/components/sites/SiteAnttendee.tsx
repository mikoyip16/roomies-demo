import './scss/SitesAttendee.scss'
import AttendeeOnList from '../pages/AttendeeOnList';
import { useEffect, useState } from 'react';
import { setSiteInfosThunk } from '../../redux/siteInfo/thunk';
import { useDispatch, useSelector } from 'react-redux';
import { IRootState } from '../../redux/store';
import { IonFab, IonFabButton, IonIcon, useIonAlert } from '@ionic/react';
import { add } from 'ionicons/icons';

const SiteAnttendee = (props: { siteID: string, meetingID: string, meetingDate: string }) => {
    const [present] = useIonAlert();
    const getAttendeeData = async (meetingID: string) => {
        const res = await fetch(process.env.REACT_APP_API_SERVER + `/attendees?siteMeetingID=${meetingID}&userID=12`, {
            headers: {
                'Authorization': 'Bearer ' + localStorage.getItem('roomie_token')
            },
        }) 
        const resJson = await res.json();
        console.log(resJson.payload);
        setOnAttendeeList(resJson.payload.onAttendList);
        setAttendees(resJson.payload.attendees);
    }
    const addAttendeeData = async (meetingID: string, siteID: string) => {
        const body = { 'siteMeetingID': meetingID, 'siteID': siteID };
        const res = await fetch(process.env.REACT_APP_API_SERVER + "/attendees", {
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem('roomie_token')
            },
            body: JSON.stringify(body)
        });
        const resJson = await res.json();
        console.log(resJson.payload);
        getAttendeeData(props.meetingID);
    }
    interface IAttendee {
        id: number;
        attended: boolean;
        pastEvent: boolean;
        usersId: number;
        nickname: string;
        image: string;
    }
   
    const [attendees, setAttendees] = useState<Array<IAttendee>>([]);
    const [onAttendeeList, setOnAttendeeList] = useState<boolean>(false);
    const [isPastEvent, setIsPastEvent] = useState<boolean>(false);

    useEffect(() => {

        getAttendeeData(props.meetingID); 
        const eventDate = new Date(props.meetingDate);
        const now = new Date();
        let pastEvent = false;

        
        if (now > eventDate) {
            pastEvent = true;
        } else {
            pastEvent = false;
        }
        setIsPastEvent(pastEvent);
        console.log(props.meetingDate)
    }, []); 
    return (
        <div style={{}}>
            {attendees.map(attendee => {
                return <AttendeeOnList key={attendee.id} name={attendee.nickname} img={attendee.image} isAttended={attendee.attended} pastEvent={attendee.pastEvent} />
            })}

            {!isPastEvent && !onAttendeeList && <IonFab className='ion-margin-bottom' vertical="bottom" horizontal="end" slot="fixed">
                <IonFabButton onClick={() => present({
                    header: '注意',
                    message: '報名參加後，因任何原因缺席都將會被記錄於資料庫。若缺席次數過多，帳戶可能會被停用。\r\n確定報名？',
                    buttons: [
                        'Cancel',
                        { text: 'Ok', handler: (d) => { addAttendeeData(props.meetingID, props.siteID) } },
                    ],
                })} style={{ fontWeight: 'bold', fontSize: '2em' }} color="primary">
                    <IonIcon icon={add} />
                </IonFabButton>
            </IonFab>}
        </div>
    )
}
export default SiteAnttendee