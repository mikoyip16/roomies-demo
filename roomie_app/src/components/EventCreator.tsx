import React, { useState } from 'react'
import Navbar from './layout/NavBar';
import {
    IonContent,
    IonHeader,
    IonPage,
    IonTitle,
    IonToolbar,
    IonList,
    IonItem,
    IonLabel,
    IonInput,
    IonSelect,
    IonSelectOption,
    IonNote,
    IonButton,
    IonIcon,
    IonButtons,
    IonImg,
    IonAvatar,
    IonPickerColumn,
    IonTextarea,
    IonFooter,
    useIonAlert,
} from '@ionic/react'
import { checkmark, imageOutline, trash } from 'ionicons/icons'
import { selectImage, filesToBase64Strings } from '@beenotung/tslib/file'
import { useStateProxy } from 'use-state-proxy'
import { useHistory } from 'react-router';

function Label({ text }: { text: string }) {
    return (
        <IonLabel
            color="danger"
            position="floating"
            style={{ textTransform: 'capitalize' }}
        >
            {text}
        </IonLabel>
    )
}

function formatDate(date: any) { 
    try {
        date = new Date(date)
    } catch (error) {
        date = new Date()
    }
    if (!date.getTime()) {
        date = new Date()
    }
    return new Intl.DateTimeFormat('en-HK', {
        dateStyle: 'medium',
    }).format(date)
}

const EventCreator = () => {
    const history = useHistory();
    const [title, setTitle] = useState('');
    const [type, setType] = useState('');
    const [number, setNumber] = useState('');
    const [address, setAddress] = useState('');
    const [date, setDate] = useState('');
    const [time, setTime] = useState('');
    const [content, setContent] = useState('');
    const [present] = useIonAlert();


    
    type State = {
        fileList: File[]
        previewList: string[]
    }
    const statePic = useStateProxy<State>({ 
        fileList: [],
        previewList: [],
    })
    async function pickFile() {
        statePic.fileList = await selectImage({ multiple: true })
        statePic.previewList = await filesToBase64Strings(statePic.fileList)
    }
    async function submit() {
        
        let mapResult = await fetch(`https://maps.googleapis.com/maps/api/geocode/json?address=${address}&key=AIzaSyAYYZ7I6mQojRR0523AUMQgpxThmZjYoJs`);
        let mapResultJson = await mapResult.json();
        console.log(mapResultJson)
        const map_x = mapResultJson['results'][0].geometry.location.lat;
        const map_y = mapResultJson['results'][0].geometry.location.lng;
        console.log(map_x);
        console.log(map_y);
       
        let formData = new FormData() 
        formData.set('images', ''); 
        statePic.fileList.forEach(file => formData.append('images', file)) 
        formData.set('title', title);
        formData.set('type', type);
        formData.set('ppl', number);
        formData.set('address', address);
        formData.set('eventDateTime', `${date} ${time}`); 
        formData.set('description', content);
        formData.set('map_x', map_x);
        formData.set('map_y', map_y);
        let res = await fetch(process.env.REACT_APP_API_SERVER + '/event',
            {
                headers: { 'Authorization': 'Bearer ' + localStorage.getItem('roomie_token')! }, 
                method: 'POST',
                body: formData
            })
        let response = await res.json();
        if (res.status == 400) {
            present('提交失敗: ' + response.message, [{ text: 'Ok' }]) 
        } else if (res.status !== 200) {
            present('提交失敗: ' + res.statusText, [{ text: 'Ok' }])
           
        } else {
            present('成功提交～', [{ text: 'Ok' }])
            history.push('/gathering');
        }
    }



    return (

        <IonPage>
            <IonHeader>
                <IonToolbar className='rainbow-bg'>
                    <IonTitle>創建活動</IonTitle>
                </IonToolbar>
            </IonHeader>
            <IonContent >
                <IonList>

                    <IonItem>
                        <Label text="標題" />
                        <IonInput type="text" value={title}
                            onIonChange={(e: Event) =>
                                setTitle((e.target as HTMLInputElement).value)
                            } />
                    </IonItem>
                    <IonItem><Label text="活動類型" />
                        <IonSelect value={type} onIonChange={(e: Event) =>
                            setType((e.target as HTMLInputElement).value)
                        }>
                            <IonSelectOption>遊戲</IonSelectOption>
                            <IonSelectOption>運動與健身</IonSelectOption>
                            <IonSelectOption>愛好</IonSelectOption>
                            <IonSelectOption>寵物</IonSelectOption>
                            <IonSelectOption>宗教身靈性</IonSelectOption>
                            <IonSelectOption>音樂</IonSelectOption>
                        </IonSelect>
                    </IonItem>
                    <IonItem><Label text="參加人數" />
                        <IonInput type="number" value={number} onIonChange={(e: Event) =>
                            setNumber((e.target as HTMLInputElement).value)
                        }></IonInput>
                    </IonItem>
                    <IonItem><Label text="地址" />
                        <IonInput value={address} type="text" onIonChange={(e: Event) =>
                            setAddress((e.target as HTMLInputElement).value)
                        } />
                    </IonItem>
                    <IonItem><Label text="日期" />
                        <IonInput value={date} type="date" onIonChange={(e: Event) =>
                            setDate((e.target as HTMLInputElement).value)
                        } />
                    </IonItem>
                    <IonNote className="ion-padding">{formatDate(date)}</IonNote>
                    <IonItem>
                        <Label text="時間" />
                        <IonInput value={time} type="time" onIonChange={(e: Event) =>
                            setTime((e.target as HTMLInputElement).value)} />
                    </IonItem>
                    <IonItem>
                        <Label text="活動內容" />
                        <IonTextarea value={content} onIonChange={(e: Event) =>
                            setContent((e.target as HTMLInputElement).value)} />
                    </IonItem>
                    <div style={{ paddingLeft: '12px', display: 'flex', alignItems: 'center' }}>
                        <IonButtons>
                            <IonButton size="small" onClick={pickFile}>
                                <IonIcon icon={imageOutline}></IonIcon>
                            </IonButton>
                        </IonButtons>
                        <p style={{ paddingLeft: '15px' }}>上載照片 ({statePic.fileList.length})</p>
                    </div>
                    <IonList>
                        {statePic.previewList.map((url, i) => (
                            <IonItem key={i}>
                                <IonAvatar slot="start">
                                    <IonImg src={url}></IonImg>
                                </IonAvatar>
                                <IonLabel>{statePic.fileList[i].name}</IonLabel>
                                <IonButtons slot="end">
                                    <IonButton
                                        color="danger"
                                        shape="round"
                                        onClick={() => {
                                            statePic.fileList.splice(i, 1)
                                            statePic.previewList.splice(i, 1)
                                        }}
                                    >
                                        <IonIcon src={trash}></IonIcon>
                                    </IonButton>
                                </IonButtons>
                            </IonItem>
                        ))}
                    </IonList>
                   
                </IonList>
            

                <IonButton color="light" expand="block" onClick={() => { submit() }}>
                    <IonIcon icon={checkmark}></IonIcon>
                </IonButton>
                <div style={{ marginBottom: '70px' }}></div>

            </IonContent>
            <IonFooter>

                <Navbar />
            </IonFooter>
        </IonPage>

    )
}

export default EventCreator
