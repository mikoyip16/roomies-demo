import './scss/GatheringAttendee.scss'
import AttendeeOnList from '../pages/AttendeeOnList';
import { useEffect, useState } from 'react';
import { setSiteInfosThunk } from '../../redux/siteInfo/thunk';
import { useDispatch, useSelector } from 'react-redux';
import { IRootState } from '../../redux/store';
import { IonFab, IonFabButton, IonIcon, useIonAlert } from '@ionic/react';
import { add } from 'ionicons/icons';

const SiteAnttendee = (props: { eventID: string }) => {
    const [present] = useIonAlert();
    const getAttendeeData = async (eventID: string) => {
        const res = await fetch(process.env.REACT_APP_API_SERVER + `/event/attendees?eventID=${eventID}`, {
            headers: {
                'Authorization': 'Bearer ' + localStorage.getItem('roomie_token')
            }
        })
        const resJson = await res.json();
        console.log(resJson.payload);
        setOnAttendeeList(resJson.payload.onAttendList);
        setAttendees(resJson.payload.attendees);
    }
    const addAttendeeData = async (eventID: string) => {
        const body = { 'eventID': eventID };
        const res = await fetch(process.env.REACT_APP_API_SERVER + "/event/attendee", {
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem('roomie_token')
            },
            body: JSON.stringify(body)
        });
        const resJson = await res.json();
        console.log(resJson.payload);
        getAttendeeData(props.eventID);
    }
    interface IAttendee {
        id: number;
        attended: boolean;
        pastEvent: boolean;
        usersId: number;
        nickname: string;
        image: string;
    }
    // const dispatch = useDispatch();
    const siteInfo = useSelector((state: IRootState) => state.auth.payload)
    const [attendees, setAttendees] = useState<Array<IAttendee>>([]);
    const [onAttendeeList, setOnAttendeeList] = useState<boolean>(false);
    const [isPastEvent, setIsPastEvent] = useState<boolean>(false);

    useEffect(() => {

        getAttendeeData(props.eventID); 
    }, []); 
    return (
        <div style={{}}>
            {attendees.map(attendee => {
                return <AttendeeOnList key={attendee.id} name={attendee.nickname} img={attendee.image} isAttended={attendee.attended} pastEvent={attendee.pastEvent} />
            })}

            {!isPastEvent && !onAttendeeList && <IonFab className='ion-margin-bottom' vertical="bottom" horizontal="end" slot="fixed">
                <IonFabButton onClick={() => present({
                    header: '注意',
                    message: '報名參加後，因任何原因缺席都將會被記錄於資料庫。若缺席次數過多，帳戶可能會被停用。\r\n確定報名？',
                    buttons: [
                        'Cancel',
                        { text: 'Ok', handler: (d) => { addAttendeeData(props.eventID) } },
                    ],
                })} style={{ fontWeight: 'bold', fontSize: '2em' }} color="primary">
                    <IonIcon icon={add} />
                </IonFabButton>
            </IonFab>}
        </div>
    )
}
export default SiteAnttendee