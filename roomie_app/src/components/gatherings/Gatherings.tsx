import React, { useEffect, useState } from 'react';
import { useLocation } from 'react-router';
import GatheringItem from './GatheringItem';
import './scss/Gatherings.scss'

interface IGathering {
    created_at: string,
    date: string,
    description: string,
    gap: any,
    id: number,
    image: string,
    userimage: string
    meeting_address: string,
    nickname: string,
    title: string,
    type: string
}

export const Sites = () => {
    const location = useLocation()
    const params = new URLSearchParams(location.search)
    const type = params.get('type')
    const [gatherings, setGatherings] = useState<IGathering[]>([]);
    const getEvents = async (type: string | null) => {
        let res: any = {};
        if (type) {
            res = await fetch(process.env.REACT_APP_API_SERVER + `/event?type=` + type) 
        } else {
            res = await fetch(process.env.REACT_APP_API_SERVER + `/event`) 
        } const resJson = await res.json();
        console.log(resJson.payload);
        setGatherings(resJson.payload.eventInfo);
    }
    useEffect(() => {
        getEvents(type);
    }, [type])
    return (
        <div >
            {gatherings.map(gathering => {
                return <GatheringItem key={gathering.id} eventID={gathering.id} hostImg={gathering.userimage} eventImg={gathering.image} title={gathering.title} date={gathering.date} createAt={gathering.created_at} userName={gathering.nickname} />
            })}
            <div style={{ height: '150px' }}></div>
        </div>
    )
}
export default Sites