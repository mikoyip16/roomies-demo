import React, { useState } from 'react';
import './scss/GatheringInfo.scss'
import CommentBox from '../CommentBox';
import { Form, Input } from 'reactstrap';
import { useForm } from 'react-hook-form';
import peter from "../../images/peter.jpg"

const GatheringInfo = (props: { description: any; ppl: any; address: any; }) => {
    const { description, ppl, address } = props;

    return (
        <div className="left_lower">
            <div className="flat_detail">
                <div className="InfoTable">
                    <div className="pplJoin">
                        <h4>參加人數:</h4>
                        <h5>{ppl}人</h5>
                    </div>
                    <div className="Address">
                        <h4>地址:</h4>
                        <h5>{address}</h5>
                    </div>
                    <div className="Description">
                        <h4>活動內容:</h4>
                        <h5>{description}</h5>
                    </div>
                </div>
            </div>
        </div>
    )
}
export default GatheringInfo