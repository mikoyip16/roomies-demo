import React, { useState } from "react";
import { Link } from "react-router-dom";
import bat from "../../images/bat.jpg"
import peter from "../../images/peter.jpg"
import './scss/GatheringItem.scss'
import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar, IonCard, IonCardHeader, IonCardSubtitle, IonCardTitle, IonCardContent, IonItem, IonIcon, IonLabel, IonButton, IonGrid, IonAvatar } from '@ionic/react';
import { create } from "ionicons/icons";

const MeetingItem = (props: { hostImg: any; eventImg: any; title: any; date: any; createAt: any; userName: any; eventID: any }) => {
    const { hostImg, eventImg, title, date, createAt, userName, eventID } = props
    console.log(props);
    const displayStartDate = new Date(date)
    const displayCreateDate = new Date(createAt)
    const [hearted, setHearted] = useState(false);
    return (

        <IonCard routerLink={`/gathering/detail?eventID=${eventID}&tab=gatheringInfo`} style={{ borderRadius: '20px', position: 'relative', margin: '8px' }}>
            <IonCardHeader style={{ display: 'flex', padding: '8px 15px' }}>
                <div style={{ width: '20%' }}>
                    <IonAvatar>
                        <img src={process.env.REACT_APP_IMAGE_SERVER + "/" + hostImg} />
                    </IonAvatar>
                </div>
                <div style={{ width: '80%', display: 'flex', justifyContent: 'space-around', flexDirection: 'column' }}>
                    <p className='gatheringHost'>
                        {userName} <span style={{ color: '#9597A1' }}>hosted</span> event
                    </p>

                </div>
            </IonCardHeader>
            <div style={{ position: 'absolute', borderRadius: 'px', width: '42px', height: '43px', background: 'white', top: '60%', left: '5%', display: "flex", flexDirection: 'column' }}>
                <span style={{ fontSize: '20px', textAlign: "center" }}>{displayStartDate.getDate()}</span>
                <span style={{ color: '#FF5353', textAlign: "center" }}>{displayStartDate.toLocaleString('default', { month: 'short' })}</span>
            </div>
            <img style={{ width: '100%', height: '180px', objectFit: 'cover' }} src={process.env.REACT_APP_IMAGE_SERVER + "/" + eventImg} />
            <IonCardHeader style={{ display: 'flex', padding: '8px 15px' }}>
                <div style={{ width: '90%', display: 'flex', justifyContent: 'space-around', flexDirection: 'column' }}>
                    <p className='gatheringTitle'>
                        {title}
                    </p>
                    <p className='gatheringDateTime'>
                        {displayStartDate.toLocaleDateString() + " " + displayStartDate.toLocaleTimeString()}
                    </p>
                </div>
               
                <div style={{ margin: 'auto' }} onClick={(event) => { event.preventDefault(); setHearted(!hearted) }}>
                   
                    {hearted ? <i className="fas fa-heart" style={{ fontSize: '18px', color: 'red' }}></i> : <i className="far fa-heart" style={{ fontSize: '18px' }}></i>}
                </div>
            </IonCardHeader>
        </IonCard>

    );
}

export default MeetingItem;