import React, { useEffect, useState } from 'react';
import './scss/GatheringMessage.scss'
import CommentBox from '../CommentBox';
import { Form, Input } from 'reactstrap';

import { IonAvatar, IonButton, IonButtons, IonFooter, IonIcon, IonImg, IonInput, IonItem } from '@ionic/react';
import { useSelector } from 'react-redux';
import { IRootState } from '../../redux/store';
import { paperPlane, paperPlaneOutline } from 'ionicons/icons';
interface IMessage {
    "usersid": number,
    "id": number,
    "content": string,
    "nickname": string,
    "image": string,
    "msg_img": string,
    "edit": boolean,
    "del": boolean,
}
const SiteMessage = (props: { messages: IMessage[] }) => {

    const { messages } = props;

    return (
        <div className="underSegm entBtn" style={{ display: 'flex', flexDirection: 'column' }}>
            {messages.map((message: { id: React.Key | null | undefined; image: any; content: any; nickname: any; }, idx) => {
                return <CommentBox ref_={(e: HTMLDivElement) => { 
                    if (e && idx === messages.length - 1) {
                        e.scrollIntoView({ behavior: 'smooth' })
                    }
                }} key={message.id} img={message.image} content={message.content} nickname={message.nickname} />
            })}
            <div style={{ height: '80px' }}></div>
        </div >
    )
}
export default SiteMessage