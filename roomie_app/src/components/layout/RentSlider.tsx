import React, { useState } from 'react'
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Slider from '@material-ui/core/Slider';
import { useDispatch, useSelector } from 'react-redux';
import { setQueryAction } from '../../redux/siteFilter/action';
import { IRootState } from '../../redux/store';
import { querySiteThunk } from '../../redux/siteFilter/thunk';


const useStyles = makeStyles({
    root: {
        padding: '0',
        width: '16vw',
        color: '#c4c4c4',
    },
});

function valuetext(value: number[]) {
    return `$${value}`;
}

const RentSlider = (props:{
}) => {

    const classes = useStyles();
    const [value, setValue] = useState<number[]>([8000, 20000]);
    const dispatch = useDispatch()
    const rooms = useSelector((state: IRootState) => state.siteFilter.query?.compartment)
    const area = useSelector((state: IRootState) => state.siteFilter.query?.area)
    const handleChange = (event: any, newValue: number | number[]) => {
        setValue(newValue as number[]);
        dispatch(setQueryAction({ rentRange: newValue as number[] }))

    };
    return (
        <div className={classes.root} style={{ position: 'relative', bottom: '-1.4rem' }}>
            <Typography id="range-slider" gutterBottom>
                租金
            </Typography>
            <Slider
                value={value}
                min={2000}
                step={1000}
                max={30000}
                scale={(x) => x * 1}
                onChange={handleChange}
                valueLabelDisplay="auto"
                aria-labelledby="range-slider"
            />
        </div>
    )
}

export default RentSlider