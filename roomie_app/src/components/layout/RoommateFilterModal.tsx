import React, { useState } from 'react';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';
import { IonIcon } from '@ionic/react';
import { arrowDown } from 'ionicons/icons';
import RangeOption from './RangeOption';
import ItemOption from './ItemOption';
import { useDispatch, useSelector } from 'react-redux';
import { IRootState } from '../../redux/store';
import { setQueryAction } from '../../redux/roommateFilter/action';
type Range = {
    min?: number
    max?: number
}

const useModalStyles = makeStyles((theme: Theme) =>
    createStyles({
        modal: {
            display: 'flex',
            justifyContent: 'center',
            marginTop: '11.5rem',
            overflow: 'auto'
        },
        paper: {
            backgroundColor: theme.palette.background.paper,
            border: '0px 4px 4px rgba(0, 0, 0, 0.25);',
            borderRadius: '20px',
            height: '27rem',
            width: '84vw',

        },
    }),
);

export default function RoommateFilterModal(props: {
    specialRequirementList: string[],
    ageList: string[],

}) {
    const { specialRequirementList, ageList } = props
    const classesModal = useModalStyles();
    const [open, setOpen] = React.useState(false);

    const handleOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };
    const dispatch = useDispatch()

    const [selectedSpecialRequirementList, setSelectedSpecialRequirementList] = useState<string[]>([])
    const [selectedAgeList, setSelectedAgeList] = useState<string[]>([])
    const [reset, setReset] = useState(false)
    const query = useSelector((state: IRootState) => state.roommateFilter.query)!

    return (
        <div>
            <div className='button'
                onClick={handleOpen}
                style={{}}
            >篩選<IonIcon icon={arrowDown} mode='md'></IonIcon></div>
            <Modal
                aria-labelledby="transition-modal-title"
                aria-describedby="transition-modal-description"
                className={classesModal.modal}
                open={open}
                onClose={handleClose}
                closeAfterTransition
                BackdropComponent={Backdrop}
                BackdropProps={{
                    timeout: 500,
                }}
            >
                <Fade in={open}>
                    <div className={classesModal.paper}>
                        <div className='filter-container-s'>
                            <div className='orange-label'>起租日期</div>
                            <span className='choice'><input style={{ margin: '0.5vh 0 0 1rem' }} type='date' 
                            
                            /></span>

                        </div>
                        <div className='filter-container-s'>

                            <div className='orange-label'>租住月數</div>
                            <span className='choice'><input style={{ margin: '0.5vh 0 0 1rem' }} type='number' 
                           
                            /></span>
                        </div>
                        <div className='filter-container-s' >

                            <div className='orange-label' >
                                特別要求
                            </div>
                            <div className="grey-options" >
                                {specialRequirementList.map((requirement, i) => (
                                    <ItemOption
                                        key={i}
                                        item={requirement}
                                        isReset={reset}
                                        onChange={selectedItem => {

                                            if (selectedItem) {
                                                setSelectedSpecialRequirementList([...selectedSpecialRequirementList, requirement])
                                            } else {
                                                setSelectedSpecialRequirementList(
                                                    selectedSpecialRequirementList.filter(
                                                        eachItem =>
                                                            !(
                                                                eachItem === requirement
                                                            ),
                                                    ),
                                                )
                                            }
                                        }}

                                    />
                                ))}
                            </div>
                        </div>
                        <div className='filter-container-s' >
                            <div className='orange-label' >
                                年齡
                            </div>
                            <div className="grey-options" >
                                {ageList.map((age, i) => (
                                    <ItemOption

                                        key={i}
                                        item={age}
                                        isReset={reset}
                                        onChange={selectedItem => {

                                            if (selectedItem) {

                                                setSelectedAgeList([...selectedAgeList, age])
                                            } else {
                                                setSelectedAgeList(
                                                    selectedAgeList.filter(
                                                        eachItem =>
                                                            !(
                                                                eachItem === age
                                                            ),
                                                    ),
                                                )
                                            }



                                        }}
                                    />
                                ))}
                            </div>
                        </div>
                        <div className='confirm-btn-container' >
                            <div className='btn' onClick={() => {
                                setSelectedSpecialRequirementList([])
                                setSelectedAgeList([])
                                setReset(true)
                            }}>重置</div>
                            <div className='btn' onClick={e => {
                                dispatch(setQueryAction({
                                    specialRequirementList: selectedSpecialRequirementList,
                                    ageList: selectedAgeList

                                }))
                                handleClose()
                            }}> 確認</div>
                        </div>
                    </div>
                </Fade>
            </Modal>
        </div>
    );
}
