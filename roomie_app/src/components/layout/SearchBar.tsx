import { IonIcon } from '@ionic/react';
import e from 'cors';
import { compassSharp, locate, pin, search } from 'ionicons/icons';
import React, { useRef, useState } from 'react';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { IRootState } from '../../redux/store';
import Notification from './Notification';
import './scss/SearchLocation.scss'
import UserIcon from './UserIcon';

const SearchBar = (props: {
    placeHolderText: string
    icon: any
    searchChange: (searchInput: string) => void
}) => {
    const isAuthenticated = useSelector(
        (state: IRootState) => state.auth.isAuthenticated,
    )
    const {placeHolderText, searchChange, icon} = props
   
    return (
        <div >
            <div style={{ display: 'flex', flexDirection: 'row', alignItems: 'flex-end', justifyContent: "space-between", marginLeft:'1.5rem', marginTop:'0.8rem'}}>
                <div style={{ border:'1px solid #E96443', width: '60vw', height: '2rem', borderRadius:'1rem', overflow:'hidden'}}>
                    <form onSubmit={e=> e.preventDefault()}>
                        <div className='searchSite' style={{display:'flex', flexDirection:'row', justifyContent:'space-between'}}>
                            <div style={{width: '20%',display:'flex', justifyContent: 'center'}}><IonIcon icon={`${icon}`} mode='ios' size='large' style={{color:'#c4c4c4', height:'80%', marginTop: '10%', }}></IonIcon></div>
                            <input
                                id='search'
                                type='search'
                                placeholder={placeHolderText}
                                style={{ outline: 'none', border: 'none', height: '1.9rem', width:'80%'}}
                                onChange={(e) => {searchChange(e.target.value)}}
                            />
                            <label className='label-icon' htmlFor='search' style={{ width:'17%',marginRight: "3%"}} >
                                <IonIcon icon={search} mode='md' size='large' style={{color:'#E96443', height:'90%', marginTop: '5%'}}></IonIcon>
                            </label>
                        </div>
                    </form>
                </div>
                <div className='signInUp'>
                    {isAuthenticated ?
                        <>

                            <UserIcon />
                            <Notification />
                        </> :

                        <>
                            <div className='s-text'><Link to='/signUp' >註冊</Link></div>
                            <div className='s-text'><Link to='/login' >登入</Link></div>
                        </>
                    }
                </div>


            </div>
        </div>
    )
}

export default SearchBar