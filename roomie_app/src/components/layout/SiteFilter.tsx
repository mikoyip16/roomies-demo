import { IonIcon } from '@ionic/react';
import { arrowDown } from 'ionicons/icons';
import React, { useState } from 'react';
import SiteFilterModal from './SiteFilterModal'
import RentSlider from './RentSlider';
import { Range } from '../../redux/types'
import { useDispatch } from 'react-redux';
import './scss/SiteFilter.scss'
import { setQueryAction } from '../../redux/siteFilter/action';


const SiteFilter = () => {

    const rangeList: Range[] = [
        { max: 300 },
        { min: 300, max: 500 },
        { min: 500, max: 800 },
        { min: 1000 },
    ]
    const furnitureList: string[] = ['床', '洗衣機', '冷氣', '電視', '雪櫃', '微波爐']
    const specialList: string[] = ['複式', '頂樓', '天台', '平台', '花園']
    const compartmentList: string[] = ['1房', '2房', '3房', '4房', '4房以上']
    const areaList: string[] = [
        '中西區', '東區', '南區', '南區',
        '灣仔區', '九龍城區', '觀塘區', '深水埗區',
        '黃大仙區', '油尖旺區', '葵青區', '北區',
        '西貢區', '沙田區', '大埔區', '荃灣區',
        '屯門區', '元朗區', '離島區',]


    const dispatch = useDispatch()
   
    


    return (


        <form className='site-filter'>
            <div className='rent'>
                <RentSlider />
            </div>

            <label htmlFor='area'>
                <select name='area'
                    onChange={e => {
                        dispatch(setQueryAction({ area: e.target.value }))
                      
                    }}>
                    <option selected disabled value="地區">地區</option>
                    <option value="">所有</option>
                    {areaList.map((value, i) => (
                        <option key={i} value={`${value}`}>
                            {value}
                        </option>
                    ))}


                </select>
            </label>
            <label htmlFor='compartment'>
                <select name='compartment'
                    onChange={e => {
                        dispatch(setQueryAction({ compartment: e.target.value }))
                      
                    }}>

                    <option selected disabled value="間隔">間隔</option>
                    <option value="">所有</option>
                    {compartmentList.map((value, i) => (
                        <option key={i} value={`${value}`}>
                            {value}
                        </option>
                    ))}
                </select>
            </label>
            <SiteFilterModal
                rangeList={rangeList}
                specialList={specialList}
                furnitureList={furnitureList}
            />
        </form>
    )
}

export default SiteFilter


