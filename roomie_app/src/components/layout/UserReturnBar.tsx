import React, { useState } from 'react';
import {
    BrowserRouter as Router,
    Link,
} from 'react-router-dom';
import { ellipsisVerticalOutline, chevronBackOutline } from 'ionicons/icons'
import {
    IonIcon,
    IonButton,
    IonContent,
    IonItem,
    IonList,
    IonPage,
    useIonPopover,
} from '@ionic/react';
import { History } from 'history'
import './scss/UserReturnBar.scss'
import { useDispatch } from 'react-redux';
import { logoutThunk } from '../../redux/auth/thunk'

const PopoverList: React.FC<{
    dispatch:any
    onHide: () => void; history: History
}> = ({ dispatch, onHide, history }) => {
    console.log(history)
    
    const logout = () => {
        dispatch(logoutThunk(history))
        onHide()
    }
    

    return <IonList>
        <IonItem button onClick={() => history.push('/')}>首頁</IonItem>
        <IonItem button onClick={() => history.push('/site')}>搵樓</IonItem>
        <IonItem button onClick={() => history.push('/roommate')}>搵室友</IonItem>
        <IonItem button onClick={() => history.push('/gathering')}>交友聚會</IonItem>
        <IonItem button onClick={logout}>登出</IonItem>
        <IonItem lines="none" detail={false} button onClick={onHide}>
            Close
        </IonItem>
    </IonList>
};

const UserReturnBar: React.FC<{
    history: History
}> = ({ history }) => {



const dispatch = useDispatch()

    const [present, dismiss] = useIonPopover(PopoverList, { dispatch, onHide: () => dismiss(), history });

    return (

        <div className='UserReturnBar'>


            <Link to='/return/findRoommate' style={{ textDecoration: 'none' }} className='other'>
                <IonIcon size='large' icon={chevronBackOutline}></IonIcon>

                <div>{' '}搵其它室友</div>
            </Link>




            <IonPage>
                <IonContent  >
                    <IonButton size="small" fill='clear'
                        expand="block"
                        onClick={(e) =>
                            present({
                                event: e.nativeEvent,
                            })
                        }
                    >
                        <IonIcon size='large' icon={ellipsisVerticalOutline}></IonIcon>
                    </IonButton>
                </IonContent>
            </IonPage>
        </div>







    )
}

export default UserReturnBar