import React from 'react'
import './scss/HeaderBar.scss'
import { Link } from 'react-router-dom'
import { useSelector } from 'react-redux';
import { IRootState } from '../../redux/store';
import roomieLogo from '../../roomie-icon-home.png'
import UserIcon from './UserIcon';
import Notification from './Notification';
import { IonImg } from '@ionic/react';




const HeaderBar = () => {
    const isAuthenticated = useSelector(
        (state: IRootState) => state.auth.isAuthenticated,
    )
    return (

        <div className='headerBar' style={{ display: 'flex', flexDirection: 'row', alignItems: 'flex-end', justifyContent: "space-between" }}>
            <div className='logo-container'>
                <img src={roomieLogo} alt='' style={{ width: 'auto', height: '2.5rem' }}></img>
                <span className='roomie_rainbow'>Roomies</span>
            </div>
            <div className='signInUp'>
                {isAuthenticated ?
                    <>

                        <UserIcon />
                        <Notification />
                    </> :

                    <>
                        <div className='s-text'><Link to='/signUp' >註冊</Link></div>
                        <div className='s-text'><Link to='/login' >登入</Link></div>
                    </>
                }
            </div>


        </div>

    )
}

export default HeaderBar
