import React, { useState } from 'react'
import { IonModal, IonButton, IonContent, IonList, IonItem, IonIcon } from '@ionic/react';
import { History } from 'history'
import { useDispatch, useSelector } from 'react-redux';
import userLogo from '../../userIcon.png'
import './scss/UserIcon.scss'
import { logoutThunk } from '../../redux/auth/thunk';
import { useHistory } from 'react-router';
import { Link } from 'react-router-dom'
import { IRootState } from '../../redux/store';
import { colorPalette, contract, create, paperPlane, person, power } from 'ionicons/icons';
import { toUrl } from '../../helpers/api';



const UserIcon = () => {
    const image = useSelector((state: IRootState) => state.auth.payload?.image)
    const roommateIconUrl = toUrl(image || 'userIcon.png')
    const history = useHistory()
    const [showModal, setShowModal] = useState(false);
    const dispatch = useDispatch()
    const logout = () => {
        setShowModal(false)
        dispatch(logoutThunk(history))

    }
    const id = useSelector((state: IRootState) => state.auth.payload?.userID)
    return (
        <div>

            <div style={{ display: 'flex', alignItems: 'flex-end' }} onClick={() => setShowModal(true)}>
                <div className='icon' style={{

                    overflow: "hidden",
                    backgroundImage: `url(${roommateIconUrl})`,
                    backgroundSize: 'cover',
                    backgroundPosition: 'center'
                }}>

                </div>
                {console.log(id)}
            </div>

            {
                id && <IonModal isOpen={showModal} cssClass='my-custom-class'>
                    <div style={{ height: '10px', display: 'flex', justifyContent: 'center' }}>
                        <div onClick={() => setShowModal(false)} style={{ height: '10px', width: "50px", backgroundColor: "#c4c4c4", borderRadius: '8px', marginTop: '10px' }}></div>
                    </div>
                    <IonList>
                        <Link to={`/user/search?userID=${id}`}>
                            <IonItem button onClick={() => setShowModal(false)}><IonIcon icon={person}></IonIcon><span style={{ marginLeft: '10px' }}>個人頁面</span></IonItem>
                        </Link>
                        <IonItem button onClick={() => {
                            setShowModal(false)
                            history.push('/uploadIcon')

                        }}><IonIcon icon={colorPalette}></IonIcon><span style={{ marginLeft: '10px' }}>更改頭像</span></IonItem>

                        <IonItem button onClick={() => {
                            setShowModal(false)
                            history.push('/fillIn/one')
                        }

                        }><IonIcon icon={create}></IonIcon><span style={{ marginLeft: '10px' }}>更改個人資料</span></IonItem>

                        <IonItem button onClick={logout}><IonIcon icon={power}></IonIcon><span style={{ marginLeft: '10px' }}>登出</span></IonItem>
                        <div style={{width: '100px', height: '100px'}}></div>

                    </IonList>
                </IonModal>
            }

        </div>

    )
}

export default UserIcon