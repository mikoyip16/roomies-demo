import React from "react";
import './scss/NavBar.scss'
import { Link } from 'react-router-dom';
import navHome from '../../images/navicon/nav-home.svg'
import navHouse from '../../images/navicon/nav-house.svg'
import navSocial from '../../images/navicon/nav-social.svg'
import navRoomie from '../../images/navicon/nav-rommie.svg'

const Navbar = () => {
    return (
        <div className="navbar">
            <Link to='/'>
                <img style={{ height: '65%' }} src={navHome} alt="" />
            </Link>
            <Link to='/site'>
                <img style={{ height: '65%' }} src={navHouse} alt="" />
            </Link>
            <Link to='/roommate'>
                <img style={{ height: '65%' }} src={navRoomie} alt="" />
            </Link>
            <Link to='/gathering'>
                <img style={{ height: '65%' }} src={navSocial} alt="" />
            </Link>
        </div>
    )
}

export default Navbar