import { IonIcon } from '@ionic/react';
import { notificationsOutline, notificationsSharp } from 'ionicons/icons';
import React from 'react';
import { Link } from 'react-router-dom';
import './scss/Notification.scss'

const Notification = () => {
    return (
        <Link to="/notification" style={{ marginRight: '5vw', display: 'flex', alignItems: 'flex-end', height: '3rem' }}>
            <IonIcon icon={notificationsSharp} mode='md' size='large' style={{ color: '#C4C4C4', }}></IonIcon>
        </Link>
    )
}
export default Notification
