import React, { useState } from 'react'

const RangeOption = (props: {
    min?: number
    max?: number
    onChange: (selected: boolean) => void
}) => {
    const { max, min, onChange } = props
    const [selected, setSelected] = useState(false)
    return (
        <div className="choice"
            style={{
                border: selected ? '2px solid #e96443' : '2px solid #c4c4c4',

            }}
            onClick={() => {
                setSelected(!selected)
                onChange(!selected)
            }}
        >
            {!min ? (
                <>{max} 呎以下</>
            ) : !max ? (
                <> {min} 呎以上</>
            ) : (
                <>
                    {min} - {max} 呎
                </>
            )}
        </div>
    )
}

export default RangeOption