import React, { useState } from 'react'

const ItemOption = (props: {
    item: string
    onChange: (selected: boolean) => void
    isReset: boolean
}) => {
    const { item, onChange, isReset } = props
    const [selectedItem, setSelectedItem] = useState(false)

    return (
        <div className="choice"
            style={{
                border: selectedItem ? '2px solid #e96443' : '2px solid #c4c4c4',
            }}
            onClick={() => {

                setSelectedItem(!selectedItem)
                onChange(!selectedItem)
            }}

        >

            {item}
        </div>
    )
}

export default ItemOption