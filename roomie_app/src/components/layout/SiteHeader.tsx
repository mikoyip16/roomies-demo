import React from 'react'
import { Link } from 'react-router-dom'
import './scss/SiteHeader.scss'

interface ISiteHeaderState {
    title: string,
    previousPage: string,
}



const SiteHeader = (props: ISiteHeaderState) => {

    return (
        <div className="siteHeader" style={{ zIndex: 99999 }}>
            <Link to={props.previousPage}>
                <svg width="8" height="15" viewBox="0 0 8 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M0.212184 6.91707L6.07891 0.241471C6.36186 -0.0804873 6.82059 -0.0804873 7.10351 0.241471L7.78777 1.02007C8.07023 1.34148 8.07077 1.86239 7.78897 2.18456L3.1395 7.50002L7.789 12.8154C8.0708 13.1376 8.07026 13.6585 7.7878 13.9799L7.10354 14.7585C6.82059 15.0805 6.36186 15.0805 6.07894 14.7585L0.212214 8.08293C-0.0707332 7.76101 -0.0707329 7.23903 0.212184 6.91707Z" fill="white" />
                </svg>
            </Link>
            <div id="address">{props.title}</div>
        </div>
    )
}

export default SiteHeader
