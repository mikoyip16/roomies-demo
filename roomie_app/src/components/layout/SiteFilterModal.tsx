import React, { useState } from 'react';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';
import { IonIcon } from '@ionic/react';
import { arrowDown } from 'ionicons/icons';
import RangeOption from './RangeOption';
import ItemOption from './ItemOption';
import { useDispatch, useSelector } from 'react-redux';
import { clearQueryAction, setQueryAction } from '../../redux/siteFilter/action';
import { querySiteThunk } from '../../redux/siteFilter/thunk';
import { IRootState } from '../../redux/store';
type Range = {
  min?: number
  max?: number
}

const useModalStyles = makeStyles((theme: Theme) =>
  createStyles({
    modal: {
      display: 'flex',
      justifyContent: 'center',
      marginTop: '8.5rem'
    },
    paper: {
      backgroundColor: theme.palette.background.paper,
      border: '0px 4px 4px rgba(0, 0, 0, 0.25);',
      borderRadius: '20px',
      height: '60vh',
      width: '84vw'
    },
  }),
);

export default function SiteFilterModal(props: {
  rangeList: Range[],
  furnitureList: string[],
  specialList: string[]

}) {
  const { rangeList, furnitureList, specialList } = props
  const classesModal = useModalStyles();
  const [open, setOpen] = React.useState(false);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const [selectedRangeList, setSelectedRangeList] = useState<Range[]>([])
  const [selectedFurnitureList, setSelectedFurnitureList] = useState<string[]>([])
  const [selectedSpecialList, setSelectedSpecialList] = useState<string[]>([])
  const [reset, setReset] = useState(false)
  const dispatch = useDispatch()
  const query = useSelector((state: IRootState) => state.siteFilter.query) !
  return (
    <div>
      <div className='button'
        onClick={handleOpen}
        style={{}}
      >篩選<IonIcon icon={arrowDown} mode='md'></IonIcon></div>
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classesModal.modal}
        open={open}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={open}>
          <div className={classesModal.paper}>
            <div className='filter-container-s' >
              <div className='orange-label' >
                實用面積
              </div>
              <div className="grey-options" >
                {rangeList.map((range, i) => (
                  <RangeOption
                    key={i}
                    min={range.min}
                    max={range.max}
                    onChange={selected => {
                      if (selected) {
                        setSelectedRangeList([...selectedRangeList, range])
                      } else {
                        setSelectedRangeList(
                          selectedRangeList.filter(
                            eachRange =>
                              !(
                                eachRange.min === range.min &&
                                eachRange.max === range.max
                              ),
                          ),
                        )
                      }
                    }}
                  />
                ))}


              </div>
            </div>
            <div className='filter-container-s' >

              <div className='orange-label' >
                傢俬設備
              </div>
              <div className="grey-options" >
                {furnitureList.map((furniture, i) => (
                  <ItemOption
                    key={i}
                    item={furniture}
                    isReset={reset}
                    onChange={selectedItem => {

                      if (selectedItem) {
                        setSelectedFurnitureList([...selectedFurnitureList, furniture])
                      } else {
                        setSelectedFurnitureList(
                          selectedFurnitureList.filter(
                            eachItem =>
                              !(
                                eachItem === furniture
                              ),
                          ),
                        )
                      }
                    }}

                  />
                ))}
              </div>
            </div>
            <div className='filter-container-s' >
              <div className='orange-label' >
                特色單位
              </div>
              <div className="grey-options" >
                {specialList.map((special, i) => (
                  <ItemOption

                    key={i}
                    item={special}
                    isReset={reset}
                    onChange={selectedItem => {

                      if (selectedItem) {

                        setSelectedSpecialList([...selectedSpecialList, special])
                      } else {
                        setSelectedSpecialList(
                          selectedFurnitureList.filter(
                            eachItem =>
                              !(
                                eachItem === special
                              ),
                          ),
                        )
                      }



                    }}
                  />
                ))}
              </div>
            </div>
            <div className='confirm-btn-container' >
              <div className='btn' onClick={() => {
                setSelectedRangeList([])
                setSelectedSpecialList([])
                setSelectedFurnitureList([])
                setReset(true)
                dispatch(clearQueryAction())
              }}>重置</div>
              <div className='btn' onClick={e => {
                dispatch(setQueryAction({
                  rangeList: selectedRangeList,
                  requirementList: selectedFurnitureList,
                  specialListList: selectedSpecialList
                }))
                handleClose()
                
              }}>確認</div>
            </div>
          </div>
        </Fade>
      </Modal>
    </div>
  );
}
