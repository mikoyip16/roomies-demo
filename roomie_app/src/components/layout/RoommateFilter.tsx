import React, { useState } from 'react';
import { IonIcon } from '@ionic/react';
import { arrowDown } from 'ionicons/icons';
import RoommateFilterModal from './RoommateFilterModal'
import { useDispatch, useSelector } from 'react-redux';
import RentSlider from './RentSlider';
import './scss/RoommateFilter.scss'
import { IRootState } from '../../redux/store';
import { queryRoommateThunk } from '../../redux/roommateFilter/thunk';
import { setQueryAction } from '../../redux/roommateFilter/action';


const RoommateFilter = () => {
    const ethnicityList: string[] = ['香港', '中國內地', '台灣']
    const religionList: string[] = ['佛教', '基督教', '天主教', '伊斯蘭教', '沒有宗教信仰', '其他']

    const languageList: string[] = [
        '廣東話',
        '普通話',
        '英文',
        '法文',
        '德文',
        '日文',
        '西班牙文',
        '韓文',
        '其他'
    ]
    const occupationList: string[] = [
        '學生',
        '航空',
        '法律',
        '醫療',
        '建築',
        '餐飲',
        '零售',
        '金融',
        '教育',
        '體育',
        '娛樂',
        '製造',
        '貿易',
        '行政',
        '房地產',
        '物流運輸',
        '資訊科技',
        '農林牧漁',
        '已退休',
        '自僱',
        '其他'
    ]
    const areaList: string[] = ['中西區', '東區', '南區',
        '灣仔區',
        '九龍城區',
        '觀塘區',
        '深水埗區',
        '黃大仙區',
        '油尖旺區',
        '葵青區',
        '北區',
        '西貢區',
        '沙田區',
        '大埔區',
        '荃灣區',
        '屯門區',
        '元朗區',
        '離島區',]

    const genderList: string[] = [
        '男',
        '女',
        '其他'
    ]
    const specialRequirementList: string[] = ['無寵物', '不吸煙', '無孩子']

    const ageArr: string[] = ['18-25歲', '26-30歲', '30-40歲', '40歲以上']
    const [roommateFiltered, setRoommateFiltered] = useState({
        ethnicity: '',
        religion: '',
        language: '',
        occupation: '',
        area: '',
        gender: '',

    })
    const dispatch = useDispatch()
   
    return (
        <form className='roommate-filter'>
            <div className='line'>
                <label htmlFor='ethnicity'>
                    <select name='ethnicity' 
                        onChange={e => {
                            dispatch(setQueryAction({ ethnicity: e.target.value }))
                            
                        }}>
                        <option selected disabled value="來自">來自</option>
                        <option value="">所有</option>
                        {ethnicityList.map((value, i) => (
                            <option key={i} value={`${value}`}>
                                {value}
                            </option>
                        ))}
                    </select>
                </label>
                <label htmlFor='religion'>
                    <select name='religion' 
                        onChange={e => {
                            dispatch(setQueryAction({ religion: e.target.value }))
                         
                        }
                        }>
                        <option selected disabled value="宗教">宗教</option>
                        <option value="">所有</option>
                        {religionList.map((value, i) => (
                            <option key={i} value={`${value}`}>
                                {value}
                            </option>
                        ))}
                    </select>
                </label>
                <label htmlFor='language'>
                    <select name='language' 
                        onChange={e => {
                            dispatch(setQueryAction({ language: e.target.value }))
                      
                        }}

                    >
                        <option selected disabled value="語言">語言</option>
                        <option value="">所有</option>
                        {languageList.map((value, i) => (
                            <option key={i} value={`${value}`}>
                                {value}
                            </option>
                        ))}
                    </select>
                </label>
                <label htmlFor='occupation'>
                    <select name='occupation'
                        onChange={e => {
                            dispatch(setQueryAction({ occupation: e.target.value }))
                            
                        }

                        }>
                        <option selected disabled value="職業">職業</option>
                        <option value="">所有</option>
                        {occupationList.map((value, i) => (
                            <option key={i} value={`${value}`}>
                                {value}
                            </option>
                        ))}
                    </select>
                </label>
            </div>
            <div className='line'>
                <div className='rent'>
                    <RentSlider />
                </div>
                <label htmlFor='area'>
                    <select name='area' 
                        onChange={e => {
                            dispatch(setQueryAction({ preferredLocation: e.target.value }))
                        
                        }

                        }>
                        <option selected disabled value="地區">地區</option>
                        <option value="">所有</option>
                        {areaList.map((value, i) => (
                            <option key={i} value={`${value}`}>
                                {value}
                            </option>
                        ))}
                    </select>
                </label>
                <label htmlFor='gender'>
                    <select name='gender' 
                    onChange={e => 
                        {
                            dispatch(setQueryAction({ gender: e.target.value }))
                            
                        }
                    }>
                        <option selected disabled value="性別">性別</option>
                        <option value="">所有</option>
                        {genderList.map((value, i) => (
                            <option key={i} value={`${value}`}>
                                {value}
                            </option>
                        ))}
                    </select>
                </label>
                <RoommateFilterModal
                    specialRequirementList={specialRequirementList}
                    ageList={ageArr}
                />
            </div>
        </form>
    )
}



export default RoommateFilter