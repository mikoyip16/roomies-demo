import React, { useEffect } from 'react';
import { BrowserRouter as Router, Switch, Route, useHistory } from 'react-router-dom';
import { IonApp, IonRouterOutlet } from '@ionic/react';
import { IonReactRouter } from '@ionic/react-router';
import { Deeplinks } from '@ionic-native/deeplinks';

import HomePage from './components/pages/HomePage';
import SitePage from './components/pages/SitePage';
import RoommatePage from './components/pages/RoommatePage'
import LoginPage from './components/pages/LoginPage'
import SignUpPage from './components/pages/SignUpPage'
import FillInUserInformationPageOne from './components/pages/FillInUserInformationPageOne'
import FillInUserInformationPageTwo from './components/pages/FillInUserInformationPageTwo'
import UploadIconPage from './components/pages/UploadIconPage'
import SignUpPageSuccess from './components/pages/SignUpPageSuccess'
import CreateEventPage from './components/pages/CreateEventPage';
import User from './components/roommate/User'
import SiteMeetingPageDetail from './components/pages/SiteMeetingPageDetail';
import GatheringPageDetail from './components/pages/GatheringPageDetail'

/* Core CSS required for Ionic components to work properly */
import '@ionic/react/css/core.css';

/* Basic CSS for apps built with Ionic */
import '@ionic/react/css/normalize.css';
import '@ionic/react/css/structure.css';
import '@ionic/react/css/typography.css';

/* Optional CSS utils that can be commented out */
import '@ionic/react/css/padding.css';
import '@ionic/react/css/float-elements.css';
import '@ionic/react/css/text-alignment.css';
import '@ionic/react/css/text-transformation.css';
import '@ionic/react/css/flex-utils.css';
import '@ionic/react/css/display.css';

// import "react-slideshow-image/dist/styles.css"

import './App.css';
import GatheringPage from './components/pages/GatheringPage';
import GatheringCatPage from './components/pages/GatheringCatPage';
import GatheringByCatPage from './components/pages/GatheringByCatPage';
import NotificationPage from './components/pages/NotificationPage';
import { PrivateRoute } from './components/pages/PrivateRoute';
import { loginSuccessAction } from './redux/auth/action';
import { useDispatch } from 'react-redux';


/* Theme variables */
// import './theme/variables.css';

const DeepLinkMonitor = () => {
    const history = useHistory()
    useEffect(() => {
        let sub = Deeplinks.route({}).subscribe(
            match => {
                console.log('match:', match)
            },
            nomatch => {
                if (!nomatch || !nomatch.$link) return
                console.log('nomatch:', nomatch)
                const { path, queryString } = nomatch.$link
                if (path !== '/') return
                const params = new URLSearchParams(queryString)
                const token = params.get('jwt_token')
                const target = params.get('target')
                console.log(token);
                console.log(target);
                // if (token) {
                //     const dispatch = useDispatch()
                //     localStorage.setItem('roomie_token', token);
                //     dispatch(loginSuccessAction(token));
                //     console.log("Token SET!!!!!!!!!!!!")
                // }
                if (!token) return
                if (target == 'home') history.push('/?' + queryString)
                else if (target == 'signUp') history.push('/signUp/success?' + queryString)
                else history.push('/?' + queryString)

            },
        )
        return () => {
            sub.unsubscribe()
        }
    }, [history])
    return <></>
}

function App() {
    // useEffect(() => {
    //     Deeplinks.route({
    //         '/signUp/success': SignUpPage

    //     }).subscribe((match: any) => {
    //         // match.$route - the route we matched, which is the matched entry from the arguments to route()
    //         // match.$args - the args passed in the link
    //         // match.$link - the full link data
    //         console.log('Successfully matched route', match);
    //     }, (nomatch: any) => {
    //         // nomatch.$link - the full link data
    //         console.error('Got a deeplink that didn\'t match', nomatch);
    //     });
    // }, [])


    useEffect(() => {
        let script = document.createElement("script");
        script.src = "/custom-sw.js";
        script.async = true;
        document.body.appendChild(script);
    }, [])
    return (
        <IonApp>
            <IonReactRouter>
                <DeepLinkMonitor />
                <IonRouterOutlet>
                    <Route exact path="/" component={HomePage} />
                    <Route exact path="/site" component={SitePage} />
                    <Route exact path='/roommate' component={RoommatePage} />
                    <Route exact path='/return/findRoommate' component={RoommatePage} />
                    <Route exact path='/createEvent' component={CreateEventPage} />
                    <Route exact path='/create-event' component={CreateEventPage} />
                    <Route exact path='/site/detail' component={SiteMeetingPageDetail} />
                    <Route exact path='/gathering/detail' component={GatheringPageDetail} />
                    <Route exact path='/gathering' component={GatheringCatPage} />
                    <Route exact path='/gatheringList' component={GatheringByCatPage} />
                    <Route exact path='/login' component={LoginPage} />
                    <Route exact path='/signUp' component={SignUpPage} />
                    <Route exact path='/signUp/success' component={SignUpPageSuccess} />
                    <PrivateRoute exact path='/fillIn/one' component={FillInUserInformationPageOne} />
                    <PrivateRoute exact path='/fillIn/two' component={FillInUserInformationPageTwo} />
                    <PrivateRoute exact path='/uploadIcon' component={UploadIconPage} />
                    <PrivateRoute exact path='/user/search' component={User} />
                    <Route exact path='/notification' component={NotificationPage} />
                </IonRouterOutlet>
            </IonReactRouter>
        </IonApp>

    );
}

export default App;
