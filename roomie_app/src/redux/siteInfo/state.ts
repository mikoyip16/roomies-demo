export interface ISiteInfoState {
    address: string;
    siteImage: Array<any>;
    title: string;
    map_x: number;
    map_y: number;
    rent: number | null;
    gross_area: number | null;
    usable_area: number | null;
    rooms: number | null;
    years: number | null;
    view: string | null;
    television: boolean | null;
    air_con: boolean | null;
    washing_machine: boolean | null;
    water_heater: boolean | null;
    microwave: boolean | null;
    fridge: boolean | null;
    bed: boolean | null;
    meeting_id: string;
    meeting_date: string;
    image: string;
}

export const initialSiteMeeingState: ISiteInfoState = {
    address: "",
    siteImage: [],
    title: "",
    rent: null,
    map_x: 0,
    map_y: 0,
    gross_area: null,
    usable_area: null,
    rooms: null,
    years: null,
    view: null,
    television: null,
    air_con: null,
    washing_machine: null,
    water_heater: null,
    microwave: null,
    fridge: null,
    bed: null,
    meeting_id: "",
    meeting_date: "",
    image: ""
}



