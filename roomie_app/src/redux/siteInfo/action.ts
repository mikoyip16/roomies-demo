import { ISiteInfoState } from "./state"

export function setSiteInfo(
    address: string,
    siteImage: Array<string>,
    title: string,
    rent: number,
    gross_area: number,
    usable_area: number,
    rooms: number,
    years: number,
    view: string,
    television: boolean,
    air_con: boolean,
    washing_machine: boolean,
    water_heater: boolean,
    microwave: boolean,
    fridge: boolean,
    bed: boolean,
    map_x: number,
    map_y: number,
    meeting_id: string,
    meeting_date: string,
    image: string) {
    return {
        type: "@@SITEINFO/SET_SITE_INFO" as const,
        address,
        siteImage,
        title,
        rent,
        gross_area,
        usable_area,
        rooms,
        years,
        view,
        television,
        air_con,
        washing_machine,
        water_heater,
        microwave,
        fridge,
        bed,
        map_x,
        map_y,
        meeting_id,
        meeting_date,
        image
    }
}



export type ISiteInfoAction =
    | ReturnType<typeof setSiteInfo>