import { Dispatch } from 'redux'
import { ISiteInfoAction, setSiteInfo } from './action'


const demoSiteID = 36; 


export const setSiteInfosThunk = (siteID: string) => {
    return async (dispatch: Dispatch<ISiteInfoAction>) => {
        try {
            const host = process.env.REACT_APP_API_SERVER
            const res = await fetch(`${host}/siteDetail/${siteID}`) //而家backend同fontend分開左，就要由http開始寫
            console.log("res")
            console.log(res);
            if (res.status !== 200) {
                console.log("error")
            } else {
                const totalJsonDate = await res.json();
                const students = totalJsonDate.students;
                const siteDetailjsonData = totalJsonDate.payload.siteInfo;
                const siteImagejsonData = totalJsonDate.payload.siteImage;
                const { address, image, title, rent,
                    gross_area,
                    usable_area,
                    rooms,
                    years,
                    view,
                    television,
                    air_con,
                    washing_machine,
                    water_heater,
                    microwave,
                    fridge,
                    bed, map_x, map_y, meeting_id, meeting_date } = siteDetailjsonData[0];

                console.log(`siteDetailjsonData = ${siteDetailjsonData}`)
                dispatch(setSiteInfo(
                    address,
                    siteImagejsonData,
                    title,
                    rent,
                    gross_area,
                    usable_area,
                    rooms,
                    years,
                    view,
                    television,
                    air_con,
                    washing_machine,
                    water_heater,
                    microwave,
                    fridge,
                    bed, map_x, map_y, meeting_id, meeting_date, image));
            }

        } catch (err) {
            // console.log(err.message);

        }
    }
}