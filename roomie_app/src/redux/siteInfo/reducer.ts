import { ISiteInfoAction } from "./action";
import { initialSiteMeeingState, ISiteInfoState } from "./state";


export const siteInfoReducer = (state: ISiteInfoState = initialSiteMeeingState, action: ISiteInfoAction): ISiteInfoState => { //加左, action:ICounterAction
    switch (action.type) {
        case "@@SITEINFO/SET_SITE_INFO":
            return {
                ...state,
                address: action.address,
                siteImage: action.siteImage,
                title: action.title,
                rent: action.rent,
                gross_area: action.gross_area,
                usable_area: action.usable_area,
                rooms: action.rooms,
                years: action.years,
                view: action.view,
                television: action.television,
                air_con: action.air_con,
                washing_machine: action.washing_machine,
                water_heater: action.water_heater,
                microwave: action.microwave,
                fridge: action.fridge,
                bed: action.bed,
                map_x: action.map_x,
                map_y: action.map_y,
                meeting_id: action.meeting_id,
                meeting_date: action.meeting_date,
                image: action.image
            }
            break;


        default:
            return state;
            break;
    }
}