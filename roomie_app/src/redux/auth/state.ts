import { JWTPayload } from '../types'

export type AuthState = {
    isAuthenticated: boolean
    isLoading: boolean
    errorMessage?: string
    payload?: JWTPayload
}