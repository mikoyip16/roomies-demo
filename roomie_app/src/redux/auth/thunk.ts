import {
    NativeLoginInput,
    NativeLoginOutput,
    FacebookLoginInput,
    FacebookLoginOutput,
} from '../types';
import { callAPI } from '../../helpers/api';
import { IRootThunkDispatch } from '../store';
import { loginFailAction, loginSuccessAction, logoutAction, loadingLoginAction } from './action';

import { History } from 'history';

export function autoLoginThunk() {
    return async (dispatch: IRootThunkDispatch) => {
        let token = localStorage.getItem('roomie_token');
        if (token) {
            dispatch(loginSuccessAction(token));
        } else {
            dispatch(logoutAction());
        }
    };
}

export function nativeLoginThunk(username: string, password: string, history: History) {
    return async (dispatch: IRootThunkDispatch) => {
        try {
            dispatch(loadingLoginAction());
            const data = await callAPI<NativeLoginInput, NativeLoginOutput>(
                'POST',
                '/user/login/native',
                {
                    username,
                    password,
                }
            ) as any;
            console.log(data)
            console.log(data.jwt_token);
            if (data.jwt_token) {
                localStorage.setItem('roomie_token', data.jwt_token);

                dispatch(loginSuccessAction(data.jwt_token));
                history.push('/');
            }
            else{
                dispatch(loginFailAction('Login Failed: ' + data.message));
                
               
            }
        } catch (error) {
            dispatch(loginFailAction('Login Failed: ' + error.toString()));
        }
    };
}

export function facebookLoginThunk(accessToken: string, history: History) {
    return async (dispatch: IRootThunkDispatch) => {
        try {
            dispatch(loadingLoginAction());
            const data = await callAPI<FacebookLoginInput, FacebookLoginOutput>(
                'POST',
                '/login/facebook',
                {
                    accessToken,
                }
            );
            localStorage.setItem('roomie_token', data.jwt_token);
            dispatch(loginSuccessAction(data.jwt_token));
            history.push('/');
        } catch (error) {
            dispatch(loginFailAction('Facebook Login Failed: ' + error.toString()));
        }
    };
}

export function logoutThunk(history: History) {
    return (dispatch: IRootThunkDispatch) => {
        localStorage.removeItem('roomie_token');
        localStorage.removeItem('userInfo');
        localStorage.removeItem('allSites');
        dispatch(logoutAction());
        history.push('/login');
    };
}
