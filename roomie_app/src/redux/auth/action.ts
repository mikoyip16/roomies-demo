export function loginSuccessAction(token: string) {
    return {
        type: '@@auth/login_success' as const,
        token,
    }
}

export function loginFailAction(errorMessage: string) {
    return {
        type: '@@auth/login_fail' as const,
        errorMessage,
    }
}

export function logoutAction() {
    return {
        type: '@@auth/logout' as const,
    }
}

export function loadingLoginAction() { 
    return {
        type: '@@auth/loading' as const,
    }
}

export function updateIconAction(token: string){
    return{
        type: '@@update/icon_success' as const,
        token
    }
}

export type AuthAction =
    | ReturnType<typeof loginSuccessAction>
    | ReturnType<typeof loginFailAction>
    | ReturnType<typeof logoutAction>
    | ReturnType<typeof loadingLoginAction>
    | ReturnType<typeof updateIconAction>