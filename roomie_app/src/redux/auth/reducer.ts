import { AuthState } from './state';
import { AuthAction } from './action';
import { JWTPayload } from '../types';
import jwtDecode from 'jwt-decode';

const initialState: AuthState = {
    isAuthenticated: !!localStorage.getItem('token'),
    isLoading: true,
    payload: undefined,
};

export const authReducer = (state: AuthState = initialState, action: AuthAction): AuthState => {
    switch (action.type) {
        case '@@auth/loading': {
            return {
                ...state,
                isAuthenticated: false,
                isLoading: true,
            };
        }
        case '@@auth/login_success': {
            try {
                let payload = jwtDecode<JWTPayload>(action.token);
                return {
                    ...state,
                    isAuthenticated: true,
                    isLoading: false,
                    payload,
                };
            } catch (error) {
                return {
                    ...state,
                    isAuthenticated: false,
                    isLoading: false,
                    errorMessage: 'Failed to decode JWT: ' + error.toString(),
                };
            }
        }
        case '@@auth/login_fail': {
            let errorMessage = action.errorMessage;
            return{
                isAuthenticated: false,
                isLoading: false,
                errorMessage: errorMessage
            }
        }
        case '@@auth/logout': {
            return {
                ...state,
                isAuthenticated: false,
                isLoading: false,
            };
        }
        case '@@update/icon_success': {
            try {
                let payload = jwtDecode<JWTPayload>(action.token);
                return {
                    ...state,
                    isAuthenticated: true,
                    isLoading: false,
                    payload,
                };
            } catch (error) {
                return {
                    ...state,
                    isAuthenticated: false,
                    isLoading: false,
                    errorMessage: 'Failed to decode JWT: ' + error.toString(),
                };
            }
        }
        default:
            return state;
    }
};
