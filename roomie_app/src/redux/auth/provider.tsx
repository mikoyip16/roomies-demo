import React, { useEffect } from "react";
import { useDispatch } from "react-redux";
import { autoLoginThunk } from './thunk'
import { History} from 'history'

export function AuthProvider() {
    const dispatch = useDispatch()
    useEffect(() => {
        dispatch(autoLoginThunk())
        
    }, [dispatch])
    return <></> 
}