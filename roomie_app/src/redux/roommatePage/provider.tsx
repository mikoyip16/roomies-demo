import React, { useEffect } from "react";
import { useDispatch } from "react-redux";
import { displayAllRoommateThunk } from './thunk'


export function RoommatePageProvider() {
    const dispatch = useDispatch()
    useEffect(() => {
        dispatch(displayAllRoommateThunk())
        
    }, [])
    return <></>
}