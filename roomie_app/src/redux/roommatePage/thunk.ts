import { callAPI } from "../../helpers/api";
import { IRootThunkDispatch } from "../store";
import { displayAllRoommateSuccessAction, displayAllRoommateFailAction, loadingRoommateAction } from './action'

export function displayAllRoommateThunk(){
    return async (dispatch:IRootThunkDispatch) => {
        try{
            dispatch(loadingRoommateAction());
            const reply = await callAPI(
                'GET',
                '/findRoommate',
            ) as any
            dispatch(displayAllRoommateSuccessAction(reply))
        }catch(err){
            dispatch(displayAllRoommateFailAction('failed to fetch roommate data' + err.toString()))
        }
    }
}

