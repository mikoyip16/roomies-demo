import { RoommatePageData } from "../types";

export function displayAllRoommateSuccessAction(data: RoommatePageData[]){
    return{
        type: '@@roommate/display_all_success' as const,
        data
    }
}

export function displayAllRoommateFailAction(errorMessage: string){
    return{
        type: '@@roommate/display_all_fail' as const,
        errorMessage
    }
}

export function loadingRoommateAction(){
    return{
        type: '@@roommate/dispaly_all_loading' as const
    }
}


export type RoommatePageAction =
    | ReturnType<typeof displayAllRoommateSuccessAction>
    | ReturnType<typeof displayAllRoommateFailAction>
    | ReturnType<typeof loadingRoommateAction>
    
    