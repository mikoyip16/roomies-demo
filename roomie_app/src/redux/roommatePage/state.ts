import { RoommatePageData } from "../types";

export type RoommatePageState = {
    isDisplayed:boolean
    isLoading: boolean
    errorMessage?: string
    payload?: RoommatePageData[]
}