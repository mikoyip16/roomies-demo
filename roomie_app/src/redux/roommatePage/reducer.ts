import { RoommatePageState } from "./state";
import { RoommatePageAction } from './action';

const initialState: RoommatePageState = {
    isDisplayed: false,
    isLoading: true,
}

export const roommatePageReducer = (
    state: RoommatePageState = initialState,
    action: RoommatePageAction
): RoommatePageState => {
    switch(action.type){
        case '@@roommate/dispaly_all_loading':{
            return{
                ...state,
                isDisplayed: false,
                isLoading: true,
            }
        }
        case '@@roommate/display_all_success':{
            let payload = action.data
            return{
                ...state,
                isDisplayed: true,
                isLoading: false,
                payload
            }
        }
        case '@@roommate/display_all_fail': {
            return{
                ...state,
                isDisplayed: false,
                isLoading: false,
                errorMessage: 'Failed to display roommates'
            }
        }

        default:
            return state
    }
}