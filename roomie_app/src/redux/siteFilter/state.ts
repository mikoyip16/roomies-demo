import { SitePageData, Range } from '../types'

export type SiteFilterState = {
    hasQuery: boolean
    isDisplayed: boolean
    isLoading: boolean
    errorMessage?: string
    query?: {
        rangeList?: Range[],
        rentRange?: number[],
        area?: string,
        compartment?: string,
        requirementList?: string[],
        specialListList?: string[]
    }
    searchText?: string
    payload?: SitePageData[]    
}