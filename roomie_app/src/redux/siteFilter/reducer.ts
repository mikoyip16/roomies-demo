import { SiteFilterAction } from './action';
import { SiteFilterState } from './state';



const initialState: SiteFilterState = {
    hasQuery: false,
    isDisplayed: false,
    isLoading: false,
};

export const siteFilterReducer = (
    state: SiteFilterState = initialState,
    action: SiteFilterAction
): SiteFilterState => {
    switch (action.type) {
        case '@@site/set_query': {
            return {
                ...state,
                hasQuery: true,
                isDisplayed: false,
                isLoading: true,
                query: {
                    ...state.query,
                    ...action.query,
                },
               
                
            };
        }
        case '@@site/filter_success': {
            let payload = action.data;
            return {
                ...state,
                hasQuery: true,
                isDisplayed: true,
                isLoading: false,
                payload,
            };
        }
        case '@@site/filter_loading': {
            return {
                ...state,
                hasQuery: true,
                isDisplayed: false,
                isLoading: true,
            };
        }
        case '@@site/filter_fail': {
            return {
                ...state,
                hasQuery: true,
                isDisplayed: false,
                isLoading: true,
                errorMessage: 'Failed to display sites',
            };
        }
        case '@@site/filter_clearQuery':{
            let payload = undefined
            return{
                hasQuery: false,
                isDisplayed: false,
                isLoading: false,
                payload,
            }
        }
        case '@@site/filter_setSearchText':{
            let searchText = action.searchText
            return{
                hasQuery: false,
                isDisplayed: false,
                isLoading: false,
                payload: undefined,
                searchText: searchText
            }
        }
        default:
            return state;
    }
};
