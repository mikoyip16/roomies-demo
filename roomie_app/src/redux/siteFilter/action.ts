import { SitePageData, Range } from '../types';

export function setQueryAction(query: {
    rangeList?: Range[];
    rentRange?: number[];
    area?: string;
    compartment?: string;
    requirementList?: string[];
    specialListList?: string[];
}) {
    return {
        type: '@@site/set_query' as const,
        query,
    };
}
export function displayFilterSuccessAction(data: SitePageData[]) {
    return {
        type: '@@site/filter_success' as const,
        data,
    };
}

export function displayFilterFailAction(errorMessage: string) {
    return {
        type: '@@site/filter_fail' as const,
        errorMessage,
    };
}

export function loadingFilterAction() {
    return {
        type: '@@site/filter_loading' as const,
    };
}

export function clearQueryAction(){
    return{
        type: '@@site/filter_clearQuery' as const,

    }
}
export function searchSiteAction(searchText: string){
    return{
        type: '@@site/filter_setSearchText' as const,
        searchText
    }
}


export type SiteFilterAction =
    | ReturnType<typeof setQueryAction>
    | ReturnType<typeof displayFilterSuccessAction>
    | ReturnType<typeof displayFilterFailAction>
    | ReturnType<typeof loadingFilterAction>
    | ReturnType<typeof clearQueryAction>
    | ReturnType<typeof searchSiteAction>;
