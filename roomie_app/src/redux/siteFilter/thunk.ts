import { callAPI } from '../../helpers/api';
import { IRootThunkDispatch } from '../store';
import { Range } from '../types';
import {
    displayFilterSuccessAction,
    displayFilterFailAction,
    loadingFilterAction,
} from './action';

export function querySiteThunk(query: {
    rangeList?: Range[];
    rentRange?: number[];
    area?: string;
    compartment?: string;
    requirementList?: string[];
    specialListList?: string[];
}) {
    return async (dispatch: IRootThunkDispatch) => {
        try {
            console.log(query)
            dispatch(loadingFilterAction());
            const reply = (await callAPI('POST', '/sites', query)) as any;
            if (reply.result.length === 0) {
                dispatch(displayFilterFailAction('沒有符合要求的樓盤'));
            }
            dispatch(displayFilterSuccessAction(reply.result));
        } catch (err) {
            dispatch(displayFilterFailAction('failed to fetch site data' + err.toString()));
        }
    };
}


