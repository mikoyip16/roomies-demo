export interface ISiteAttendeesState {
    users_id: number | null;
    meeting_id: number | null;
    attended: boolean;
}

export const initialSiteAttendeesState: ISiteAttendeesState = {

    users_id: null,
    meeting_id: null,
    attended: false,
}


