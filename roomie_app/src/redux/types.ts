export type JWTPayload = {
    userID: number;
    username: string;
    nickname: string;
    image: string;
};

export type NativeLoginInput = {
    username: string;
    password: string;
};
export type NativeLoginOutput = {
    jwt_token: string;
};

export type SignupInput = NativeLoginInput;
export type SignupOutput = NativeLoginOutput;

export type FacebookLoginInput = {
    accessToken: string;
};
export type FacebookLoginOutput = NativeLoginOutput;

export type Range = {
    min?: number;
    max?: number;
};

export type FilterInput = {
    rangeList?: Range[];
    rentRange?: number[];
    area?: string;
    compartment?: string;
    requirementList?: string[];
    specialListList?: string[];
};

export type SitePageData = {
    id: number;
    image: string;
    title: string;
    rooms: number;
    usable_area: number;
    floor: number;
    years: number;
    district: string;
    address: string;
    rent: number;
    owner_id: number;
    updated_at: Date;
    date: Date;
};

export type RoommateFilterInput = {
    ethnicity?: string;
    religion?: string;
    language?: string;
    occupation?: string;
    rentRange?: number[];
    preferredLocation?: string;
    gender?: string;
    rentDate?: Date;
    rentDuration?: number;
    specialRequirementList?: string[];
    ageList?: string[];
};


export type RoommatePageData = {
    id: number;
    username: string;
    image: string;
    flower: number;
    egg: number;
    gender: string;
    nickname: string;
    age: string;
    introduction: string;
    education_level: string;
    ethnicity: string;
    occupation: string;
    language: string;
    preferred_location: string;
    budget_min: number;
    budget_max: number;
    rent_date: Date;
    rent_duration: number;
    preferred_gender: string;
    religion: string;
    not_smoking_text: string;
    have_no_kids_text: string;
    have_no_pet_text: string;
};

export type mapCoordinate = {
    lat: number;
    lng: number;
};

export type CoordinateList = {
    東區: mapCoordinate;
    中西區: mapCoordinate;
    南區: mapCoordinate;
    灣仔區: mapCoordinate;
    九龍城區: mapCoordinate;
    觀塘區: mapCoordinate;
    深水埗區: mapCoordinate;
    黃大仙區: mapCoordinate;
    油尖旺區: mapCoordinate;
    葵青區: mapCoordinate;
    北區: mapCoordinate;
    西貢區: mapCoordinate;
    沙田區: mapCoordinate;
    大埔區: mapCoordinate;
    荃灣區: mapCoordinate;
    屯門區: mapCoordinate;
    元朗區: mapCoordinate;
    離島區: mapCoordinate;
};

export const coordinateList:any = {
    東區: {
        lat:22.2756223,
        lng:114.1883877
    },
    中西區: {
        lat:22.2724542,
        lng: 114.1174916
    },

    南區: {
        lat:22.2394709,
        lng: 114.1185476
    },
    灣仔區: {
        lat:22.2739396,
        lng:114.1646805
    }, 
    九龍城區: {
        lat:22.3219416,
        lng:114.1717764
    },
    觀塘區: {
        lat:22.3104576,
        lng:114.2060891
    },
    深水埗區:{
        lat:22.3312086,
        lng: 114.1315615
    },
    黃大仙區: {
        lat: 22.3524512,
        lng: 114.1523155
    },
    油尖旺區: {
        lat:22.3099306,
        lng:114.1500485
    },
    葵青區: {
        lat: 22.3534098,
        lng: 114.0782376
    },
    北區: {
        lat:22.5124073,
        lng:114.0829366
    },
    西貢區: {
        lat:22.3669149,
        lng: 114.2382615
    },
    沙田區: {
        lat:22.3909943,
        lng:114.1386385
    },
    大埔區: {
        lat:22.4765848,
        lng:114.150072
    },
    荃灣區: {
        lat:22.3742205,
        lng: 114.0354634
    },
    屯門區:{
        lat:22.3953898,
        lng: 113.9437096
    },
    元朗區: {
        lat:22.4635019,
        lng:113.9588185
    },
    離島區: {
        lat:22.3543168,
        lng:113.8593663
    }
}