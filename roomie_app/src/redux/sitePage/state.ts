import { SitePageData } from '../types'

export type SitePageState = {
    isDisplayed: boolean
    isLoading: boolean
    errorMessage?: string
    payload?: SitePageData[]
}