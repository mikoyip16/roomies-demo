import { SitePageData } from '../types'

export function displayAllSuccessAction(data: SitePageData[]){
    return{
        type: '@@site/display_all_success' as const,
        data
    }
}

export function displayAllFailAction(errorMessage: string){
    return{
        type: '@@site/display_all_fail' as const,
        errorMessage
    }
}

export function loadingDisplayAction(){
    return {
        type: '@@site/display_all_loading' as const,
    }
}


export type SitePageAction = 
    | ReturnType<typeof displayAllSuccessAction>
    | ReturnType<typeof displayAllFailAction>
    | ReturnType<typeof loadingDisplayAction>
