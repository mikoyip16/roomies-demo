import React, { useEffect } from "react";
import { useDispatch } from "react-redux";
import { displayAllThunk } from './thunk'


export function SitePageProvider() {
    const dispatch = useDispatch()
    useEffect(() => {
        dispatch(displayAllThunk())
        
    }, [])
    return <></>
}