import { callAPI } from "../../helpers/api";
import { IRootThunkDispatch } from "../store";
import { displayAllSuccessAction, displayAllFailAction, loadingDisplayAction} from './action';

export function displayAllThunk(){
    return async (dispatch: IRootThunkDispatch) => {
        try{
            dispatch(loadingDisplayAction());
            const reply = await callAPI(
                'GET',
                '/sites',                
            ) as any
            dispatch(displayAllSuccessAction(reply.data))
            localStorage.setItem('allSites', reply.data);
        }catch(err){
            dispatch(displayAllFailAction('failed to fetch site data' + err.toString()))
        }
    }
}

