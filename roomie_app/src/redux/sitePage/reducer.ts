import { SitePageState } from './state';
import { SitePageAction } from './action';

const initialState: SitePageState = {
    isDisplayed: false,
    isLoading: true,
};

export const sitePageReducer = (
    state: SitePageState = initialState,
    action: SitePageAction
): SitePageState => {
    switch (action.type) {
        case '@@site/display_all_loading': {
            return {
                ...state,
                isDisplayed: false,
                isLoading: true,
            };
        }
        case '@@site/display_all_success': {
            let payload = action.data;
            return {
                ...state,
                isDisplayed: true,
                isLoading: false,
                payload,
            };
        }
        case '@@site/display_all_fail': {
            return {
                ...state,
                isDisplayed: false,
                isLoading: false,
                errorMessage: 'Failed to display sites',
            };
        }
        default:
            return state;
    }
};
