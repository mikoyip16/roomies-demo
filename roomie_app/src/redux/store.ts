import { ISiteInfoState } from './siteInfo/state';
import { siteInfoReducer } from './siteInfo/reducer';
import { Action, applyMiddleware, combineReducers, compose, createStore } from 'redux';
import {
    RouterState,
    connectRouter,
    routerMiddleware,
    CallHistoryMethodAction,
} from 'connected-react-router';
import { createBrowserHistory } from 'history';
import logger from 'redux-logger';
import { ISiteInfoAction } from './siteInfo/action';
import { AuthAction } from './auth/action';
import thunk, { ThunkDispatch } from 'redux-thunk';
import { AuthState } from './auth/state';
import { authReducer } from './auth/reducer';
import { SitePageState } from './sitePage/state';
import { SitePageAction } from './sitePage/action';
import { sitePageReducer } from './sitePage/reducer';
import { SiteFilterState } from './siteFilter/state';
import { siteFilterReducer } from './siteFilter/reducer';
import { SiteFilterAction } from './siteFilter/action';
import { RoommatePageState } from './roommatePage/state';
import { roommatePageReducer } from './roommatePage/reducer';
import { RoommatePageAction } from './roommatePage/action';
import { RoommateFilterState } from './roommateFilter/state';
import { roommateFilterReducer } from './roommateFilter/reducer';
import { RoommateFilterAction } from './roommateFilter/action';
import { RoommateInfoState } from './roommateInfo/state';
import { RoommateInfoAction } from './roommateInfo/action';
import { roommateInfoReducer } from './roommateInfo/reducer';
import { RegisterState } from './register/state';
import { RegisterAction } from './register/action';
import { registerReducer } from './register/reducer';

export const history = createBrowserHistory();

//step1: IRootState
export interface IRootState {
    siteInfo: ISiteInfoState;
    auth: AuthState;
    sitePage: SitePageState;
    siteFilter: SiteFilterState;
    roommatePage: RoommatePageState
    roommateFilter: RoommateFilterState
    roommateInfo: RoommateInfoState
    register: RegisterState
    router: RouterState;
}

//step2: IRootAction
export type IRootAction = CallHistoryMethodAction 
                        | ISiteInfoAction 
                        | AuthAction 
                        | SitePageAction 
                        | SiteFilterAction 
                        | RoommatePageAction 
                        | RoommateFilterAction
                        | RoommateInfoAction
                        | RegisterAction


//Step3: rootReducer
const rootReducer = combineReducers<IRootState>({
    siteInfo: siteInfoReducer,
    router: connectRouter(history),
    auth: authReducer,
    siteFilter: siteFilterReducer,
    sitePage: sitePageReducer,
    roommatePage: roommatePageReducer,
    roommateFilter: roommateFilterReducer,
    roommateInfo: roommateInfoReducer,
    register: registerReducer
});
//Step4: middleware
declare global {
    interface Window {
        __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: any;
    }
}
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export type IRootThunkDispatch = ThunkDispatch<IRootState, null, IRootAction>;

//step5: createStore
let store = createStore<IRootState, IRootAction, {}, {}>(
    rootReducer,
    composeEnhancers(
        applyMiddleware(logger),
        applyMiddleware(routerMiddleware(history)),
        applyMiddleware(thunk)
    )
);

export default store;
