export function registerFailAction(errorMessage: string ){
    return{
        type: '@@register/register_fail' as const,
        errorMessage
    }
}

export function loadingRegisterAction(){
    return {
        type: '@@register/loading' as const,
    }
}

export type RegisterAction =
    | ReturnType<typeof registerFailAction>
    | ReturnType<typeof loadingRegisterAction>