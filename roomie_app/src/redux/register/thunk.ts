import { push } from 'connected-react-router';
// import { body } from 'ionicons/icons';
// import { callAPI } from '../../helpers/api';
import { loginSuccessAction } from '../auth/action';
import { IRootThunkDispatch } from '../store';
import { registerFailAction, loadingRegisterAction } from './action';
import { History } from 'history';

export function registerThunk(username: string, password: string, email: string, nickname: string, history: History) {
    return async (dispatch: IRootThunkDispatch) => {
        try {
            dispatch(loadingRegisterAction());
            const host = process.env.REACT_APP_API_SERVER
            const res = await fetch(`${host}/user`, {
                method: 'POST',
                headers:{
                    "Content-Type": "application/json",
                },
                body: JSON.stringify({
                    username,
                    password,
                    email,
                    nickname
                })
            }) as any
            
            if(res.status !== 200){
                console.log('error')
            }else{
                const data = await res.json()
                console.log(data)
                localStorage.setItem('roomie_token', data.jwt_token);
                dispatch(loginSuccessAction(data.jwt_token));
                history.push('/signUp/success')
            }
            
        } catch (error) {
            dispatch(registerFailAction('Fail to register new user' + error.toString()));
        }
    };
}
