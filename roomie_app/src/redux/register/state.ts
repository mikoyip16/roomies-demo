

export type RegisterState = {
    isRegistered: boolean
    isLoading: boolean
    errorMessage?: string
}

