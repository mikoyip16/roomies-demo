import { RegisterState } from './state';
import { RegisterAction } from './action';

const initialState: RegisterState = {
    isRegistered: !!localStorage.getItem('token'),
    isLoading: true,
};

export const registerReducer = (
    state: RegisterState = initialState,
    action: RegisterAction
): RegisterState => {
    switch (action.type) {
        case '@@register/loading': {
            return {
                ...state,
                isRegistered: false,
                isLoading: true,
            };
        }
        default:
            return state;
    }
};
