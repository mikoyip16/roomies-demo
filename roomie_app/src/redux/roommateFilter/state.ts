import { RoommatePageData } from '../types';

export type RoommateFilterState = {
    hasQuery: boolean
    isDisplayed: boolean
    isLoading: boolean
    errorMessage?: string
    query?: {
        ethnicity?: string;
        religion?: string;
        language?: string;
        occupation?: string;
        rentRange?: number[];
        preferredLocation?: string;
        gender?: string;
        rentDate?:any;
        rentDuration?: any;
        specialRequirementList?: string[];
        ageList?: string[];
    }
    searchText?: string
    payload?: RoommatePageData[]
};
