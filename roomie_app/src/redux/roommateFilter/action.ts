import { RoommatePageData } from '../types';

export function setQueryAction(query: {
    ethnicity?: string;
    religion?: string;
    language?: string;
    occupation?: string;
    rentRange?: number[];
    preferredLocation?: string;
    gender?: string;
    rentDate?: Date;
    rentDuration?: number;
    specialRequirementList?: string[];
    ageList?: string[];
}) {
    return {
        type: '@@roommate/set_query' as const,
        query,
    };
}
export function displayFilRoommateSuccessAction(data: RoommatePageData[]) {
    return {
        type: '@@roommate/filter_success' as const,
        data,
    };
}
export function displayFilRoommateFailAction(errorMessage: String) {
    return {
        type: '@@roommate/filter_fail' as const,
    };
}
export function loadingFilRoommateAction() {
    return {
        type: '@@roommate/filter_loading' as const,
    };
}
export function clearQueryAction(){
    return{
        type: '@@roommate/filter_clearQuery' as const,
    }
}
export function searchRoommateAction(searchText: string){
    return{
        type: '@@roommate/filter_setSearchText' as const,
        searchText
    }
}

export type RoommateFilterAction = 
    | ReturnType<typeof setQueryAction>
    | ReturnType<typeof displayFilRoommateSuccessAction>
    | ReturnType<typeof displayFilRoommateFailAction>
    | ReturnType<typeof loadingFilRoommateAction>
    | ReturnType<typeof clearQueryAction>
    | ReturnType<typeof searchRoommateAction>