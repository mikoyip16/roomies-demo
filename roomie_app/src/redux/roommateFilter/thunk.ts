
import { callAPI } from '../../helpers/api';
import { IRootThunkDispatch } from '../store';
import {
    displayFilRoommateSuccessAction,
    displayFilRoommateFailAction,
    loadingFilRoommateAction,
} from './action'

export function queryRoommateThunk(query:{
    ethnicity?: string;
    religion?: string;
    language?: string;
    occupation?: string;
    rentRange?: number[];
    preferredLocation?: string;
    gender?: string;
    rentDate?: Date;
    rentDuration?: number;
    specialRequirementList?: string[];
    ageList?: string[];
}){
    return async(dispatch:IRootThunkDispatch) => {
        try{
            dispatch(loadingFilRoommateAction());
            const reply = (await callAPI('POST', '/findRoommate/search', query)) as any;
            
            
            dispatch(displayFilRoommateSuccessAction(reply.filterRoommateData))
        }catch(err){
            dispatch(displayFilRoommateFailAction('failed to fetch roommate data' + err.toString()))
        }
    }
}