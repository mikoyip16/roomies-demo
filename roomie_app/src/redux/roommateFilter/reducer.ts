import { RoommateFilterAction } from './action';
import { RoommateFilterState } from './state';

const initialState: RoommateFilterState = {
    hasQuery: false,
    isDisplayed: false,
    isLoading: false,
};

export const roommateFilterReducer = (
    state: RoommateFilterState = initialState,
    action: RoommateFilterAction
): RoommateFilterState => {
    switch (action.type) {
        case '@@roommate/set_query': {
            return {
                ...state,
                hasQuery: true,
                isDisplayed: false,
                isLoading: true,
                query: {
                    ...state.query,
                    ...action.query,
                },
            };
        }
        case '@@roommate/filter_success': {
            let payload = action.data;
            return {
                ...state,
                hasQuery: true,
                isDisplayed: true,
                isLoading: false,
                payload
            };
        }
        case '@@roommate/filter_loading': {
            return {
                ...state,
                hasQuery: true,
                isDisplayed: false,
                isLoading: true,
            }
        }
        case '@@roommate/filter_fail': {
            return{
                ...state,
                hasQuery: true,
                isDisplayed: false,
                isLoading: true,
                errorMessage: 'Failed to display roommates',
            }
        }
        case '@@roommate/filter_clearQuery':{
            let payload = undefined
            return{
                hasQuery: false,
                isDisplayed: false,
                isLoading: false,
                payload,
            }
        }
        case '@@roommate/filter_setSearchText': {
            let searchText = action.searchText
            return{
                hasQuery: false,
                isDisplayed: false,
                isLoading: false,
                payload: undefined,
                searchText: searchText
            }
        }
        default:
            return state;
    }
};
