import { RoommatePageData } from "../types";

export type RoommateInfoState = {
    isDisplayed:boolean
    isLoading: boolean
    errorMessage?: string
    payload?: RoommatePageData
    loginUserInfo?:RoommatePageData
}