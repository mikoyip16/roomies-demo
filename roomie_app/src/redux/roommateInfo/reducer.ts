import { RoommateInfoState } from "./state";
import { RoommateInfoAction } from './action';

const initialState: RoommateInfoState = {
    isDisplayed: false,
    isLoading: true,
}

export const roommateInfoReducer = (
    state: RoommateInfoState = initialState,
    action: RoommateInfoAction
): RoommateInfoState => {
    switch (action.type) {
        case '@@roommate/get_user_byID': {
            let payload = action.data
            return {
                ...state,
                isDisplayed: true,
                isLoading: false,
                payload:payload
            };
        }
        case '@@roommate/get_loginUser_Info': {
            let loginUserInfo = action.data
            return {
                ...state,
                isDisplayed: true,
                isLoading: false,
                loginUserInfo:loginUserInfo
            };
        }

        default:
            return state;
    }
};
