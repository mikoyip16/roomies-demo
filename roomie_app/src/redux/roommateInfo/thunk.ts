import { callAPI } from "../../helpers/api";
import { IRootThunkDispatch } from "../store";
import { RoommatePageData } from "../types";
import { getLoginUserInfoAction, getUserbyIDAction } from "./action";

export function getUserbyIDThunk(userID: string){
    return async (dispatch: IRootThunkDispatch) => {
        try{
            const reply = await callAPI<null, RoommatePageData >(
                'GET',
                `/user/search/${userID}`
            ) 
            console.log(reply)
            dispatch(getUserbyIDAction(reply))
        }catch(err){

        }
    }
}

export function getLoginUserInfoThunk(userID: string){
    return async (dispatch: IRootThunkDispatch) => {
        try{
            const reply = await callAPI<null, RoommatePageData >(
                'GET',
                `/user/search/${userID}`
            ) 
            console.log(reply)
            dispatch(getLoginUserInfoAction(reply))
        }catch(err){

        }
    }
}