import { RoommatePageData } from "../types";


export function getUserbyIDAction(data: RoommatePageData){
    
    return{
        type: '@@roommate/get_user_byID' as const,
        data
    }
}

export function getLoginUserInfoAction(data: RoommatePageData){
    
    return{
        type: '@@roommate/get_loginUser_Info' as const,
        data
    }
}


export type RoommateInfoAction =
| ReturnType<typeof getUserbyIDAction>
| ReturnType<typeof getLoginUserInfoAction>