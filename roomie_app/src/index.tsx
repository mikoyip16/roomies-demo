import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { Provider, useSelector } from 'react-redux';
import store from './redux/store';
import { ConnectedRouter } from 'connected-react-router';
import { history } from './redux/store'
import { AuthProvider } from './redux/auth/provider';
import { SitePageProvider } from './redux/sitePage/provider';
import * as serviceWorker from './components/pages/serviceWorker';
import { subscribeUser } from './components/pages/subscription';
import { RoommatePageProvider } from './redux/roommatePage/provider';

ReactDOM.render(
    <React.StrictMode>
        <Provider store={store}>
            <ConnectedRouter history={history}>
                <AuthProvider />
                <SitePageProvider />
                <RoommatePageProvider />


                <App />
            </ConnectedRouter>
        </Provider>
    </React.StrictMode>,
    document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
async function getPublicKey() {
    let result = await fetch(`${process.env.REACT_APP_API_SERVER}/subscribe_public_key`)
    let json = await result.json()
    return json.public_key
}
async function subscribe() {
    let publicKey = await getPublicKey();
    serviceWorker.register();
    console.log("Public Key", publicKey)
    await subscribeUser(publicKey)
    reportWebVitals();

}
subscribe()