## Install packages under roomie_app folder and roomie_app_server folder
```Bash
yarn install
```

## Setup .env file according to .env.sample

## Database set up

1. create database
2. create tables

```Bash
yarn knex migrate:latest
```

3. run seed files 

```Bash
yarn knex seed:run
```
## Run roomie_app folder and roomie_app_server folder

```Bash
yarn start
```