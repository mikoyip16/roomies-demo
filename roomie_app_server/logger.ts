import winston from 'winston';

const logFormat = winston.format.printf(function (info: { level: any; message: any }) {
    let date = new Date().toISOString();
    return `${date} [${info.level}]: ${info.message}`;
});

export const logger = winston.createLogger({
    level: 'debug', // info, error, warnd
    format: winston.format.combine(winston.format.colorize(), logFormat),
    transports: [new winston.transports.Console()],
});
