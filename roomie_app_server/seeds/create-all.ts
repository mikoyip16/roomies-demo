import { Knex } from 'knex';
import tables from '../table';
import csv from 'csvtojson/v2';


const csvUserPath = __dirname + '/data/users_v2.csv';
const csvSitePath = __dirname + '/data/site_v2.csv';
const csvSiteImagePath = __dirname + '/data/site_image_v2.csv';
const csvSiteAttendeePath = __dirname + '/data/site_attendee.csv';
const csvSiteMeetingPath = __dirname + '/data/site_meeting_v2.csv';
const csvSiteMessagePath = __dirname + '/data/site_message.csv';
const csvSocialEventPath = __dirname + '/data/social_event.csv';
const userTableName = tables.USERS;
const siteTableName = tables.SITE;
const siteImageTableName = tables.SITE_IMAGE;
const siteAttendeeTableName = tables.SITE_ATTENDEE;
const siteMeetingTableName = tables.SITE_MEETING;
const siteMessageTableName = tables.SITE_MESSAGE;
const socialEventTableName = tables.SOCIAL_EVENT;
const eventImageTableName = tables.EVENT_IMAGE;


export async function seed(knex: Knex): Promise<void> {
    await knex(siteMessageTableName).del();

    await knex(siteMeetingTableName).del();

    await knex(siteAttendeeTableName).del();

    await knex(siteImageTableName).del();

    await knex(siteTableName).del();

    await knex(userTableName).del();

    await knex(socialEventTableName).del();
    console.log('[data _dirname]' + ' ' + __dirname);
    const jsonUserArray = await csv().fromFile(csvUserPath);

    const newJsonUserArray = jsonUserArray.map((obj) => {
        Object.keys(obj).forEach((key) => {
            if (obj[key] == '') obj[key] = null;
        });
        return obj;
    });

    await knex(userTableName).insert(newJsonUserArray);
    console.log('seed user table success');

    const jsonSiteArray = await csv().fromFile(csvSitePath);

    const newJsonSiteArray = jsonSiteArray.map((obj) => {
        Object.keys(obj).forEach((key) => {
            if (obj[key] == '') obj[key] = null;
        });
        return obj;
    });
    await knex(siteTableName).insert(newJsonSiteArray);
    console.log('seed site table success');

    const jsonSiteImageArray = await csv().fromFile(csvSiteImagePath);
    const newJsonSiteImageArray = jsonSiteImageArray.map((obj) => {
        Object.keys(obj).forEach((key) => {
            if (obj[key] == '') obj[key] = null;
        });
        return obj;
    });
    await knex(siteImageTableName).insert(newJsonSiteImageArray);
    console.log('seed site image table success');

    const jsonSiteAttendeeArray = await csv().fromFile(csvSiteAttendeePath);
    const newJsonSiteAttendeeArray = jsonSiteAttendeeArray.map((obj) => {
        Object.keys(obj).forEach((key) => {
            if (obj[key] == '') obj[key] = null;
        });
        return obj;
    });
    await knex(siteAttendeeTableName).insert(newJsonSiteAttendeeArray);
    console.log('seed site attendee table success');

    const jsonSiteMeetingArray = await csv().fromFile(csvSiteMeetingPath);
    const newJsonSiteMeetingArray = jsonSiteMeetingArray.map((obj) => {
        Object.keys(obj).forEach((key) => {
            if (obj[key] == '') obj[key] = null;
        });
        return obj;
    });
    await knex(siteMeetingTableName).insert(newJsonSiteMeetingArray);
    console.log('seed site meeting table success');

    const jsonSiteMessageArray = await csv().fromFile(csvSiteMessagePath);
    const newJsonSiteMessageArray = jsonSiteMessageArray.map((obj) => {
        Object.keys(obj).forEach((key) => {
            if (obj[key] == '') obj[key] = null;
        });
        return obj;
    });
    await knex(siteMessageTableName).insert(newJsonSiteMessageArray);
    console.log('seed site message table success');

    const jsonSocialEventArray = await csv().fromFile(csvSocialEventPath);
    const newJsonSocialEventArray = jsonSocialEventArray.map((obj) => {
        Object.keys(obj).forEach((key) => {
            if (obj[key] == '') obj[key] = null;
        });
        return obj;
    });
    await knex(socialEventTableName).insert(newJsonSocialEventArray);
    console.log('seed social event table success');

    const jsonEventImageArray = await csv().fromFile(csvEventImagePath);
    const newJsonEventImageArray = jsonEventImageArray.map((obj) => {
        Object.keys(obj).forEach((key) => {
            if (obj[key] == '') obj[key] = null;
        });
        return obj;
    });
    await knex(eventImageTableName).insert(newJsonEventImageArray);
    console.log('seed event Image table success');
}
