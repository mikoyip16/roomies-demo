export type JWTPayload = {
    user_id: number,
    nickname: string,
    username: string,
    image: string
}

export type NativeLoginInput = {
    username: string
    password: string
}
export type NativeLoginOutput = {
    jwt_token: string
}

export type SignupInput = NativeLoginInput
export type SignupOutput = NativeLoginOutput

export type FacebookLoginInput = {
    accessToken: string
}
export type FacebookLoginOutput = NativeLoginOutput