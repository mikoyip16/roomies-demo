import webpush from 'web-push'
import dotenv from 'dotenv'


dotenv.config(); 

const publicVapidKey = process.env.WEB_PUSH_PUBLIC_KEY;
const privateVapidKey = process.env.WEB_PUSH_PRIVATE_KEY;

export const init = () => {
    webpush.setVapidDetails('mailto:cheunggaga@gmail.com', publicVapidKey!, privateVapidKey!)
};

export const sendNotification = (subscription: webpush.PushSubscription, payload: string) => { 
    //pass the object into sendNotification fucntion and catch any error
    webpush.sendNotification(subscription, payload).catch(err => console.error(err));
}
