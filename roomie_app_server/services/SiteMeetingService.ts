import { Knex } from "knex";

class ApiResponse {
    result: boolean;
    msg: string;
    payload: any;
    constructor() {
        this.result = true;
        this.msg = '';
        this.payload = {};
    }
}


export class SiteMeetingService {
    constructor(private knex: Knex) { }

    async getsiteMeetings(siteID: string) {
        const response = new ApiResponse();
        try {
            let siteMeetings = (await this.knex.raw(`select site.rooms, site_meeting.id, site_id, availability, date, address from site_meeting inner join site on site.id = site_meeting.site_id where site_id = ? order by site_meeting.id desc;`, [siteID])).rows;
            response.payload.siteMeetings = siteMeetings;
            return response;
        } catch (e) {
            console.log(e)
            response.result = false;
            response.msg = `[${e.code}] ${e.detail}`;
            return response;
        }
    }

    async randomSites(siteID: string) {
        const response = new ApiResponse();
        try {
            let siteMeetings = (await this.knex.raw(`SELECT * FROM site LEFT OUTER JOIN site_images ON site.id = site_images.cover_image LEFT OUTER JOIN site_meeting ON site.id = site_meeting.site_id WHERE site.id = ?`, [siteID])).rows;
            response.payload.siteMeetings = siteMeetings;
            return response;
        } catch (e) {
            console.log(e)
            response.result = false;
            response.msg = `[${e.code}] ${e.detail}`;
            return response;
        }
    }

}