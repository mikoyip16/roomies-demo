import { Knex } from "knex";

class ApiResponse {
    result: boolean;
    msg: string;
    payload: any;
    constructor() {
        this.result = true;
        this.msg = '';
        this.payload = {};
    }
}

export const MessageMode = {
    SiteVistor: 'vistor',
    MessageOwner: 'owner',
    SiteOwner: 'siteOwner'
}

export class EventMessageService {
    constructor(private knex: Knex) { }

    async getMessages(eventID: string, mode: string) {
        const response = new ApiResponse();
        try {
            let messages = (await this.knex.raw(`select 
            users.id as usersid, event_message.id, event_message.content, users.nickname, users.image, event_message.image as msg_img from event_message 
            inner join users on users.id = event_message.users_id 
            where event_message.event_id = ? order by event_message.id;`, [eventID])).rows;
            messages = messages.map((message: { usersid: any; }) => {
                let edit = false;
                let del = false;
                if (mode == MessageMode.MessageOwner) {
                    edit = true;
                    del = true;
                } if (mode == MessageMode.SiteOwner) { 
                    del = true;
                }
                return {
                    ...message, 
                    edit, 
                    del
                }
            })
            response.payload = { messages };
            return response;
        } catch (e) {
            console.log(e)
            response.result = false;
            response.msg = `[${e.code}] ${e.detail}`;
            return response;
        }
    }

    async creatMessage(userID: string, eventID: string, content: string, image: string | null,) {
        const response = new ApiResponse();
        try {
            // console.log(req.session?.['user'])
            await this.knex.raw(`insert into event_message (users_id, event_id, content, image, created_at, updated_at) values (?, ?, ?, ?, ?, ?)`, [userID, eventID, content, image, "now()", "now()"])
            return response;
        } catch (e) {
            console.log(e)
            response.result = false;
            response.msg = `[${e.code}] ${e.detail}`;
            return response;

        }
    }

 

    async deleteMessage(messageID: string, userID: string) {
        const response = new ApiResponse();
        try {
            
            let result = await this.knex.raw(`DELETE from event_message WHERE id = ? and users_id = ?`, [messageID, userID]);
            if (result.rowCount === 0) {
                response.result = false;
                response.msg = `Message not found/ userID not match message`;
            }
            return response;
        } catch (e) {
            console.log(e)
            response.result = false;
            response.msg = `[${e.code}] ${e.detail}`;
            return response;
        }
    }
}