import { Knex } from 'knex';
import { logger } from '../logger';

export class FindRoommateService {
    constructor(private knex: Knex) {}

    async displayAllRoommates() {
        try {
            const userData = await this.knex
                .from('users')
                .select(
                    'id',
                    'username',
                    'image',
                    'flower',
                    'egg',
                    'gender',
                    'nickname',
                    'age',
                    'introduction',
                    'education_level',
                    'ethnicity',
                    'occupation',
                    'language',
                    'preferred_location'
                ).orderBy('id');
            return userData;
        } catch (e) {
            console.error(e.message);
            return;
        }
    }

    async filterRoommates(
        ethnicity: string,
        religion: string,
        language: string,
        occupation: string,
        rentRange: number[],
        preferredLocation: string,
        gender: string,
        rentDate: Date,
        rentDuration: number,
        specialRequirementList: string[],
        ageList: string[]
    ) {
        try {
            const knex = this.knex;
            let findRoommateQuery = knex
                .from('users')
                .select(
                    'id',
                    'username',
                    'image',
                    'flower',
                    'egg',
                    'gender',
                    'nickname',
                    'age',
                    'introduction',
                    'education_level',
                    "ethnicity",
                    'occupation',
                    'language'
                );
            if (ethnicity) {
                logger.debug('filtering roommate ethnicity', ethnicity);
                findRoommateQuery = findRoommateQuery.andWhere('ethnicity', `${ethnicity}`);
            }
            if (religion) {
                logger.debug('filtering roommate religion', religion);
                findRoommateQuery = findRoommateQuery.andWhere('religion', `${religion}`);
            }
            if (language) {
                logger.debug('filtering roommate language', language);
                findRoommateQuery = findRoommateQuery.andWhere('language', `${language}`);
            }
            if (occupation) {
                logger.debug('filtering roommate occupation', occupation);
                findRoommateQuery = findRoommateQuery.andWhere('occupation', `${occupation}`);
            }
           
            if (preferredLocation) {
                logger.debug('filtering roommate preferredLocation', preferredLocation);
                findRoommateQuery = findRoommateQuery.andWhere(
                    'preferred_location',
                    `${preferredLocation}`
                );
            }
            if (gender) {
                logger.debug('filtering roommate gender', gender);
                findRoommateQuery = findRoommateQuery.andWhere('gender', `${gender}`);
            }
            if (rentDate) {
                logger.debug('filtering roommate rentDate', rentDate);
                findRoommateQuery = findRoommateQuery.andWhere('rent_date', '>=', `${rentDate}`);
            }
            if (rentDuration) {
                logger.debug('filtering roommate rentDuration', rentDuration);
                findRoommateQuery = findRoommateQuery.andWhere(
                    'rent_duration',
                    '=',
                    `${rentDuration}`
                );
            }
            if (specialRequirementList) {
                logger.debug('filtering roommate specialRequirementList', specialRequirementList);
                specialRequirementList.forEach((specialRequire) => {
                    if (specialRequire == '無寵物') {
                        findRoommateQuery = findRoommateQuery.andWhere(
                            'have_no_pet_text',
                            '無寵物'
                        );
                    }
                    if (specialRequire == '無孩子') {
                        findRoommateQuery = findRoommateQuery.andWhere(
                            'have_no_kids_text',
                            '無孩子'
                        );
                    }
                    if (specialRequire == '不吸煙') {
                        findRoommateQuery = findRoommateQuery.andWhere('not_smoking_text', '不吸煙');
                    }
                });
            }
            if (ageList) {
                logger.debug('filtering roommate ageList', ageList);
                ageList.forEach((age) => {
                    findRoommateQuery = findRoommateQuery.andWhere('age', `${age}`);
                });
            }
            const result = await findRoommateQuery;
            return result;
        } catch (e) {
            console.error(e.message);
            return;
        }
    }
}
