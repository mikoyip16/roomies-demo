import { Knex } from 'knex';
import { logger } from '../logger';

type Range = {
    min?: number;
    max?: number;
};

export class SitePageService {
    constructor(private knex: Knex) {}

    async displayAllSites() {
        try {
            const data = (
                await this.knex
                    .raw(`SELECT s.id, image, title, rooms, usable_area, floor, years, district, address, rent, owner_id, s.updated_at, i.updated_at, m.updated_at, date FROM site as s inner JOIN
            site_images as i ON s.id = i.cover_image inner join site_meeting as m ON s.id = m.site_id where m.availability = true AND s.id <= 85;`)
            ).rows;
            const meetingAtDescending = (
                await this.knex
                    .raw(`SELECT s.id, image, title, rooms, usable_area, floor, years, district, address, rent, owner_id, s.updated_at, date FROM site as s inner JOIN
            site_images as i ON s.id = i.cover_image inner join site_meeting as m ON s.id = m.site_id where m.availability = true ORDER BY date DESC;`)
            ).rows;
            const meetingAtAscending = (
                await this.knex
                    .raw(`SELECT s.id, image, title, rooms, usable_area, floor, years, district, address, rent, owner_id, s.updated_at, date FROM site as s inner JOIN
            site_images as i ON s.id = i.cover_image inner join site_meeting as m ON s.id = m.site_id where m.availability = true ORDER BY date;`)
            ).rows;
            const usableAreaDescending = (
                await this.knex
                    .raw(`SELECT s.id, image, title, rooms, usable_area, floor, years, district, address, rent, owner_id, s.updated_at, date FROM site as s inner JOIN
            site_images as i ON s.id = i.cover_image inner join site_meeting as m ON s.id = m.site_id where m.availability = true ORDER BY usable_area DESC;`)
            ).rows;
            const usableAreaAscending = (
                await this.knex
                    .raw(`SELECT s.id, image, title, rooms, usable_area, floor, years, district, address, rent, owner_id, s.updated_at, date FROM site as s inner JOIN
            site_images as i ON s.id = i.cover_image inner join site_meeting as m ON s.id = m.site_id where m.availability = true ORDER BY usable_area;`)
            ).rows;
            const rentDescending = (
                await this.knex
                    .raw(`SELECT s.id, image, title, rooms, usable_area, floor, years, district, address, rent, owner_id, s.updated_at, date FROM site as s inner JOIN
            site_images as i ON s.id = i.cover_image inner join site_meeting as m ON s.id = m.site_id where m.availability = true ORDER BY rent DESC;`)
            ).rows;
            const rentAscending = (
                await this.knex
                    .raw(`SELECT s.id, image, title, rooms, usable_area, floor, years, district, address, rent, owner_id, s.updated_at, date FROM site as s inner JOIN
            site_images as i ON s.id = i.cover_image inner join site_meeting as m ON s.id = m.site_id where m.availability = true ORDER BY rent;`)
            ).rows;
            const EventData = (
                await this.knex
                    .raw(`SELECT s.id, description, image, title, rooms, usable_area, floor, years, district, address, rent, owner_id, s.updated_at, date FROM site as s inner JOIN
            site_images as i ON s.id = i.cover_image inner join site_meeting as m ON s.id = m.site_id where m.availability = true and s.id > 85 `)
            ).rows;
            const result = {
                data,
                meetingAtDescending,
                meetingAtAscending,
                usableAreaDescending,
                usableAreaAscending,
                rentDescending,
                rentAscending,
                EventData,
            };
            // console.log(result)
            return result;
        } catch (e) {
            console.error(e.message);
            return;
        }
    }

    async searchSites(address: string) {
        try {
            const searchAddress = address;
            logger.debug(searchAddress);
            const result = (
                await this.knex.raw(
                    `SELECT s.id, image, title, rooms, usable_area, floor, years, district, address, rent, owner_id, s.updated_at, date FROM site as s inner JOIN
            site_images as i ON s.id = i.cover_image inner join site_meeting as m ON s.id = m.site_id where m.availability = true and address LIKE '${searchAddress}%';`
                )
            ).rows;
            logger.debug(result);
            return result;
        } catch (e) {
            console.error(e.message);
            return;
        }
    }

    async filterSites(
        rangeList: Range[],
        rentRange: number[],
        area: string,
        compartment: string,
        requirementList: string[],
        specialList: string[]
    ) {
        try {
            let requirementQuery = this.knex.from('site').select('id');

            if (requirementList) {
                requirementList.forEach((requirement) => {
                    if (requirement == '洗衣機') {
                        console.log('洗衣機');
                        requirementQuery = requirementQuery.andWhere('washing_machine', true);
                    } else if (requirement == '電視') {
                        requirementQuery = requirementQuery.andWhere('television', true);
                    } else if (requirement == '冷氣') {
                        requirementQuery = requirementQuery.andWhere('air_con', true);
                    } else if (requirement == '熱水爐') {
                        requirementQuery = requirementQuery.andWhere('water_heater', true);
                    } else if (requirement == '微波爐') {
                        requirementQuery = requirementQuery.andWhere('microwave', true);
                    } else if (requirement == '雪櫃') {
                        requirementQuery = requirementQuery.andWhere('fridge', true);
                    } else if (requirement == '床') {
                        requirementQuery = requirementQuery.andWhere('bed', true);
                    }
                });
            }
            if (specialList) {
                specialList.forEach((special) => {
                    console.log(special);
                    if (special == '複式') {
                        requirementQuery = requirementQuery.andWhere('duplex', 'TRUE');
                    }
                    if (special == '天台') {
                        requirementQuery = requirementQuery.andWhere('top_roof', 'TRUE');
                    }
                    if (special == '頂樓') {
                        requirementQuery = requirementQuery.andWhere('upper_floor', 'TRUE');
                    }
                    if (special == '平台') {
                        requirementQuery = requirementQuery.andWhere('balcony', 'TRUE');
                    }
                    if (special == '花園') {
                        requirementQuery = requirementQuery.andWhere('garden', 'TRUE');
                    }
                });
            }
            
            if (rentRange) {
                
                requirementQuery = requirementQuery.andWhereBetween('rent', [
                    rentRange[0],
                    rentRange[1],
                ]);
            }
            if (area) {
               
                requirementQuery = requirementQuery.andWhere('district', `${area}`);
            }
           
            if (compartment) {
                
                if (compartment === '開放式') {
                    requirementQuery = requirementQuery.andWhere('rooms', '=', 0);
                } else if (compartment === '1房') {
                    requirementQuery = requirementQuery.andWhere('rooms', '=', 1);
                } else if (compartment === '2房') {
                    requirementQuery = requirementQuery.andWhere('rooms', '=', 2);
                } else if (compartment === '3房') {
                    requirementQuery = requirementQuery.andWhere('rooms', '=', 3);
                } else if (compartment === '4房') {
                    requirementQuery = requirementQuery.andWhere('rooms', '=', 4);
                } else if (compartment === '4房以上') {
                    requirementQuery = requirementQuery.andWhere('rooms', '>=', 4);
                }
            }
            let sizeQuery = this.knex.from('site').select('id');

            if (rangeList) {
                rangeList.forEach(({ min, max }) => {
                    if (min && max) {
                        sizeQuery = sizeQuery.orWhereBetween('usable_area', [min, max]);
                    } else if (min) {
                        sizeQuery = sizeQuery.orWhere('usable_area', '>=', min);
                    } else if (max) {
                        sizeQuery = sizeQuery.orWhere('usable_area', '<=', max);
                    }
                });
            }

            const resultQuery = this.knex('site')
                .select(
                    'site.id',
                    'site_images.image',
                    'site.title',
                    'site.rooms',
                    'site.usable_area',
                    'site.floor',
                    'site.years',
                    'site.district',
                    'site.address',
                    'site.rent',
                    'site.owner_id',
                    'site_meeting.date',
                    'site.updated_at'
                   
                )
                .whereIn('site.id', this.knex.intersect(sizeQuery, requirementQuery))
                .innerJoin('site_images', 'site.id', '=', 'site_images.cover_image')
                .innerJoin('site_meeting', 'site.id', '=', 'site_meeting.site_id');

            const result = await resultQuery;
            return result;
        } catch (e) {
            console.error(e.message);
            return;
        }
    }
}
