import { Knex } from "knex";
import { ApiResponse, apiResponseInit } from "../model";



export class SiteDetailService {
    constructor(private knex: Knex) { }

    async getSiteDetail(siteID: string) {
        let apiResponse: ApiResponse = { ...apiResponseInit }
        try {
    
            let siteInfo = (await this.knex.raw(`select owner_id, title, description, map_x, map_y, users.image,
            site_meeting.id as meeting_id ,site_meeting.date as meeting_date, address, users.nickname, rent, rooms, gross_area,
            usable_area, years, view, television, air_con, washing_machine, water_heater, microwave, fridge, bed
                        from site
                        inner join users on users.id = site.owner_id
                        JOIN (
                            SELECT DISTINCT ON (site_id) *
                            FROM site_meeting
                            ORDER BY site_id, date DESC
                         ) site_meeting on site.id = site_meeting.site_id
                        where site.id = ?`, [siteID])).rows;
            if (siteInfo.length == 0) {
                apiResponse.isSuccessful = false;
                apiResponse.msg = "Site ID not exist";
                return apiResponse;
            }
            apiResponse.payload.siteInfo = siteInfo;
            const siteImage = (await this.knex.raw(`select * from site_images where site_id = ?`, [siteID])).rows;
            apiResponse.payload.siteImage = siteImage;
            return apiResponse
        } catch (e) {
            console.log(e)
            apiResponse.isSuccessful = false;
            apiResponse.msg = `[${e.code}] ${e.detail}`;
            return apiResponse
        }
    }

}