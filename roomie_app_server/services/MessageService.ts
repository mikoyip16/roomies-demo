import { Knex } from "knex";

class ApiResponse {
    result: boolean;
    msg: string;
    payload: any;
    constructor() {
        this.result = true;
        this.msg = '';
        this.payload = {};
    }
}

export const MessageMode = {
    SiteVistor: 'vistor',
    MessageOwner: 'owner',
    SiteOwner: 'siteOwner'
}

export class MessageService {
    constructor(private knex: Knex) { }

    async getMessages(siteMeetingID: string, mode: string) {
        const response = new ApiResponse();
        try {
            let messages = (await this.knex.raw(`select 
            users.id as usersid, site_message.id, site_message.content, users.nickname, users.image, site_message.image as msg_img from site_message 
            inner join users on users.id = site_message.users_id 
            where site_message.meeting_id = ? order by site_message.id`, [siteMeetingID])).rows;
            messages = messages.map((message: { usersid: any; }) => {
                let edit = false;
                let del = false;
                if (mode == MessageMode.MessageOwner) {
                    edit = true;
                    del = true;
                } if (mode == MessageMode.SiteOwner) { 
                    del = true;
                }
                return {
                    ...message, 
                    edit, 
                    del
                }
            })
            response.payload = { messages };
            return response;
        } catch (e) {
            console.log(e)
            response.result = false;
            response.msg = `[${e.code}] ${e.detail}`;
            return response;
        }
    }

    async creatMessage(userID: string, siteMeetingID: string, content: string, image: string | null,) {
        const response = new ApiResponse();
        try {
            
            await this.knex.raw(`insert into site_message (users_id, meeting_id, content, image, created_at, updated_at) values (?, ?, ?, ?, ?, ?)`, [userID, siteMeetingID, content, image, "now()", "now()"])
            return response;
        } catch (e) {
            console.log(e)
            response.result = false;
            response.msg = `[${e.code}] ${e.detail}`;
            return response;

        }
    }

   

    async deleteMessage(messageID: string) {
        const response = new ApiResponse();
        try {
            let result = await this.knex.raw(`DELETE from site_message WHERE id = ? `, [messageID]);
            if (result.rowCount === 0) { 
                response.result = false;
                response.msg = `Message not found`;
            }
            return response;
        } catch (e) {
            console.log(e)
            response.result = false;
            response.msg = `[${e.code}] ${e.detail}`;
            return response;
        }
    }
}