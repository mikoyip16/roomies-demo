import { Telegraf, Telegram } from 'telegraf' 
import jwtSimple from 'jwt-simple'
import { UserService } from './UserService';

export class TelegramBotService {
    bot1: Telegraf; 
    bot2: Telegram; 
    jwtSecret: string;
    constructor(telegramBotToken: string, jwtSecret: string, private userService: UserService) {
        this.bot1 = new Telegraf(telegramBotToken!);
        this.bot2 = new Telegram(telegramBotToken);
        this.jwtSecret = jwtSecret;
        process.once('SIGINT', () => this.bot1.stop('SIGINT'))
        process.once('SIGTERM', () => this.bot1.stop('SIGTERM'))
    }

    telegramRegistrationServiceInit() {
        this.bot1.start(async ctx => { 
            console.log('received start:', {
                startPayload: ctx.startPayload,
                message: ctx.update.message,
            })
            let user_telegram_id = ctx.update.message.from.id 
            let nickname = ``
            if (ctx.update.message.from.first_name) {
                nickname += ctx.update.message.from.first_name
                if (ctx.update.message.from.last_name) {
                    nickname += ctx.update.message.from.last_name
                }
            }
            const username = `${user_telegram_id} ${nickname}`
            const telegramUserName = ctx.update.message.from.username;

            let app_name = 'Roomies'
            let id;
            let jwt_payload = {};
            const result = await this.userService.checkTelegramIdExist(user_telegram_id); 
            if (!result.isSuccessful) {
                ctx.reply('failed to check user, try again later')
                return
            }
            if (result.msg === 'User Not Exist') { 
                console.log("Tg Registration")
                id = (await this.userService.createUserByTG(username, Math.random().toString(), "", nickname, user_telegram_id, telegramUserName!)).payload.rows[0].id//怕改createUser會令其他地方爆，所以另開一個
                console.log("Roomies User ID: " + id);
                jwt_payload = {
                    userID: id,
                    username: username,
                    nickname: nickname,
                    image: 'testingBackground.jpg'
                }
                let jwt_token = jwtSimple.encode(jwt_payload, this.jwtSecret!) 

                ctx.reply(
                    `Welcome ${nickname} 
            Click below link to confirm login app "${app_name}" with user "${nickname}":
            ${process.env.REACT_APP_DOMAIN}/?jwt_token=${jwt_token}&target=signUp`,
                )

            } else {
                let payload = result.payload
                console.log("Tg login")
                jwt_payload = {
                    userID: payload.user.id,
                    username: payload.user.username,
                    nickname: payload.user.nickname,
                    image: payload.user.image
                }
                let jwt_token = jwtSimple.encode(jwt_payload, this.jwtSecret!) 
                ctx.reply(
                    `Welcome ${nickname} 
            Click below link to confirm login app "${app_name}" with user "${nickname}":
            ${process.env.REACT_APP_DOMAIN}/?jwt_token=${jwt_token}&target=home`,
                )
            }
        })
        this.bot1.launch();
        console.log('telegram bot is ready')
    }


    telegramSendMessage(message: string, telegramID: number) {
        this.bot2.sendMessage(telegramID, message);
    }

}