import { Knex } from "knex";

class ApiResponse {
    result: boolean;
    msg: string;
    payload: any;
    constructor() {
        this.result = true;
        this.msg = '';
        this.payload = {};
    }
}

export class EventAttendeeService {
    constructor(private knex: Knex) { }

    async getAttendees(eventID: string, userID: string) {
        const response = new ApiResponse();
        let onAttendList = false;
        let host = false;
        try {
            let attendees = (await this.knex.raw(` 
    select sa.id, sa.users_id, sa.attended ,u.id as usersid, u.nickname, u.image
    from event_attendee as sa
    inner join users as u on u.id = sa.users_id 
    inner join social_event as se on se.id = sa.event_id
    where se.id = ?`, [eventID])).rows;
            let date = (await this.knex.raw(`
    select date,admin_id
    from social_event 
    where id = ?`, [eventID])).rows;
            const eventDate = new Date(date[0].date); 
            const now = new Date();
            let pastEvent = false;

            
            if (now > eventDate) {
                pastEvent = true;
            } else {
                pastEvent = false;
            }
            
            for (const attendee of attendees) {
                if (attendee.usersid == userID) {
                    onAttendList = true;
                }
            }
          
            if (parseInt(userID) == date[0].admin_id) {
                host = true;
            }
           
            attendees = attendees.map((attendee: { id: any; attended: any; usersid: any; nickname: any; image: any; }) => {
                return {
                    id: attendee.id,
                    attended: attendee.attended,
                    pastEvent,
                    usersID: attendee.usersid, 
                    nickname: attendee.nickname,
                    image: attendee.image
                }
            })
            console.log(attendees)
            response.payload = { attendees, onAttendList, host }
            return response;
        } catch (e) {
            console.log(e)
            response.result = false;
            response.msg = `[${e.code}] ${e.detail}`;
            return response;
        }
    }

    async addAttendList(userID: string, eventID: string) {
        const response = new ApiResponse();
        try {
            
            let result = (await this.knex.raw(`SELECT * from event_attendee WHERE users_id = ? and event_id = ?;`, [userID, eventID])).rows;
            if (result.length === 0) {
                await this.knex.raw(`INSERT INTO event_attendee (users_id, event_id, attended) values (?, ?, ?)`, [userID, eventID, false]);
            } else {
                response.result = false;
                response.msg = 'duplicated record';
            }
            return response;
        } catch (e) {
            console.log(e)
            response.result = false;
            response.msg = `[${e.code}] ${e.detail}`;
            return response;

        }
    }

    async toggleAttendStatus(attendeeID: string, userID: string) {
        const response = new ApiResponse();
        try {
            const result = await this.knex.raw(`UPDATE event_attendee SET attended = NOT attended WHERE id = ? and users_id = ?;`, [attendeeID, userID]);
            if (result.rowCount == 1) {
                response.msg = 'Success';
            } else {
                response.result = false;
                response.msg = 'AttendeeID not exist/ User ID not match AttendeeID';
            }
            return response;
        } catch (e) {
            console.log(e)
            response.result = false;
            response.msg = `[${e.code}] ${e.detail}`;
            return response;
        }
    }

}