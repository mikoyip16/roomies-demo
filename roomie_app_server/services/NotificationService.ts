import { Knex } from 'knex';
import { ApiResponse as apiResponse, apiResponseInit } from '../model';

export const NOTIFICATION_TYPE_LEAVE_MESSAGE = 'Leave Message';
export const NOTIFICATION_TYPE_ATTEND_SITE = 'Site Attend';
export const NOTIFICATION_TYPE_ATTEND_EVENT = 'Event Attend';

export const LEAVE_MESSAGE_STRING = '留言比你：';
export const ATTEND_SITE_STRING = '參加左你個睇樓團：';
export const ATTEND_EVENT_STRING = '參加左你個活動：';

export class NotificationService {
    constructor(private knex: Knex) { }
   

    async createNotification(userID: string, message: string, type: string, targetURL: string, senderID: string) {
        const response: apiResponse = apiResponseInit;
        try {
            let result = (
                await this.knex.raw(`INSERT INTO notification (receiver_id, message, type, target_url, sender_id) values (?, ?, ?, ?, ?) returning id`, [
                    userID, message, type, targetURL, senderID
                ])
            ).rows;
            if (result.length == 0) {
                response.isSuccessful = false; 
                response.msg = 'Notifcation not created ';
            } else {
                response.isSuccessful = true;
                response.msg = 'Notifcation created '; 
                response.payload = { notification: result[0] };
            }
            return response;
        } catch (e) {
            console.log(e);
            response.isSuccessful = false; 
            response.msg = e
            return response;
        }
    }

    async getNotifications(userID: string) {
        const response: apiResponse = apiResponseInit;
        try {
            let notifications = (
                await this.knex.raw(`SELECT * from notification inner join users on users.id = notification.sender_id where receiver_id = ?`, [
                    userID
                ])
            ).rows;
            response.isSuccessful = true;
            response.msg = 'got Notifcations '; 
            response.payload = { notifications };
            return response;
        } catch (e) {
            response.isSuccessful = false;
            response.msg = e 
            return response;
        }
    }
}
