import mailjet from 'node-mailjet'

class ApiResponse {
    result: boolean;
    msg: string;
    constructor() {
      this.result = true;
      this.msg = '';
    }
  }

export class MailService {
    mailjet:mailjet.Email.Client;
    constructor(mailjet: mailjet.Email.Client){
        this.mailjet = mailjet;
    }

    async registrationConfirmEmail(receiverEmail: String, recieverName: String, confirmKey: String) {
        const response = new ApiResponse();
        try {
            this.mailjet.post("send", { 'version': 'v3.1' })
                .request({
                    "Messages": [{
                        "From": {
                            "Email": "1155045906@link.cuhk.edu.hk", 
                            "Name": "Roomies" 
                        },
                        "To": [{
                            "Email": receiverEmail, 
                            "Name": recieverName 
                        }],
                        "TemplateID": 2938417,
                        "TemplateLanguage": true,
                        "Variables": { 
                            "username": recieverName,
                            "url": 'http://localhost:8080/activated.html?confirmKey=' + confirmKey
                        }
                    }]
                })
                .then((result) => {
                    console.log(result.body)
                })
                .catch((err) => {
                    response.msg = "Mailjet Error";
                    response.result = false;
                    console.log(err)
                })
            return response;
        } catch (error) {
            response.msg = error.message;
            response.result = false;
            return response;
            
        }
    }

    async resetPwEmail(receiverEmail: String, recieverName: String, confirmKey: String) {
        const response = new ApiResponse();
        try {
            this.mailjet.post("send", { 'version': 'v3.1' })
                .request({
                    "Messages": [{
                        "From": {
                            "Email": "1155045906@link.cuhk.edu.hk", 
                            "Name": "Roomies"
                        },
                        "To": [{
                            "Email": receiverEmail, 
                            "Name": recieverName 
                        }],
                        "TemplateID": 2940357,
                        "TemplateLanguage": true,
                        "Variables": { 
                            "username": recieverName,
                            "resetPwLink": 'http://localhost:8080/resetPw.html?confirmKey=' + confirmKey
                        }
                    }]
                })
                .then((result) => {
                    console.log(result.body)
                })
                .catch((err) => {
                    response.msg = "Mailjet Error";
                    response.result = false;
                    console.log(err)
                })
            return response;
        } catch (error) {
            response.msg = error.message;
            response.result = false;
            return response;
            
        }
    }
}