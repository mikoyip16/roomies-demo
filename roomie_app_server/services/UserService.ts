import { Knex } from 'knex';
import { checkPassword, hashPassword } from '../hash';
import { MailService } from './MailService';
import { ApiResponse as apiResponse, apiResponseInit } from '../model';
import { logger } from '../logger';

class ApiResponse {
    result: boolean;
    payload: any;
    msg: any;
    constructor() {
        this.result = true;
        this.msg = '';
    }
}

export class UserService {
    constructor(private knex: Knex, private mailService: MailService) { }
   

    async loginNative(username: string, password: string) {
        const response = new ApiResponse();
        try {
            const users = (
                await this.knex.raw('SELECT * FROM users where username = ?', [username])
            ).rows;
            if (users.length == 0) {
                response.result = false;
                response.msg = 'invalid email/password';
                return response;
            }
            const foundUser = users[0];
            const validPassword = await checkPassword(password, foundUser.password);
            if (!validPassword) {
                response.result = false;
                response.msg = 'invalid email/password';
                return response;
            }
            if (foundUser.activated == false) {
                response.result = false;
                response.msg = 'not yet activate';
                return response;
            }

            response.msg = {
                userID: foundUser.id,
                nickname: foundUser.nickname,
                image: foundUser.image,
            };
            return response;
        } catch (e) {
            console.log(e);
            response.result = false;
            response.msg = `[${e.code}] ${e.detail}`;
            return response;
        }
    }

    async createUser(username: string, password: string, email: string, nickname: string) {
        const hashedPassword = await hashPassword(password);
        const confirmKey = await hashPassword(username);
        const response = new ApiResponse();
        try {
            let checkDuplicated = (
                await this.knex.raw(`SELECT * FROM users where username = ? `, [username])
            ).rows;
            if (checkDuplicated.length > 0) {
                response.result = false;
                response.msg = 'This username has been used';
                return response;
            }

            const lastUserID = (await this.knex.raw('SELECT MAX(id) FROM users')).rows[0].max;

            const newUserID = lastUserID + 1;
            const userID = (
                await this.knex.raw(
                    'INSERT INTO users (id, username, nickname, password, email, confirm_key, activated) values (?, ?, ?, ?, ?, ?, ?) returning id',
                    [newUserID, username, nickname, hashedPassword, email, confirmKey, true]
                )
            ).rows[0].id;

            
            if (userID) {
                response.result = true;
                response.msg = userID;
            } else {
                response.result = false;
                response.msg = 'fail to insert user';
            }
            return response;
        } catch (e) {
            console.log(e);
            response.result = false;
            response.msg = `[${e.code}] ${e.detail}`;
            return response;
        }
    }

    async updateUser(
        userID: string,
        info: {
            nickname?: string;
            ethnicity?: string;
            religion?: string;
            language?: string;
            occupation?: string;
            gender?: string;
            age?: string;
            not_smoking_text?: string;
            have_no_pet_text?: string;
            have_no_kids_text?: string;
            rent_duration?: string;
            rent_date?: string;
            introduction?: string;
            budget_min?: number;
            budget_max?: number;
            preferred_location?: string
        }
    ) {
        Object.keys(info).forEach((key) => {
            if (info[key].length === 0) delete info[key];
        });
        const id = await this.knex('users').update(info).where('id', '=', userID);
        console.log(id);
        return id;
    }

    async getUser(userID: string) {
        try {
            let userData = (
                await this.knex('users')
                    .select(
                        'id',
                        'username',
                        'image',
                        'flower',
                        'egg',
                        'gender',
                        'nickname',
                        'age',
                        'introduction',
                        'education_level',
                        'ethnicity',
                        'occupation',
                        'language',
                        'preferred_location',
                        'budget_min',
                        'budget_max',
                        'rent_date',
                        'rent_duration',
                        'preferred_gender',
                        'religion',
                        'not_smoking_text',
                        'have_no_kids_text',
                        'have_no_pet_text',
                        'telegram_id',
                        'telegram_username'
                    )
                    .where('id', '=', userID)
            )[0];

            let meetingData = await this.knex('site_attendee')
                .select('*')
                .where('users_id', '=', userID)
                .innerJoin('site_meeting', 'site_attendee.meeting_id', '=', 'site_meeting.id')
                .innerJoin('site', 'site_meeting.site_id', '=', 'site.id');
            let siteData = await this.knex('site')
                .innerJoin('site_images', 'site.id', '=', 'site_images.cover_image')
                .select('*')
                .where('owner_id', '=', userID);

            if (!userData.image || userData.image === 'undefined') {
                userData.image = 'testingBackground.jpg';
            }
            const result = { userData, meetingData, siteData };
            return result;
        } catch (e) {
            console.log(e);
            return;
        }
    }

    async uploadUserIcon(userID: number, image: string) {
        await this.knex('users').update({ image }).where('id', '=', userID);
        const result = (
            await this.knex('users').select('nickname', 'username').where('id', '=', userID)
        )[0];

        logger.debug(result);
        return result;
    }

    async activateUser(confirmKey: string) {
        const response = new ApiResponse();
        try {
            let result = await this.knex.raw(
                `UPDATE users set activated = true where confirm_key = ?`,
                [confirmKey]
            );
            if (result.rowCount == 1) {
                response.msg = 'activated';
            } else {
                response.result = false;
                response.msg = 'not a valid confirm key';
            }
            return response;
        } catch (e) {
            console.log(e);
            response.result = false;
            response.msg = `[${e.code}] ${e.detail}`;
            return response;
        }
    }

    async checkTelegramIdExist(telegramID: number) {
        const response: apiResponse = apiResponseInit;
        try {
            let result = (
                await this.knex.raw(`SELECT * FROM users where telegram_id = ?`, [telegramID])
            ).rows;
            if (result.length == 0) {
                response.isSuccessful = true; 
                response.msg = 'User Not Exist';
            } else {
                response.isSuccessful = true;
                response.msg = 'User Exist'; 
                response.payload = { user: result[0] };
            }
            return response;
        } catch (e) {
            console.log(e);
            response.isSuccessful = false; 
            response.msg = e; 
            return response;
        }
    }
    async forgotPw(emailAddress: string) {
        const response = new ApiResponse();
        const confirmKey = await hashPassword(emailAddress);
        try {
            let result = (
                await this.knex.raw(`SELECT * FROM users where email = ? order by id desc`, [
                    emailAddress,
                ])
            ).rows;
            if (result.length == 0) {
                response.result = false;
                response.msg = 'Wrong email';
            } else {
                const foundUser = result[0];
                response.result = true;
                await this.knex.raw(`update users set confirm_key = ? where email = ?`, [
                    confirmKey,
                    emailAddress,
                ]);
                const mailResult = await this.mailService.resetPwEmail(
                    emailAddress,
                    foundUser.username,
                    confirmKey
                );
                if (mailResult.result) response.result = true;
                else {
                    response.result = false;
                    response.msg = mailResult.msg;
                }
                return response;
            }
            return response;
        } catch (e) {
            console.log(e);
            response.result = false;
            response.msg = `[${e.code}] ${e.detail}`;
            return response;
        }
    }

    async resetPw(confirmKey: string, newPassword: string) {
        const response = new ApiResponse();
        try {
            let result = await this.knex.raw(
                `UPDATE users set password = ? where confirm_key = ?;`,
                [await hashPassword(newPassword), confirmKey]
            );
            if (result.rowCount == 1) {
                response.msg = 'reset pw successfully';
            } else {
                response.result = false;
                response.msg = 'not a valid confirm key';
            }
            return response;
        } catch (e) {
            console.log(e);
            response.result = false;
            response.msg = `[${e.code}] ${e.detail}`;
            return response;
        }
    }

    async createUserByTG(
        username: string,
        password: string,
        email: string,
        nickname: string,
        telegramID: number,
        telegramUsername: string
    ) {
        const hashedPassword = await hashPassword(password);
        const response: apiResponse = apiResponseInit;
        let id;
        try {
            let checkDuplicated = (
                await this.knex.raw(`SELECT * FROM users where username = ? `, [username])
            ).rows;
            if (checkDuplicated.length > 0) {
                response.isSuccessful = false;
                response.msg = 'This username has been used';
                return response;
            }
            const lastUserID = (await this.knex.raw('SELECT MAX(id) FROM users')).rows[0].max;
            const newUserID = lastUserID + 1;
            id = await this.knex.raw(
                'INSERT INTO users (id, username, password, email, nickname, activated, telegram_id, telegram_username,image) values (?,?, ?, ?, ?,?,?, ?,?) RETURNING id',
                [
                    newUserID,
                    username,
                    hashedPassword,
                    email,
                    nickname,
                    true,
                    telegramID,
                    telegramUsername,
                    'testingBackground.jpg',
                ] 
            );

           
            response.payload = id;
            return response;
        } catch (e) {
            console.log(e);
            response.isSuccessful = false;
            response.msg = `[${e.code}] ${e.detail}`;
            return response;
        }
    }
}
