import { Knex } from "knex";

class ApiResponse {
    result: boolean;
    msg: string;
    payload: any;
    constructor() {
        this.result = true;
        this.msg = '';
        this.payload = {};
    }
}

export class EventService {
    constructor(private knex: Knex) { }

    async getEventDetail(eventID: string) {
        const response = new ApiResponse();
        try {
            let eventInfo = (await this.knex.raw(`select map_x,map_y,users.image,social_event.id,title, admin_id, meeting_address, date ,ppl,description,type
            from social_event
            inner join users on users.id = social_event.admin_id 
            where social_event.id = ?`, [eventID])).rows;
            response.payload.eventInfo = eventInfo;
            const eventImage = (await this.knex.raw(`select * from event_image where event_id = ?`, [eventID])).rows;
            response.payload.eventImage = eventImage;
            return response;
        } catch (e) {
            console.log(e)
            response.result = false;
            response.msg = `[${e.code}] ${e.detail}`;
            return response;
        }
    }

    async getEvents(type: string | null) {
        const response = new ApiResponse();
        try {
            if (type == null) {
                let avaliableEvent = (await this.knex.raw(`select date - now() as gap,social_event.id, social_event.type, social_event.meeting_address, social_event.description, social_event.date, social_event.created_at, social_event.title, event_image.image, users.nickname,users.image as userImage from social_event inner join event_image on event_image.event_id = social_event.id inner join users on users.id = social_event.admin_id where date >= now() order by gap`)).rows;
                let notavaliableEvent = (await this.knex.raw(`select date - now() as gap,social_event.id, social_event.type, social_event.meeting_address, social_event.description, social_event.date, social_event.created_at, social_event.title, event_image.image, users.nickname,users.image as userImage from social_event inner join event_image on event_image.event_id = social_event.id inner join users on users.id = social_event.admin_id where date < now() order by gap`)).rows;
                response.payload.eventInfo = avaliableEvent.concat(notavaliableEvent);
                return response;
            } else {
                let avaliableEvent = (await this.knex.raw(`select date - now() as gap,social_event.id, social_event.type, social_event.meeting_address, social_event.description, social_event.date, social_event.created_at, social_event.title, event_image.image, users.nickname,users.image as userImage from social_event inner join users on users.id = social_event.admin_id inner join event_image on event_image.event_id = social_event.id where date >= now() and social_event.type = '${type}' order by gap`)).rows;
                let notavaliableEvent = (await this.knex.raw(`select date - now() as gap,social_event.id, social_event.type, social_event.meeting_address, social_event.description, social_event.date, social_event.created_at, social_event.title, event_image.image, users.nickname,users.image as userImage from social_event inner join users on users.id = social_event.admin_id join event_image on event_image.event_id = social_event.id where date < now() and social_event.type = '${type}' order by gap`)).rows;
                response.payload.eventInfo = avaliableEvent.concat(notavaliableEvent);
                return response;
            }
        } catch (e) {
            console.log(e)
            response.result = false;
            response.msg = `[${e.code}] ${e.detail}`;
            return response;
        }
    }

    async createEvent(title: string, address: string, userID: string, eventDateTime: string, ppl: string, description: string, type: string, images: string[], map_x: number, map_y: number) {
        const response = new ApiResponse();
        try {
            const lastEventID = (await this.knex.raw('SELECT MAX(id) FROM social_event')).rows[0].max;
            const newEventID = lastEventID + 1;
            const event_id = await this.knex.raw(`insert into social_event (id,title, admin_id, meeting_address, date ,ppl,description,type, map_x, map_y) values (?,?, ?, ?, ?, ?,?,?,?,?) returning id`, [newEventID, title, userID, address, eventDateTime, parseInt(ppl), description, type, map_x, map_y])
            console.log(event_id.rows[0].id);
            for (let index = 0; index < images!.length; index++) {
                let cover_image = null;
                if (index == 0) {
                    cover_image = event_id.rows[0].id;
                }
                const lastEventImageID = (await this.knex.raw('SELECT MAX(id) FROM event_image')).rows[0].max;
                const newEventImageID = lastEventImageID + 1;
                await this.knex.raw(`insert into event_image (id,event_id, image, cover_image) values (?,?, ?, ?) returning id`, [newEventImageID, event_id.rows[0].id, images![index], cover_image])
            }
            return response;
        } catch (e) {
            console.log(e)
            response.result = false;
            response.msg = `[${e.code}] ${e.detail}`;
            return response;
        }
    }


}