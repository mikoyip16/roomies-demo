import { Knex } from "knex";

class ApiResponse {
    result: boolean;
    msg: string;
    payload: any;
    constructor() {
        this.result = true;
        this.msg = '';
        this.payload = {};
    }
}

export const MessageMode = {
    SiteVistor: 'vistor',
    MessageOwner: 'owner',
    SiteOwner: 'siteOwner'
}

export class AttendeeService {
    constructor(private knex: Knex) { }

    async getAttendees(siteMeetingID: string, userID: string) {
        const response = new ApiResponse();
        let onAttendList = false;
        let host = false;
        try {
            let attendees = (await this.knex.raw(` 
    select sa.id, sa.users_id, sa.attended ,u.id as usersid, u.nickname, u.image,
        case when now() > sm.date then true else false end as past_event
    from site_attendee as sa
    inner join users as u on u.id = sa.users_id 
    inner join site_meeting as sm on sa.meeting_id = sm.id 
    where sm.id = ?`, [siteMeetingID])).rows;
            let date = (await this.knex.raw(`
    select date 
    from site_meeting 
    where site_meeting.id = ?`, [siteMeetingID])).rows;
            const eventDate = new Date(date[0].date); 
            const now = new Date();
            let pastEvent = false;

          
            if (now > eventDate) {
                pastEvent = true;
            } else {
                pastEvent = false;
            }
            
            for (const attendee of attendees) {
                if (attendee.usersid == userID) {
                    onAttendList = true;
                }
            }
          
            if (parseInt(userID) == 101) {
                host = true;
            }
           
            attendees = attendees.map((attendee: { id: any; attended: any; usersid: any; nickname: any; image: any; }) => {
                return {
                    id: attendee.id,
                    attended: attendee.attended,
                    pastEvent,
                    usersID: attendee.usersid, 
                    nickname: attendee.nickname,
                    image: attendee.image
                }
            })
            console.log(attendees)
            response.payload = { attendees, onAttendList, host }
            return response;
        } catch (e) {
            console.log(e)
            response.result = false;
            response.msg = `[${e.code}] ${e.detail}`;
            return response;
        }
    }

    async addAttendList(userID: string, siteMeetingID: string) {
        const response = new ApiResponse();
        try {
            // console.log(req.session?.['user'])
            let result = (await this.knex.raw(`SELECT * from site_attendee WHERE users_id = ? and meeting_id = ?;`, [userID, siteMeetingID])).rows;
            if (result.length === 0) {
                await this.knex.raw(`INSERT INTO site_attendee (users_id, meeting_id, attended) values (?, ?, ?)`, [userID, siteMeetingID, false]);
            } else { 
                response.result = false;
                response.msg = 'duplicated record';
            }
            return response;
        } catch (e) {
            console.log(e)
            response.result = false;
            response.msg = `[${e.code}] ${e.detail}`;
            return response;

        }
    }

    async toggleAttendStatus(attendeeID: string, userID: string) {
        const response = new ApiResponse();
        try {
            const result = await this.knex.raw(`UPDATE site_attendee SET attended = NOT attended WHERE id = ? and users_id = ?`, [attendeeID, userID]);
            if (result.rowCount == 1) {
                response.msg = 'Success';
            } else {
                response.result = false;
                response.msg = 'AttendeeID not exist/ userID not match attendee ID';
            }
            return response;
        } catch (e) {
            console.log(e)
            response.result = false;
            response.msg = `[${e.code}] ${e.detail}`;
            return response;
        }
    }
}