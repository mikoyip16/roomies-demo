import { Knex } from "knex";

const tableName = 'social_event'
const stringColList:string[] = ['rent_district','meeting_address','type','category'];
const integerColList:string[] = ['meeting_district','admin_id'];
const booleanColList:string[] = ['availability'];
const realColList:string[] = ['map_x','map_y'];
const textColList:string[] = ['description'];
const timestampColList:string[] = ['date'];

export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable(tableName);
    if(!hasTable){
        await knex.schema.createTable(tableName,(table)=>{
            //id field
            table.increments();
            //String Field
            for (const stringCol of stringColList) {
                table.string(stringCol);
            }
            //Integer Field
            for (const integerCol of integerColList) {
                table.integer(integerCol);
            }
            //Boolean Field
            for (const booleanCol of booleanColList) {
                table.boolean(booleanCol);
            }
            //Real Field
            for (const realCol of realColList) {
                table.float(realCol);
            }
            //Text Field
            for (const textCol of textColList) {
                table.text(textCol);
            }
            //Text Field
            for (const timestampCol of timestampColList) {
                table.timestamp(timestampCol);
            }
            // created_at, updated_at
            table.timestamps(false,true);
        });  
    }
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists(tableName);
}

