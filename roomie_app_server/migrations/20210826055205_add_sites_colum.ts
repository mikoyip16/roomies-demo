import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.alterTable('site', (table) => {
        table.text('duplex');
        table.text('top_roof');
        table.text('upper_floor');
        table.text('balcony');
        table.text('garden');
    })
}


export async function down(knex: Knex): Promise<void> {
}

