import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    const hasSiteTable = await knex.schema.hasTable('site');
    if(!hasSiteTable){
        await knex.schema.alterTable('site', (table) => {
            table.date('created_at').notNullable().defaultTo(knex.fn.now());
            table.date('updated_at').notNullable().defaultTo(knex.fn.now());             
        })
    }
}


export async function down(knex: Knex): Promise<void> {
    
}
