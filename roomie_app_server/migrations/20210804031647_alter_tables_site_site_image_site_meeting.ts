import { Knex } from "knex";



export async function up(knex: Knex): Promise<void> {
    const hasSiteTable = await knex.schema.hasTable('site')
    if(!hasSiteTable){
        await knex.schema.alterTable('site', (table) => {
            table.foreign('owner_id').references('id').inTable('users')
        })
    }
    const hasSiteImageTable = await knex.schema.hasTable('site_image')
    if(!hasSiteImageTable){
        await knex.schema.alterTable('site_images', (table) => {
            table.foreign('site_id').references('id').inTable('site')
        })
    }
    const hasSiteMeetingTable = await knex.schema.hasTable('site_meeting')
    if(!hasSiteMeetingTable){
        await knex.schema.alterTable('site_meeting', (table) => {
            table.foreign('site_id').references('id').inTable('site')
        })
    }
}

export async function down(knex: Knex): Promise<void> {
    // await knex.schema.alterTable('site_meeting', (table) => {
    //     table.dropForeign("site_id");
    // });
    // await knex.schema.alterTable('site_image', (table) => {
    //     table.dropForeign("site_id");
    // });
    // await knex.schema.alterTable('site', (table) => {
    //     table.dropForeign("owner_id");
    // });
}






