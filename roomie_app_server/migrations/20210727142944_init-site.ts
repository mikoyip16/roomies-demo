import { Knex } from "knex";

const stringColList:string[] = ['title','district','address','deposit','view'];
const integerColList:string[] = ['owner_id','rent','rooms','years','floor','gross_area','usable_area'];
const booleanColList:string[] = ['television','air_con','washing_machine','water_heater','microwave','fridge','bed'];
const realColList:string[] = ['map_x','map_y'];
const textColList:string[] = ['description'];

export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable('site');
    if(!hasTable){
        await knex.schema.createTable('site',(table)=>{
            //id field
            table.increments();
            //String Field
            for (const stringCol of stringColList) {
                table.string(stringCol);
            }
            //Integer Field
            for (const integerCol of integerColList) {
                table.integer(integerCol);
            }
            //Boolean Field
            for (const booleanCol of booleanColList) {
                table.boolean(booleanCol);
            }
            //Real Field
            for (const realCol of realColList) {
                table.float(realCol);
            }
            //Text Field
            for (const textCol of textColList) {
                table.text(textCol);
            }
            // created_at, updated_at
            table.timestamps(false,true);
        });  
    }
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists('site');
}

