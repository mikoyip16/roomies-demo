import { Knex } from "knex";

const tableName = 'notification'
const stringColList: string[] = ['target_url', 'message', 'type']; //target_url=收到notification後suppose有條link比你禁返去個app到
const integerColList: string[] = ['receiver_id', 'sender_id'];
const booleanColList: string[] = ['read'];
const realColList: string[] = [];
const textColList: string[] = [];
const timestampColList: string[] = [];

export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable(tableName);
    if (!hasTable) {
        await knex.schema.createTable(tableName, (table) => {
            //id field
            table.increments();
            //String Field
            for (const stringCol of stringColList) {
                table.string(stringCol);
            }
            //Integer Field
            for (const integerCol of integerColList) {
                table.integer(integerCol);
            }
            //Boolean Field
            for (const booleanCol of booleanColList) {
                table.boolean(booleanCol).defaultTo(false)
            }
            //Real Field
            for (const realCol of realColList) {
                table.float(realCol);
            }
            //Text Field
            for (const textCol of textColList) {
                table.text(textCol);
            }
            //Text Field
            for (const timestampCol of timestampColList) {
                table.timestamp(timestampCol);
            }
            // created_at, updated_at
            table.timestamps(false, true);
        });
    }
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists(tableName);
}

