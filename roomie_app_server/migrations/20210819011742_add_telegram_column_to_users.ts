import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.alterTable('users', (table) => {
        table.integer('telegram_id');
        table.text('telegram_username');
    });
}


export async function down(knex: Knex): Promise<void> {
}

