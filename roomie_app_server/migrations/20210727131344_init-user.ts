import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable('users');
    if(!hasTable){
        await knex.schema.createTable('users',(table)=>{
            table.increments();
            table.string("username");
            table.string("password");
            table.string("mobile");
            table.string("email");
            table.string("image");
            table.integer("flower");
            table.integer("egg");
            table.string("gender");
            table.string("nickname");
            table.string("age");
            table.text("introduction");
            table.string("education_level");
            table.string("occupation");
            table.string("language");
            table.string("ethnicity");
            table.string("preferred_location");
            table.integer("budget_min");
            table.integer("budget_max");
            table.date("rent_date");
            table.integer("rent_duration");
            table.string("preferred_gender");
            table.string("religion");
            table.boolean("have_no_kids");
            table.boolean("not_smoking");
            table.boolean("have_no_pet");
            table.boolean("activated");
            table.string("not_smoking_text");
            table.string("have_no_kids_text");
            table.string("have_no_pet_text");
            table.string("confirm_key");
            table.timestamps(false,true);
        });  
    }
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists('users');
}

