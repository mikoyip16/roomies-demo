import express from "express";
import jwtVerify from "../jwtVerify";
import { userController } from "../main";
import { isLoggedIn } from "../guard";
import { upload } from "../main"
export const userRoute = express.Router();
// route: /user

userRoute.post("/login/native", userController.loginNative);
userRoute.post("/", userController.createUser);
userRoute.get("/activate", userController.activateUser) // 用query拎的所以要get
userRoute.post("/forgotPw", userController.forgetPassword) //!!!!!!!!!整/password/forgot好D
userRoute.post("/resetPw/", userController.resetPw) //!!!!!!!!!整/password/reset好D
userRoute.get("/logout", userController.logout);
userRoute.get("/search/:userID", userController.getUser);
userRoute.put('/update', isLoggedIn, userController.updateUser)
userRoute.post('/notification/:userID', jwtVerify, userController.createNotification)
userRoute.put('/upload/userIcon',isLoggedIn, upload.single('image'), userController.uploadUserIcon)
userRoute.get('/notifications', jwtVerify, userController.getNotifications)
