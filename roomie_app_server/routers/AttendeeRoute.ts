import express from 'express';
import jwtVerify from '../jwtVerify';
import { attendeeController } from "../main";

export const attendeeRoute = express.Router();

attendeeRoute.get("/", jwtVerify, attendeeController.getAttendees) //get + query
attendeeRoute.post("/", jwtVerify, attendeeController.addAttendList)
attendeeRoute.put("/:attendeeID", jwtVerify, attendeeController.toggleAttendStatus) //param 因為整一個人的data