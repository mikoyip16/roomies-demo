import express from 'express';
import { siteDetailController } from "../main";

export const siteDetailRoute = express.Router();

//*** routue start with /siteDetail


siteDetailRoute.get('/:siteID', siteDetailController.getSiteDetail); // /si
