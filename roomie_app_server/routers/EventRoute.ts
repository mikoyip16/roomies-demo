import express from 'express';
import { eventController } from "../main";
import jwtVerify from "../jwtVerify";
import { upload } from "../main"

export const eventRoute = express.Router();


//*** routue start with /event

//eventAttendee
eventRoute.post("/attendee", jwtVerify, eventController.addAttendList); //create Attendee
eventRoute.get("/attendees", jwtVerify, eventController.getAttendees); //get Attendee list
eventRoute.put("/attendee/:attendeeID", jwtVerify, eventController.toggleAttendStatus); //toggle attendee status

//event message
eventRoute.post("/message", jwtVerify, upload.single('image'), eventController.creatMessage); //create Attendee
eventRoute.get("/messages/:eventID", eventController.getMessages); //get Attendee list
eventRoute.delete("/message/:messageID", jwtVerify, eventController.deleteMessage); //toggle attendee status

//eventInfo
eventRoute.get("/", eventController.getEvents);
eventRoute.get("/:eventID", eventController.getEvent);
eventRoute.post("/", jwtVerify, upload.array('images'), eventController.createEvent); //  /event






