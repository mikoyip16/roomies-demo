import express from 'express';
import jwtVerify from '../jwtVerify';
import { messageController } from "../main";
import { upload } from "../main"

export const messageRoute = express.Router();

messageRoute.get("/:siteMeetingID", messageController.getMessages);
messageRoute.post("/", jwtVerify, upload.single('image'), messageController.creatMessage); //upload.single('image') 食formdata
messageRoute.delete("/:id", jwtVerify, messageController.deleteMessage);
