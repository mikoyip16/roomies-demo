import express from 'express';
import { siteMeetingController } from "../main";

export const siteMeetingRoute = express.Router();

siteMeetingRoute.get("/:siteID", siteMeetingController.getsiteMeetings);
siteMeetingRoute.get('/site_profile/:siteID', siteMeetingController.randomSites);