import express from 'express';
import { sitePageController, } from '../main';

export const sitePageRoute = express.Router();

sitePageRoute.get('/', sitePageController.displayAllSites);
sitePageRoute.post('/search', sitePageController.searchSites);
sitePageRoute.post('/', sitePageController.filterSites);
// upload.none(), 
// upload