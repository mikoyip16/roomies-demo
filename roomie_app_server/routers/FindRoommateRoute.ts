import express from 'express';
import { findRoommateController } from '../main'

export const findRoommateRoute = express.Router();

findRoommateRoute.get('/', findRoommateController.displayAllRoommates);
findRoommateRoute.post('/search', findRoommateController.filterRoommates);