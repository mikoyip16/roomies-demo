export interface ApiResponse {
    isSuccessful: boolean;
    msg: string;
    payload: any;
}

export const apiResponseInit: ApiResponse = {
    isSuccessful: true,
    msg: '',
    payload: {}
}

declare global{
    namespace Express{
        interface Request{
            userID?: number
        }
    }
}
