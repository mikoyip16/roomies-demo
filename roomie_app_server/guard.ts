import { Bearer } from 'permit';
import express from 'express';
import jwtSimple from 'jwt-simple';
import jwt from './jwt';

// import { JWTPayload } from './types';
import { logger } from './logger';


const permit = new Bearer({
    query: 'access_token',
});

export function isLoggedIn(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
) {
    if (!jwt.jwtSecret) {
        res.status(500).json({ error: 'missing JWT_SECRET in env' });
        return;
    }

    let token: string;
    try {
        token = permit.check(req);
    } catch (error) {
        res.status(400).json({ error: 'invalid Bearer token in req' });
        return;
    }

    if (!token) {
        res.status(401).json({ error: 'empty Bearer token in req' });
        return;
    }

    let payload: any 
    let userID
    
    try {
        payload = jwtSimple.decode(token, jwt.jwtSecret);
        // logger.debug(payload)
        userID = payload.userID
    } catch (error) {
        res.status(401).json({ error: 'invalid JWT in req' });
        return;
    }
    logger.debug(userID)
    req.userID = userID;
    next();
}
