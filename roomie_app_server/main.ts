import express from 'express';
import expressSession from 'express-session';
import multer from 'multer';
import { logger } from './logger';
import dotenv from 'dotenv';
import http from 'http';
import { Server as SocketIO } from 'socket.io';
import Knex from 'knex';
import mailjet from 'node-mailjet';
import cors from 'cors';
import path from 'path';

dotenv.config(); 
import * as knexConfig from './knexfile';
const knex = Knex(knexConfig[process.env.NODE_ENV || 'development']);

const app = express();
app.use(express.static('public'))
const server = new http.Server(app);
const io = new SocketIO(server);
const email = mailjet.connect(
    'cb046104953e847317064299e866d490',
    '45d7b67d1f71bf44b77cac9ed569f371'
);

app.use(cors());
app.use(express.json({ limit: '50mb' }));
app.use(
  express.urlencoded({
    limit: '50mb',
    extended: true,
    parameterLimit: 50000,
  })
);
app.use(express.json());
app.use(
    expressSession({
        secret: 'roomie',
        resave: true,
        saveUninitialized: true,
    })
);
app.use((req, res, next) => {
    logger.debug(`ip: [${req.ip}], path: [${req.path}] method: [${req.method}]`);
    next();
});
io.on('connection', function (socket: any) {
    console.log(socket);
});
app.get('/', (req, res) => {
    res.json(req.headers);
});

const storage = multer.diskStorage({
     
    destination: function (req, file, cb) {
        cb(null, path.resolve('./uploads'));
    },
    filename: function (req, file, cb) {
        cb(null, `${file.fieldname}-${Date.now()}.${file.mimetype.split('/')[1]}`);
    },
});
export const upload = multer({ storage, limits:{fileSize: 20*1024*1024}});

import { UserService } from './services/UserService';
import { SiteMeetingService } from './services/SiteMeetingService';
import { SiteDetailService } from './services/SiteDetailService';
import { MailService } from './services/MailService';
import { MessageService } from './services/MessageService';
import { AttendeeService } from './services/AttendeeService';
import { EventService } from './services/EventService';
import { EventAttendeeService } from './services/EventAttendeeService';
import { EventMessageService } from './services/EventMessageService';
import { SitePageService } from './services/SitePageService';
import { FindRoommateService } from './services/FindRoommateService';
import { TelegramBotService } from './services/TelegramBotService';
import { NotificationService } from './services/NotificationService';
import { init as webpushInit, sendNotification as webpushSendNotification } from './services/WebPushService'


const notificationService = new NotificationService(knex);
const mailService = new MailService(email);
const siteDetailService = new SiteDetailService(knex);
const siteMeetingService = new SiteMeetingService(knex);
const userService = new UserService(knex, mailService);
const messageService = new MessageService(knex);
const attendeeService = new AttendeeService(knex);
const eventService = new EventService(knex);
const eventAttendeeService = new EventAttendeeService(knex);
const eventMessageService = new EventMessageService(knex);
const sitePageService = new SitePageService(knex);
const findRoommateService = new FindRoommateService(knex);
const telegramBotService = new TelegramBotService(process.env.TELEGRAM_BOT_TOKEN!, process.env.JWT_SECRET!, userService);



import { UserController } from './controllers/UserController';
import { SiteDetailController } from './controllers/SiteDetailController';
import { MessageController } from './controllers/MessageController';
import { SiteMeetingController } from './controllers/SiteMeetingController';
import { AttendeeController } from './controllers/AttendeeController';
import { EventController } from './controllers/EventController';
import { SitePageController } from './controllers/SitePageController';
import { FindRoommateController } from './controllers/FindRoommateController';

export const userController = new UserController(userService, notificationService, telegramBotService);
export const messageController = new MessageController(messageService);
export const siteDetailController = new SiteDetailController(siteDetailService);
export const siteMeetingController = new SiteMeetingController(siteMeetingService);
export const attendeeController = new AttendeeController(attendeeService, telegramBotService,
    userService,
    notificationService, siteDetailService, 
   
    );
export const eventController = new EventController(
    eventService,
    eventAttendeeService,
    eventMessageService,
    telegramBotService,
    userService,
    notificationService
);
export const sitePageController = new SitePageController(sitePageService);
export const findRoommateController = new FindRoommateController(findRoommateService);



import { userRoute } from './routers/UserRoute';
import { messageRoute } from './routers/MessageRoute';
import { siteDetailRoute } from './routers/SiteDetailRoute';
import { siteMeetingRoute } from './routers/SiteMeetingRoute';
import { attendeeRoute } from './routers/AttendeeRoute';
import { eventRoute } from './routers/EventRoute';
import { sitePageRoute } from "./routers/SitePageRoute";
import { findRoommateRoute } from "./routers/FindRoommateRoute";


app.use('/user', userRoute); 
app.use('/messages', messageRoute);
app.use('/siteMeetings', siteMeetingRoute);
app.use('/siteDetail', siteDetailRoute);
app.use('/attendees', attendeeRoute);
app.use('/event', eventRoute); 
app.use('/sites', sitePageRoute);
app.use('/findRoommate', findRoommateRoute);
import jwtSimple from 'jwt-simple';
import jwt from './jwt';

app.use('/images', express.static(path.join(__dirname, 'uploads')));
app.get('/subscribe_public_key',(req,res)=>{
    res.status(201).json({public_key:process.env.WEB_PUSH_PUBLIC_KEY})
})
export const subscriptions = {}
app.post('/subscribe', (req, res) => {
    const subscription = req.body.subscription; 
    const token = req.body.token
    console.log("body", req.body)
    if (token) {

        const userID = jwtSimple.decode(token, jwt.jwtSecret!).userID 

        console.log("Userid,Subscription:", userID, subscription)
        subscriptions[userID] = subscription;
    }

    res.status(201).json({})

})
app.post('/pushTest', (req, res) => {
    console.log("Subscriptions:", subscriptions)
    const OPTION = {
        title: `You received a new message`,
        body: "Global Notification",
        image: '/images/1-1.jpg', 
        icon: '/images/guy-dp2.jpg',
        data: { url: `https://zeuschiu.com/` },

    }
    for (let userID in subscriptions) {
        let subscription = subscriptions[userID]
        webpushSendNotification(subscription, JSON.stringify(OPTION))
    }
    res.json({ Done: "Done" })
})
app.use((req, res, next) => {
    if(req.method == 'GET'){
        res.sendFile(path.resolve(path.join('public' , 'index.html')))
        return
    }
    next()
})
telegramBotService.telegramRegistrationServiceInit();
webpushInit();



const PORT = +process.env.HTTP_PORT! || 8080;
app.listen(PORT, () => {
    console.log(`Listening at http://localhost:${PORT}/`);
    if (process.env.CI) {
        process.exit(0);
    }
});
