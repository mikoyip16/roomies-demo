import jwtSimple from 'jwt-simple';
import { Request, Response, NextFunction } from "express";
import jwt from './jwt';

const jwtVerify = (req: Request, res: Response, next: NextFunction) => {
    const authHeader = req.headers.authorization //成句Bearer + token
    if (authHeader) {
        const token = authHeader.split(' ')[1]; // "Bearer xxxx" -> [Bearer, xxxx]

        const userID = JSON.stringify(jwtSimple.decode(token, jwt.jwtSecret!).userID) //拎住個secret去verify jwt signiture(jwt包1 header 2 payload 3 signiture); jwtSimple.decode(token, jwt.jwtSecret!)係payload個obj, 後面.user_id就拎payload入面既野

        //res.locals.userID 意思:
        //  EventRoute.ts有條route -> eventRoute.post("/", jwtVerify, upload.array('images'), eventController.createEvent);
        //  後面eventController.createEvent要用個verify左既userID, 就係用res.locals.userID pass過去
        res.locals.userID = userID; //用res係因為呢個係要黎pass比其他地方用的，但係拎返佢果時都係const userID = res.locals.userID! (eventController.createEvent)
        next();
    } else { // 如果header不是authorization
        res.sendStatus(401);
    }
};

export default jwtVerify;