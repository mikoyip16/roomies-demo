import { Request, Response } from 'express';
import { EventService } from '../services/EventService';
import { EventAttendeeService } from '../services/EventAttendeeService';
import { EventMessageService, MessageMode } from '../services/EventMessageService';
import { TelegramBotService } from '../services/TelegramBotService';
import { UserService } from '../services/UserService';
import { NotificationService, NOTIFICATION_TYPE_ATTEND_EVENT, ATTEND_EVENT_STRING } from '../services/NotificationService';
import { subscriptions } from '../main';
import { sendNotification as webpushSendNotification } from '../services/WebPushService'


export class EventController {
    constructor(private eventService: EventService, private eventAttendeeService: EventAttendeeService, private eventMessageService: EventMessageService, private telegramBotService: TelegramBotService, private userService: UserService, private notificationService: NotificationService) { }

    // Event Info
    getEvent = async (req: Request, res: Response) => {
        try {
            const eventID = req.params.eventID as string;
            const result = await this.eventService.getEventDetail(eventID);
            if (result.result) {
                return res.status(200).json({ message: "success", payload: result.payload });
            } else return res.status(400).json({ message: result.msg });
        } catch (err) {
            console.error(err.message);
            return res.status(500).json({ message: "internal server error" });
        }
    };

    getEvents = async (req: Request, res: Response) => {
        try {
            const type = req.query.type as string;
            const result = await this.eventService.getEvents(type);
            if (result.result) {
                return res.status(200).json({ message: "success", payload: result.payload });
            } else return res.status(400).json({ message: result.msg });
        } catch (err) {
            console.error(err.message);
            return res.status(500).json({ message: "internal server error" });
        }
    };

    createEvent = async (req: Request, res: Response) => {
        try {
            console.log(res.locals)
            const title = req.body.title;
            const address = req.body.address;
            const eventDateTime = req.body.eventDateTime;
            const ppl = req.body.ppl;
            const userID = res.locals.userID!;
            const type = req.body.type;
            const description = req.body.description;
            const map_x = req.body.map_x;
            const map_y = req.body.map_y;
            const images: string[] = [];
            if (req.files != undefined) {
                for (const file of req.files as Express.Multer.File[]) {
                    images.push(file.filename);
                }
            }
            const result = await this.eventService.createEvent(title, address, userID, eventDateTime, ppl, description, type, images, map_x, map_y);
            if (result.result) {
                return res.status(200).json({ message: "success" });
            } else return res.status(400).json({ message: result.msg });
        } catch (err) {
            console.error(err.message);
            return res.status(500).json({ message: "internal s  erver error" });
        }
    };


    getAttendees = async (req: Request, res: Response) => {
        try {
            const eventID = req.query.eventID as string;
            const userID = res.locals.userID as string;
            console.log(`userId is ${userID}`)
            const result = await this.eventAttendeeService.getAttendees(eventID, userID);
            if (result.result) {
                return res.status(200).json({ message: "success", payload: result.payload });
            } else return res.status(400).json({ message: result.msg });
        } catch (err) {
            console.error(err.message);
            return res.status(500).json({ message: "internal server error" });
        }
    };

    addAttendList = async (req: Request, res: Response) => {
        try {
            const eventID = req.body.eventID as string;
            const eventInfo = (await this.eventService.getEventDetail(eventID)).payload.eventInfo[0];
            console.log(eventInfo);
            const userID = res.locals.userID as string;
            const targetURL = '/gathering/detail?eventID=' + eventID + '&tab=gatheringInfo';
            let sender = await this.userService.getUser(userID);
            let senderName = sender?.userData.nickname;
            const message = ATTEND_EVENT_STRING + eventInfo.title;
            let user = await this.userService.getUser(eventInfo.admin_id);
            const tgID = user?.userData.telegram_id;
            const OPTION = {
                title: `${senderName} joins your event!`,
                body: senderName + message,
                image: '/event.png', 
                icon: '/' + sender?.userData.image, 
                data: { url: targetURL }, 
            }
            const payload = JSON.stringify(OPTION);
            let subscription = subscriptions[eventInfo.admin_id] 

            const result = await this.eventAttendeeService.addAttendList(userID, eventID);
            if (result.result) {
                await this.notificationService.createNotification(eventInfo.admin_id, message, NOTIFICATION_TYPE_ATTEND_EVENT, targetURL, userID);
                if (subscription) webpushSendNotification(subscription, payload)
                if (tgID) {
                    await this.telegramBotService.telegramSendMessage(`${senderName}${message}
                活動：${process.env.REACT_APP_DOMAIN}${targetURL}`, tgID)
                }

                webpushSendNotification(subscription, payload)
                return res.status(200).json({ message: "success" });
            } else return res.status(400).json({ message: result.msg });
        } catch (err) {
            console.error(err.message);
            return res.status(500).json({ message: "internal server error" });
        }
    };

    toggleAttendStatus = async (req: Request, res: Response) => {
        try {
            const attendeeID = req.params.attendeeID as string;
            const userID = res.locals.userID;
            const result = await this.eventAttendeeService.toggleAttendStatus(attendeeID, userID);
            if (result.result) {
                return res.status(200).json({ message: "success" });
            } else return res.status(400).json({ message: result.msg });
        } catch (err) {
            console.error(err.message);
            return res.status(500).json({ message: "internal server error" });
        }
    };

    //Event Message

    getMessages = async (req: Request, res: Response) => {
        try {
            const eventID = req.params.eventID as string;
            const result = await this.eventMessageService.getMessages(eventID, MessageMode.SiteVistor);
            if (result.result) {
                return res.status(200).json({ message: "success", payload: result.payload });
            } else return res.status(400).json({ message: result.msg });
        } catch (err) {
            console.error(err.message);
            return res.status(500).json({ message: "internal server error" });
        }
    };

    creatMessage = async (req: Request, res: Response) => {
        try {
            const userID = res.locals.userID;
            const eventID = req.body.eventID;
            const content = req.body.content;
            console.log(req.file)
            const image = req.file ? req.file.filename : null;
            const result = await this.eventMessageService.creatMessage(userID, eventID, content, image);
            if (result.result) {
                return res.status(200).json({ message: "success" });
            } else return res.status(400).json({ message: result.msg });
        } catch (err) {
            console.error(err.message);
            return res.status(500).json({ message: "internal s  erver error" });
        }
    };

    deleteMessage = async (req: Request, res: Response) => {
        try {
            const messageID = req.params.messageID as string;
            const userID = res.locals.userID as string;
            const result = await this.eventMessageService.deleteMessage(messageID, userID);
            if (result.result) {
                return res.status(200).json({ message: "success" });
            } else return res.status(400).json({ message: result.msg });
        } catch (err) {
            console.error(err.message);
            return res.status(500).json({ message: "internal server error" });
        }
    };

}