import { Request, Response } from 'express';
import { MessageMode, MessageService } from '../services/MessageService';

export class MessageController {
    constructor(private messageService: MessageService) { }

    getMessages = async (req: Request, res: Response) => {
        try {
            const siteMeetingID = req.params.siteMeetingID as string;
            const result = await this.messageService.getMessages(siteMeetingID, MessageMode.SiteVistor);
            if (result.result) {
                return res.status(200).json({ message: "success", payload: result.payload });
            } else return res.status(400).json({ message: result.msg });
        } catch (err) {
            console.error(err.message);
            return res.status(500).json({ message: "internal server error" });
        }
    };

    creatMessage = async (req: Request, res: Response) => {
        try {
            const userID = res.locals.userID;
            const siteMeetingID = req.body.siteMeetingId;
            const content = req.body.content;
            const image = req.file ? req.file.filename : null;
            const result = await this.messageService.creatMessage(userID, siteMeetingID, content, image);
            if (result.result) {
                return res.status(200).json({ message: "success" });
            } else return res.status(400).json({ message: result.msg });
        } catch (err) {
            console.error(err.message);
            return res.status(500).json({ message: "internal s  erver error" });
        }
    };

    deleteMessage = async (req: Request, res: Response) => {
        try {
            const messageID = req.params.id as string;
            const result = await this.messageService.deleteMessage(messageID);
            if (result.result) {
                return res.status(200).json({ message: "success" });
            } else return res.status(400).json({ message: result.msg });
        } catch (err) {
            console.error(err.message);
            return res.status(500).json({ message: "internal server error" });
        }
    };
}