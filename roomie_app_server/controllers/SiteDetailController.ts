import { Request, Response } from 'express';
import { SiteDetailService } from '../services/SiteDetailService';

export class SiteDetailController {
    constructor(private siteDetailService: SiteDetailService) { }

    getSiteDetail = async (req: Request, res: Response) => {
        try {
            const siteID = req.params.siteID as string;
            const result = await this.siteDetailService.getSiteDetail(siteID);
            if (result.isSuccessful) {
                return res.status(200).json({ message: "success", payload: result.payload });
            } else return res.status(400).json({ message: result.msg });
        } catch (err) {
            console.error(err.message);
            return res.status(500).json({ message: "internal server error" });
        }
    };
}