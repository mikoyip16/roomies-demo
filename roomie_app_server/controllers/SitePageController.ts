import { Request, Response } from 'express';
import { logger } from '../logger';
import { SitePageService } from '../services/SitePageService';

export class SitePageController {
    constructor(private sitePageService: SitePageService) {}

    displayAllSites = async (req: Request, res: Response) => {
        try {
            const result = await this.sitePageService.displayAllSites();
            return res.status(200).json(result);
        } catch (err) {
            console.error(err.message);
            return res.status(500).json({ message: 'internal server error' });
        }
    };

    searchSites = async (req: Request, res: Response) => {
        try {
            const address = req.body.address;
            logger.debug(address);
            const result = await this.sitePageService.searchSites(address);
            logger.debug(result);
            return res.status(200).json(result);
        } catch (err) {
            console.error(err.message);
            return res.status(500).json({ message: 'internal server error' });
        }
    };

    filterSites = async (req: Request, res: Response) => {
        try {
            const rangeList = req.body.rangeList;
            const rentRange = req.body.rentRange;
            const area = req.body.area;
            const compartment = req.body.compartment;
            const requirementList = req.body.requirementList;
            const specialList = req.body.specialListList;
            console.log(specialList)
            const result = await this.sitePageService.filterSites(
                rangeList,
                rentRange,
                area,
                compartment,
                requirementList,
                specialList
            );
            // console.log(result);
            return res.status(200).json({ result: result });
        } catch (err) {
            console.error(err.message);
            return res.status(500).json({ message: 'internal server error' });
        }
    };
}
