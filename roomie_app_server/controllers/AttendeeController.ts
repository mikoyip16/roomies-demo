import { Request, Response } from 'express';
import { AttendeeService } from '../services/AttendeeService';
import { SiteDetailService } from '../services/SiteDetailService';
import { TelegramBotService } from '../services/TelegramBotService';
import { UserService } from '../services/UserService';
import { NotificationService, NOTIFICATION_TYPE_ATTEND_SITE, ATTEND_SITE_STRING } from '../services/NotificationService';
import { subscriptions } from '../main';
import { sendNotification as webpushSendNotification } from '../services/WebPushService'


export class AttendeeController {
    constructor(private attendeeService: AttendeeService, private telegramBotService: TelegramBotService, private userService: UserService, private notificationService: NotificationService, private siteDetailService: SiteDetailService, ) { }

    getAttendees = async (req: Request, res: Response) => {
        try {
            const siteMeetingID = req.query.siteMeetingID as string;
            const userID = res.locals.userID as string;
            const result = await this.attendeeService.getAttendees(siteMeetingID, userID);
            if (result.result) {
                return res.status(200).json({ message: "success", payload: result.payload });
            } else return res.status(400).json({ message: result.msg });
        } catch (err) {
            console.error(err.message);
            return res.status(500).json({ message: "internal server error" });
        }
    };

    addAttendList = async (req: Request, res: Response) => {
        try {
            console.log(req.body)
            const siteMeetingID = req.body.siteMeetingID as string;
            const siteID = req.body.siteID as string;
            const siteInfo = (await this.siteDetailService.getSiteDetail(siteID)).payload.siteInfo[0];
            console.log(siteInfo);
            const userID = res.locals.userID as string;
            const targetURL = 'site/detail?siteID=' + siteID + '&tab=siteInfo';
            let sender = await this.userService.getUser(userID);
            let senderName = sender?.userData.nickname;
            const message = ATTEND_SITE_STRING + siteInfo.title;
            let user = await this.userService.getUser(siteInfo.owner_id);
            const tgID = user?.userData.telegram_id;
            const OPTION = {
                title: `${senderName} joins your site meeting`,
                body: senderName + message,
                image: '/event.png', 
                icon: '/' + sender?.userData.image, 
                data: { url: targetURL }, 
            }
            const payload = JSON.stringify(OPTION);
            let subscription = subscriptions[siteInfo.owner_id] 

            console.log(siteMeetingID);
            console.log(userID);
            const result = await this.attendeeService.addAttendList(userID, siteMeetingID);
            if (result.result) {
                await this.notificationService.createNotification(siteInfo.owner_id, message, NOTIFICATION_TYPE_ATTEND_SITE, targetURL, userID);
                if (subscription) webpushSendNotification(subscription, payload)
                if (tgID) {
                    await this.telegramBotService.telegramSendMessage(`${senderName}${message}
                睇樓團：${process.env.REACT_APP_DOMAIN}${targetURL}`, tgID)
                }

                webpushSendNotification(subscription, payload)
                return res.status(200).json({ message: "success" });
            } else return res.status(400).json({ message: result.msg });
        } catch (err) {
            console.error(err.message);
            return res.status(500).json({ message: "internal server error" });
        }
    };

    toggleAttendStatus = async (req: Request, res: Response) => {
        try {
            const attendeeID = req.params.attendeeID as string;
            const userID = res.locals.userID as string;
            const result = await this.attendeeService.toggleAttendStatus(attendeeID, userID);
            if (result.result) {
                return res.status(200).json({ message: "success" });
            } else return res.status(400).json({ message: result.msg });
        } catch (err) {
            console.error(err.message);
            return res.status(500).json({ message: "internal server error" });
        }
    };
}