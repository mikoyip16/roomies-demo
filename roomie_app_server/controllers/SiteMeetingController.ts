import { Request, Response } from 'express';
import { SiteMeetingService } from '../services/SiteMeetingService';

export class SiteMeetingController {
    constructor(private siteMeetingService: SiteMeetingService) { }

    getsiteMeetings = async (req: Request, res: Response) => {
        try {
            const siteID = req.params.siteID as string;
            const result = await this.siteMeetingService.getsiteMeetings(siteID);
            if (result.result) {
                delete result.payload['password']; // For security issue: remove password before response
                delete result.payload['confirm_key']; // For security issue: remove confirm key before response
                return res.status(200).json({ message: "success", userPageDetail: result.payload });
            } else return res.status(400).json({ message: result.msg });
        } catch (err) {
            console.error(err.message);
            return res.status(500).json({ message: "internal server error" });
        }
    };

    randomSites = async (req: Request, res: Response) => {
        try {
            const siteID = req.params.siteID as string;
            const result = await this.siteMeetingService.randomSites(siteID);
            if (result.result) {
                delete result.payload['password']; // For security issue: remove password before response
                delete result.payload['confirm_key']; // For security issue: remove confirm key before response
                return res.status(200).json({ message: "success", userPageDetail: result.payload });
            } else return res.status(400).json({ message: result.msg });
        } catch (err) {
            console.error(err.message);
            return res.status(500).json({ message: "internal server error" });
        }
    };
}