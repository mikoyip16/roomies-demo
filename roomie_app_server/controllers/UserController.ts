import { UserService } from '../services/UserService';
import { Request, Response } from 'express';
import jwtSimple from 'jwt-simple';
import jwt from '../jwt';
import { logger } from '../logger';
import {
    NotificationService,
    NOTIFICATION_TYPE_LEAVE_MESSAGE,
    LEAVE_MESSAGE_STRING,
} from '../services/NotificationService';
import { subscriptions } from '../main';

import { sendNotification as webpushSendNotification } from '../services/WebPushService';
import { TelegramBotService } from '../services/TelegramBotService';
import dotenv from 'dotenv';

dotenv.config();
export class UserController {
    constructor(
        private userService: UserService,
        private notificationService: NotificationService,
        private telegramBotService: TelegramBotService
    ) { }

    loginNative = async (req: Request, res: Response) => {
        try {
            if (!jwt.jwtSecret) {
                return res.status(500).json({ message: 'missing JWT_SECRET in env' });
            }
            const { username, password } = req.body;
            logger.debug(username);
            logger.debug(password);
            if (!username || !password) {
                return res.json({ message: 'missing username/password in req.body' });
            }

            const result = await this.userService.loginNative(username, password);

            if (result.result) {
                let payload = {
                    userID: result.msg.userID,
                    nickname: result.msg.nickname,
                    username: username,
                    image: result.msg.image,
                };
                let token = jwtSimple.encode(payload, jwt.jwtSecret);
                logger.debug(token);
                return res.status(200).json({ jwt_token: token });
            } else return res.status(400).json({ message: result.msg });
        } catch (err) {
            console.error(err.message);
            return res.status(500).json({ message: 'internal server error' });
        }
    };

    createUser = async (req: Request, res: Response) => {
        try {
            if (!jwt.jwtSecret) {
                return res.status(500).json({ message: 'missing JWT_SECRET in env' });
            }
            const { username, password, email, nickname } = req.body;

            const result = await this.userService.createUser(username, password, email, nickname);

            if (result.result) {
                let payload = {
                    userID: result.msg,
                    nickname: nickname,
                    username: username,
                    image: 'testingBackground.jpg',
                };
                let token = jwtSimple.encode(payload, jwt.jwtSecret);
                logger.debug(token);
                return res.status(200).json({ jwt_token: token });
            } else return res.status(400).json({ message: result.msg });
        } catch (err) {
            console.error(err.message);
            return res.status(500).json({ message: 'internal server error' });
        }
    };

    updateUser = async (req: Request, res: Response) => {
        try {
            const info = req.body.info;

            const userID = JSON.stringify(req.userID);
            const result = await this.userService.updateUser(userID, info);

            if (result) return res.status(200).json({ message: 'success update user information' });
            else return res.status(400).json({ message: 'update fail' });
        } catch (err) {
            console.error(err.message);
            return res.status(500).json({ message: 'internal server error' });
        }
    };

    uploadUserIcon = async (req: Request, res: Response) => {
        try {
            if (!jwt.jwtSecret) {
                return res.status(500).json({ message: 'missing JWT_SECRET in env' });
            }
            const userID = req.userID;
            const image = req.file ? req.file.filename : 'testingBackground.jpg';
            const result = await this.userService.uploadUserIcon(userID!, image);
            if (result) {
                let payload = {
                    userID: userID,
                    nickname: result.nickname,
                    username: result.username,
                    image: image,
                };
                let token = jwtSimple.encode(payload, jwt.jwtSecret);
                logger.debug(token);
                logger.debug(payload)
                return res.status(200).json({ jwt_token: token });
            } else return res.status(400).json({ message: 'uploadIcon fail' });
        } catch (err) {
            console.error(err.message);
            return res.status(500).json({ message: 'internal server error' });
        }
    };

    logout = async (req: Request, res: Response) => {
        try {
            req.session.destroy(() => {
                res.redirect('/index.html');
            });
        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: 'internal server error' });
        }
    };
    getUser = async (req: Request, res: Response) => {
        try {
            const userID = req.params.userID as string;
            const result = await this.userService.getUser(userID);
            return res.status(200).json(result);
        } catch (err) {
            console.error(err.message);
            return res.status(500).json({ message: 'internal server error' });
        }
    };

    activateUser = async (req: Request, res: Response) => {
        try {
            const confirmKey = req.query.confirmKey as string;
            const result = await this.userService.activateUser(confirmKey);
            if (result.result) return res.status(200).json({ message: 'success' });
            else return res.status(400).json({ message: result.msg });
        } catch (err) {
            console.error(err.message);
            return res.status(500).json({ message: 'internal server error' });
        }
    };

    forgetPassword = async (req: Request, res: Response) => {
        try {
            const { emailAddress } = req.body;
            const result = await this.userService.forgotPw(emailAddress);
            if (result.result) return res.status(200).json({ message: 'success' });
            else return res.status(400).json({ message: result.msg });
        } catch (err) {
            console.error(err.message);
            return res.status(500).json({ message: 'internal server error' });
        }
    };

    resetPw = async (req: Request, res: Response) => {
        try {
            const { confirmKey, pw } = req.body;
            const result = await this.userService.resetPw(confirmKey, pw);
            if (result.result) return res.status(200).json({ message: 'success' });
            else return res.status(400).json({ message: result.msg });
        } catch (err) {
            console.error(err.message);
            return res.status(500).json({ message: 'internal server error' });
        }
    };

    createNotification = async (req: Request, res: Response) => {
        try {
            const { message } = req.body;
            const receiverID = req.params.userID;
            const senderID = res.locals.userID;
            const targetURL = '/user/search?userID=' + senderID;
            let sender = await this.userService.getUser(senderID);
            let senderName = sender?.userData.nickname;
            const result = await this.notificationService.createNotification(
                receiverID,
                LEAVE_MESSAGE_STRING + message,
                NOTIFICATION_TYPE_LEAVE_MESSAGE,
                targetURL,
                senderID
            );
            const OPTION = {
                title: `${senderName} send you a new message`,
                body: senderName + LEAVE_MESSAGE_STRING + message,
                image: '/envelope.png',
                icon: '/' + sender?.userData.image, 
                data: { url: targetURL }, 
            };
            const payload = JSON.stringify(OPTION);
            let subscription = subscriptions[receiverID];
            if (subscription) webpushSendNotification(subscription, payload);
            //user tg id
            let user = await this.userService.getUser(receiverID);
            let tgID = user?.userData.telegram_id;
            if (tgID) {
                await this.telegramBotService.telegramSendMessage(
                    `${senderName}${LEAVE_MESSAGE_STRING} ${message}
                即刻搵番佢：${process.env.REACT_APP_DOMAIN}${targetURL}`,
                    tgID
                );
            }
            if (result.isSuccessful) return res.status(200).json({ message: 'success' });
            else return res.status(400).json({ message: result.msg });
        } catch (err) {
            console.error(err.message);
            return res.status(500).json({ message: 'internal server error' });
        }
    };

    getNotifications = async (req: Request, res: Response) => {
        const userID = res.locals.userID;
        const result = await this.notificationService.getNotifications(userID);
        if (result.isSuccessful) {
            return res.status(200).json({ message: 'success', payload: result.payload });
        } else {
            return res.status(400).json({ message: result.msg });
        }
    };
}
