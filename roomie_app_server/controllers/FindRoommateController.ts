import { Request, Response } from 'express';
import { FindRoommateService } from '../services/FindRoommateService';

export class FindRoommateController {
    constructor(private findRoommateService: FindRoommateService) { }

    displayAllRoommates = async (req: Request, res: Response) => {
        try {
            
            const roommateData = await this.findRoommateService.displayAllRoommates();
            return res.status(200).json(roommateData);
        } catch (err) {
            return res.status(500).json({ message: 'internal server error' });
        }
    };

    filterRoommates = async (req: Request, res: Response) => {
        try {
            const ethnicity = req.body.ethnicity;
            const religion = req.body.religion;
            const language = req.body.language;
            const occupation = req.body.occupation;
            const rentRange = req.body.rentRange;
            const preferredLocation = req.body.preferredLocation;
            const gender = req.body.gender;
            const rentDate = req.body.rentDate;
            const rentDuration = req.body.rentDuration;
            const specialRequirementList = req.body.specialRequirementList;
            const ageList = req.body.ageList;
            
            const filterRoommateData = await this.findRoommateService.filterRoommates(
                ethnicity,
                religion,
                language,
                occupation,
                rentRange,
                preferredLocation,
                gender,
                rentDate,
                rentDuration,
                specialRequirementList,
                ageList
            );
            return res.status(200).json({ filterRoommateData: filterRoommateData });
        } catch (err) {
            return res.status(500).json({ message: 'internal server error' });
        }
    };
}
