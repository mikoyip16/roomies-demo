const tables = Object.freeze({
    SITE: "site",
    SITE_ATTENDEE: "site_attendee",
    SITE_IMAGE: "site_images",
    SITE_MEETING: "site_meeting",
    SITE_MESSAGE: "site_message",
    SOCIAL_EVENT: "social_event",
    EVENT_ATTENDEE: "event_attendee",
    EVENT_IMAGE: "event_image",
    EVENT_MESSAGE: "event_message",
    FLOWER_EGG_RECORD: "flower_egg_record",
    BLOCKED_USER: "blocked_user",
    USERS: "users"
  });
  
  export default tables;
  